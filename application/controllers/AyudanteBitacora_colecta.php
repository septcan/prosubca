<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AyudanteBitacora_colecta extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelRuta');
        $this->load->model('ModeloBitacora');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $data['idpersonal']=$this->idpersonal;
        $data['perfil']=$this->perfilid;/// 3 operador - 1 administrador - 2 administrativo
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/ayudantebitacora/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/ayudantebitacora/formjs');  
    }

    function get_folios()
    {
        $dia = $this->input->post('dia');
        $operador = $this->input->post('operador');
        $idoperador = $this->input->post('idoperador');
        $get_total_folios=$this->ModeloBitacora->get_total_folios_ayudante($dia,$operador);
        $total_folios=0;
        foreach ($get_total_folios as $tf){
            $total_folios=$tf->total;
        }


        $idreg=0;    
        $result_ope=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_ayudate',array('idoperador'=>$idoperador,'dia'=>$dia));
        foreach ($result_ope->result() as $op){
            $idreg=$op->id;
        }

        $where = array('fecfol'=>$dia,'chofer'=>$operador);
        $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$where);
        $html='<h4>Folios: '.$total_folios.'</h4>
        <div class="row">';
        foreach ($get_bascula->result() as $b){
            $html.='<div class="col-sm-2" style="padding: 3px;"><button type="button" class="btn btn-warning mr-1 mb-1 btn-block btn_folios" onclick="btn_folio('.$b->id.')">'.$b->folio.'</button></div>';
        }
        $html.='</div><hr>';      
        $arraydata = array('idreg'=>$idreg,'folios'=>$total_folios,'texto_folios'=>$html);              
        echo json_encode($arraydata);
    }

    function get_info_folio()
    {
        $id = $this->input->post('id');
        $where = array('id'=>$id);
        $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$where);
        $folio='';
        $descrip='';
        $peso_real='';
        $procede='';
        $origen='';
        foreach ($get_bascula->result() as $b){
            $folio=$b->folio;
            $descrip=$b->descri;
            $peso_real=$b->peso_real;
            $procede=$b->procede;
            $origen=$b->origen;
        }

        $html='<div class="row">
            <div class="col-sm-4">
                <input type="hidden" id="peso_realk" value="'.$peso_real.'">  
                <h5>FOLIO: '.$folio.'</h5>
                <h5>PRODUCTO: '.$descrip.'</h5>
                <h5>PESO REAL: '.$peso_real.' .KG</h5>
                <h5>PROCEDE: '.$procede.'</h5>
                <h5>ORIGEN: '.$origen.'</h5>
            </div> 
            <div class="col-sm-4 btn_validar_'.$folio.'">
            </div>
        </div>';
        echo $html;
    }

    function get_info_gastos()
    {   
        $id=$this->input->post('id'); 
        $wherebc = array('id'=>$id);
        $where = array('activo'=>1);
        $result=$this->ModeloCatalogos->getselectrowwheren('categoria',$where);
        $html='<div class="row">
            <div class="col-sm-5">
                <fieldset class="form-group">
                    <label>Tipo de gasto</label>
                    <select class="form-control round" id="idtipogasto_g"><option value="0" selected disabled>Selecciona una opción</option>';
                    foreach ($result->result() as $x){
                             $html.='<option value="'.$x->id.'">'.$x->nombre.'</option>';
                        }
                    $html.='</select>
                </fieldset>
            </div>
            <div class="col-sm-2">
                <fieldset class="form-group">
                    <label>Importe</label>
                    <input type="number" id="importe_g" class="form-control round">
                </fieldset>
            </div>
            <div class="col-sm-1">
                <label style="color:#ff000000;">___</label><br>
                <button type="button" class="btn btn-warning btn-icon round mr-1 mb-1 btn_registro_gasto" title="Agregar" onclick="add_gatos_bitacora()"><b><i class="ft-plus"></i></b></button>
            </div>
        </div>';
        echo $html;
    }

    function addregistro_bitacora_colecta_gastos_ayudante(){
        $idcategoria = $this->input->post('idcategoria');
        $importe = $this->input->post('importe');
        $idoperador = $this->input->post('idoperador');
        $dia = $this->input->post('dia');
            
        $idreg=0;    
        $result_ope=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_ayudate',array('idoperador'=>$idoperador,'dia'=>$dia));
        foreach ($result_ope->result() as $op){
            $idreg=$op->id;
        }
        if($idreg==0){
            $data['idoperador']=$idoperador;
            $data['dia']=$dia;
            $data['reg']=$this->fechahoy;
            $data['personalId']=$this->idpersonal; 

            $idreg=$this->ModeloCatalogos->Insert('bitacora_colecta_ayudate',$data);
        }
        $data = array('idbitacora_colecta_ayudante'=>$idreg,'idcategoria'=>$idcategoria,'importe'=>$importe,'importe_historial'=>$importe);
        $this->ModeloCatalogos->Insert('ayudante_bitacora_gastos',$data);
        echo $idreg;
    }

    function get_tabla_gastos_colecta()
    {   
        $id=$this->input->post('idbitacora_colecta');
        $html='<div class="row">
            <div class="col-sm-12"><table class="table table-striped table-bordered" id="datatable_gasto" style="width: 100%;"> 
            <thead>
                <tr>
                    <th>Tipo gasto</th>
                    <th>Importe</th>
                    <th></th>
                </tr>
            </thead>';
            $totalk=0;
            $result_pro=$this->ModeloBitacora->get_gastos_colecta_ayudante($id);
            $suma_total=0;
            foreach ($result_pro as $x){
                $btn_borrar='';
                if($x->estatus==0){
                $btn_borrar='<a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_gasto('.$x->id.')"><b><i class="ft-trash-2"></i></b></a>';
                }
                $html.='<tr class="row_gs_'.$x->id.'">
                    <td>'.$x->tipogasto.'</td>
                    <td><input type="hidden" id="importe_x" value="'.$x->importe.'">$'.number_format($x->importe,2,'.',',').'</td>
                    <td>'.$btn_borrar.'</td>
                </tr>';
                $suma_total+=$x->importe;
            }
            $html.='<tbody>
            </tbody>
        </table>
        </div></div>
        <div class="row">
            <div class="col-sm-4">
                <h4>Total de gatos: <span class="total_gastos">$'.number_format($suma_total,2,'.',',').'</span></h4>
            </div>
        </div>
        <div class="col-sm-12"><br>
            <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="validar_estatus_gastos('.$id.')">Validar gastos  <span><i class="fa fa-check-square-o"></i></span></button>
        </div>';
        echo $html;
    }

    function delete_registro_gasto(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('ayudante_bitacora_gastos',$data,'id',$id);
    }
    
    function estatus_colecta_gastos(){  
        $id = $this->input->post('id');
        $databc = array('estatus'=>1);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_ayudate',$databc,'id',$id);
    }
}

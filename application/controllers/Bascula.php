<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bascula extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloBascula');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,9);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/bascula/index');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/bascula/indexjs');   
    }

    function registro($id=0)
    {
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/bascula/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/bascula/formjs');   
    }

    function cargaexcel(){
        $configUpload['upload_path'] = FCPATH.'fileExcel/';
        $configUpload['allowed_types'] = 'xls|xlsx|csv';
        $configUpload['max_size'] = '5000000';
        $this->load->library('upload', $configUpload);
        //$this->upload->do_upload('inputFile');  
        if ( ! $this->upload->do_upload('archivo')){
                $error = $this->upload->display_errors();
                log_message('error', 'error a la carga: '.$error);
        }
        $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
        $file_name = $upload_data['file_name']; //uploded file name
        $extension=$upload_data['file_ext']; // uploded file extension  
        //===========================================
        
        $objReader= PHPExcel_IOFactory::createReader('Excel2007'); // For excel 2007     
        $objReader->setReadDataOnly(true);          
        //Load excel file
        $objPHPExcel=$objReader->load(FCPATH.'fileExcel/'.$file_name);    

        $totalrows=$objPHPExcel->setActiveSheetIndex(0)->getHighestRow();   //Count Numbe of rows avalable in excel         
        $objWorksheet=$objPHPExcel->setActiveSheetIndex(0);                
        //loop from first data untill last data
        for($i=2;$i<=$totalrows;$i++){
            $folio= $objWorksheet->getCellByColumnAndRow(0,$i)->getValue(); 
            $clvprov= $objWorksheet->getCellByColumnAndRow(1,$i)->getValue(); 
            $razsoc= $objWorksheet->getCellByColumnAndRow(2,$i)->getValue(); 
            $fecfol= $objWorksheet->getCellByColumnAndRow(3,$i)->getValue(); 
            $fecsal= $objWorksheet->getCellByColumnAndRow(4,$i)->getValue();
            $horaent= $objWorksheet->getCellByColumnAndRow(5,$i)->getValue(); 
            $horasal= $objWorksheet->getCellByColumnAndRow(6,$i)->getValue();
            $chofer= $objWorksheet->getCellByColumnAndRow(7,$i)->getValue();
            $placas= $objWorksheet->getCellByColumnAndRow(8,$i)->getValue();
            $transporte= $objWorksheet->getCellByColumnAndRow(9,$i)->getValue();
            $procede= $objWorksheet->getCellByColumnAndRow(10,$i)->getValue();
            $origen= $objWorksheet->getCellByColumnAndRow(11,$i)->getValue();
            $clvprod= $objWorksheet->getCellByColumnAndRow(12,$i)->getValue();
            $descri= $objWorksheet->getCellByColumnAndRow(13,$i)->getValue();
            $clvope= $objWorksheet->getCellByColumnAndRow(14,$i)->getValue();
            $nombre= $objWorksheet->getCellByColumnAndRow(15,$i)->getValue();
            $peso_envdo= $objWorksheet->getCellByColumnAndRow(16,$i)->getValue();
            $peso_empaq= $objWorksheet->getCellByColumnAndRow(17,$i)->getValue();
            $pbruto= $objWorksheet->getCellByColumnAndRow(18,$i)->getValue();
            $tara= $objWorksheet->getCellByColumnAndRow(19,$i)->getValue();
            $neto= $objWorksheet->getCellByColumnAndRow(20,$i)->getValue();
            $peso_real= $objWorksheet->getCellByColumnAndRow(21,$i)->getValue();
            $cancel= $objWorksheet->getCellByColumnAndRow(22,$i)->getValue();
            $ciclo= $objWorksheet->getCellByColumnAndRow(23,$i)->getValue();
            $manual= $objWorksheet->getCellByColumnAndRow(24,$i)->getValue();
            
            if(!isset($folio)){
                $folio='';  
            }
            if(!isset($clvprov)){
                $clvprov='';  
            }
            if(!isset($razsoc)){
                $razsoc='';  
            }
            if(!isset($fecfol)){
                $fecfol_php='';  
            }else{
                $timestamp = PHPExcel_Shared_Date::ExcelToPHP($fecfol);
                $timestamp = strtotime("+1 day",$timestamp);
                $fecfol_php = date("Y-m-d", $timestamp);
            }
            if(!isset($fecsal)){
                $fecsal_php='';  
            }else{
                $timestamp_fecsal = PHPExcel_Shared_Date::ExcelToPHP($fecsal);
                $timestamp_fecsal = strtotime("+1 day",$timestamp_fecsal);
                $fecsal_php = date("Y-m-d", $timestamp_fecsal);
            }
            if(!isset($horaent)){
                $horaent='';  
            }
            if(!isset($horasal)){
                $horasal='';  
            }
            if(!isset($chofer)){
                $chofer='';  
            }
            if(!isset($placas)){
                $placas='';  
            }
            if(!isset($transporte)){
                $transporte='';  
            }
            if(!isset($procede)){
                $procede='';  
            }
            if(!isset($origen)){
                $origen='';  
            }
            if(!isset($clvprod)){
                $clvprod='';  
            }
            if(!isset($descri)){
                $descri='';  
            }
            if(!isset($clvope)){
                $clvope='';  
            }
            if(!isset($nombre)){
                $nombre='';  
            }
            if(!isset($peso_envdo)){
                $peso_envdo='';  
            }
            if(!isset($peso_empaq)){
                $peso_empaq='';  
            }
            if(!isset($pbruto)){
                $pbruto='';  
            }
            if(!isset($tara)){
                $tara='';  
            }
            if(!isset($neto)){
                $neto='';  
            }
            if(!isset($peso_real)){
                $peso_real='';  
            }
            if(!isset($cancel)){
                $cancel='';  
            }
            if(!isset($ciclo)){
                $ciclo='';  
            }
            if(!isset($manual)){
                $manual='';  
            }

            $datachecado = array(
                'folio' => $folio,
                'clvprov' => $clvprov,
                'razsoc' => $razsoc,
                'fecfol' => $fecfol_php,
                'fecsal' => $fecsal_php,
                'horaent' => $horaent,
                'horasal' => $horasal,
                'chofer' => $chofer,
                'placas' => $placas,
                'transporte' => $transporte,
                'procede' => $procede,
                'origen' => $origen,
                'clvprod' => $clvprod,
                'descri' => $descri,
                'clvope' => $clvope,
                'nombre' => $nombre,
                'peso_envdo' => $peso_envdo,
                'peso_empaq' => $peso_empaq,
                'pbruto' => $pbruto,
                'tara' => $tara,
                'neto' => $neto,
                'peso_real' => $peso_real,
                'cancel' => $cancel,
                'ciclo' => $ciclo,
                'manual' => $manual,
            );
            $this->ModeloCatalogos->Insert('bascula',$datachecado);
        }
        unlink('fileExcel/'.$file_name); //File Deleted After uploading in database .
        $datachecado = [];
        echo json_encode($datachecado); 
    }

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloBascula->List_table($params);
        $tablelistadorow=$this->ModeloBascula->List_table_total($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('bascula',$data,'id',$id);
    }

}




























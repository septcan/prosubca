<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bitacora_colecta extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelRuta');
        $this->load->model('ModeloBitacora');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,8);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $data['idpersonal']=$this->idpersonal;
        $data['perfil']=$this->perfilid;/// 3 operador - 1 administrador - 2 administrativo
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/bitacora_colecta/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/bitacora_colecta/formjs');  
    }
    
    function search_operador(){
        $search = $this->input->get('search');
        $where = array('estatus'=>1,'puesto'=>2);
        $results=$this->ModeloCatalogos->getselectwherelike('personal',$where,'nombre',$search);
        echo json_encode($results);    
    }

    function search_ruta(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('rutas',$where,'ruta',$search);
        echo json_encode($results);    
    }

    function get_folios()
    {
        $dia = $this->input->post('dia');
        $operador = $this->input->post('operador');
        $idoperador = $this->input->post('idoperador');
        $get_total_folios=$this->ModeloBitacora->get_total_folios($dia,$operador);
        $total_folios=0;
        foreach ($get_total_folios as $tf){
            $total_folios=$tf->total;
        }
        /// validar si ya hay un estatus de folio
        $wherebc = array('dia'=>$dia,'idoperador'=>$idoperador);
        $result_bc=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta',$wherebc);
        $estatus_folio=0;
        foreach ($result_bc->result() as $bc){
            $estatus_folio=$bc->estatus_folio;
        }
        $btn_disabled='';
        if($estatus_folio!=0){
            $btn_disabled='disabled'; 
        }

        ///
        $where = array('fecfol'=>$dia,'razsoc'=>$operador);
        $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$where);
        $html='<h4>Folios: '.$total_folios.'</h4>
        <div class="row">
            ';
        foreach ($get_bascula->result() as $b){
            $result_bcd=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',array('folio'=>$b->folio,'estatus'=>1));
            $btn_disabledcd='';
            $btn_color='btn btn-outline-warning btn-block';
            $estatus_det=0;
            foreach ($result_bcd->result() as $bc){
                //$btn_disabledcd='disabled';
                $estatus_det=$bc->estatus;
                $btn_color='btn btn-warning mr-1 mb-1 btn-block';
            }
            $html.='<div class="col-sm-2" style="padding: 3px;"><button type="button" class="'.$btn_color.' btn_folios" onclick="btn_folio('.$b->id.','.$estatus_det.')" '.$btn_disabled.' '.$btn_disabledcd.'>'.$b->folio.'</button></div>';

        }
        $html.='
        </div>
        <hr>';                    
        echo $html;
    }

    function get_info_folio()
    {
        $id = $this->input->post('id');
        $dia = $this->input->post('dia');
        $idoperador = $this->input->post('idoperador');
        $estatus = $this->input->post('estatus');
        $where = array('id'=>$id);
        $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$where);
        $folio='';
        $descrip='';
        $peso_real='';
        $procede='';
        $origen='';
        foreach ($get_bascula->result() as $b){
            $folio=$b->folio;
            $descrip=$b->descri;
            $peso_real=$b->peso_real;
            $procede=$b->procede;
            $origen=$b->origen;
        }

        $wherebc = array('dia'=>$dia,'idoperador'=>$idoperador);
        $result_bc=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta',$wherebc);
        $estatus_folio=0;
        foreach ($result_bc->result() as $bc){
            $estatus_folio=$bc->estatus_folio;
        }
        $btn_validar='';
        if($estatus_folio==0){
            if($estatus==0){
                $btn_validar='<button type="button" class="btn btn-info round mr-1 mb-1" onclick="validar_folio('.$folio.')">Validar folio</button>'; 
            }
        }
        $html='<div class="row">
            <div class="col-sm-4">
                <input type="hidden" id="peso_realk" value="'.$peso_real.'">  
                <h5>FOLIO: '.$folio.'</h5>
                <h5>PRODUCTO: '.$descrip.'</h5>
                <h5>PESO REAL: '.$peso_real.' .KG</h5>
                <h5>PROCEDE: '.$procede.'</h5>
                <h5>ORIGEN: '.$origen.'</h5>
            </div> 
            <div class="col-sm-4 btn_validar_'.$folio.'">
                '.$btn_validar.'
            </div>
            <div class="col-sm-8 tabla_validar_'.$folio.'" style="display:none">
                <div class="tabla_validar_materia_'.$folio.'"></div>
            </div>

        </div>';
        echo $html;
    }

    function search_proveedor(){
        $search = $this->input->get('search');
        $where = array('estatus'=>1,'activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('proveedores',$where,'nombre',$search);
        echo json_encode($results);    
    }


    public function tabla_materia()
    {   
        $id = $this->input->post('id');
        $where = array('id'=>$id);
        $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$where);
        foreach ($result->result() as $x){
            $whereb = array('folio'=>$x->folio);
            $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$whereb);
            $origen='';
            $peso_real=0;
            foreach ($get_bascula->result() as $ba){
                $origen=$ba->origen;
                $peso_real=$ba->peso_real;
            }
            $idruta=0;
            $ruta='LA RUTA NO EXISTE';
            $whereruta = array('ruta'=>$origen,'activo'=>1);
            $get_ruta=$this->ModeloCatalogos->getselectrowwheren('rutas',$whereruta);
            $ruta_ex=0;
            foreach ($get_ruta->result() as $bt){
                $idruta=$bt->id;
                $ruta=$bt->ruta;
                $ruta_ex=1;
            }
            //if($ruta_ex==1){
            $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',array('ruta'=>$idruta,'pesoreal'=>$peso_real),'id',$id);
            //}

        }
        $html='<table class="table text-center m-0" id="datatable" style="width: 100%;"> 
                    <thead>
                        <tr>
                            <th width="30%" style="font-size: 23px;">Ruta</th>
                            <th width="70"><input type="text" readonly class="form-control" value="'.$ruta.'"></th>
                        </tr>
                    </thead>
                </table>';
        echo $html;        
    }

    function addregistro_bitacora_colecta(){
        $dia = $this->input->post('dia');
        $idoperador = $this->input->post('idoperador');
        $operador = $this->input->post('operador');
        $where = array('dia'=>$dia,'idoperador'=>$idoperador);
        $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta',$where);
        $id=0;
        $estatus_folio=0;
        $idbascula=0;
        foreach ($result->result() as $x){
            $id=$x->id;
            $estatus_folio=$x->estatus_folio;
            $whereb = array('folio'=>$x->estatus_folio);
            $resultb=$this->ModeloCatalogos->getselectrowwheren('bascula',$whereb);
            foreach ($resultb->result() as $bc){
                $idbascula=$bc->id;
            }
        }

        $data['dia']=$dia;
        $data['idoperador']=$idoperador;
        $get_total_folios=$this->ModeloBitacora->get_total_folios($dia,$operador);
        $total_folios=0;
        foreach ($get_total_folios as $tf){
            $total_folios=$tf->total;
        }
        /// validar si ya hay un estatus de folio
        if($id==0){
            $sueldo=0;
            $result_ope=$this->ModeloCatalogos->getselectrowwheren('personal',array('personalId'=>$idoperador));
            foreach ($result_ope->result() as $op){
                $sueldo=$op->sueldo;
            }
            $data['reg']=$this->fechahoy;
            $data['personalId']=$this->idpersonal;
            $data['total_folios']=$total_folios;
            $data['sueldo']=$sueldo;
            $id=$this->ModeloCatalogos->Insert('bitacora_colecta',$data);
        }
        $array = array('id'=>$id,'estatus_folio'=>$estatus_folio,'idbascula'=>$idbascula,'total_folios'=>$total_folios);
        echo json_encode($array);
    }

    function addregistro_bitacora_colecta_folio(){
        $idbitacora_colecta = $this->input->post('idbitacora_colecta');
        $folio = $this->input->post('folio');
        $where = array('idbitacora_colecta'=>$idbitacora_colecta,'folio'=>$folio);
        $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$where);
        $id=0;
        foreach ($result->result() as $x){
            $id=$x->id;
        }
        if($id==0){
            $data['idbitacora_colecta']=$idbitacora_colecta;
            $data['folio']=$folio;
            $id=$this->ModeloCatalogos->Insert('bitacora_colecta_detalle',$data);
        }
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',array('estatus_folio'=>$folio),'id',$idbitacora_colecta);
        echo $id;
    }

    function addregistro_bitacora_colecta_ruta(){
        $id = $this->input->post('idbitacora_colecta_detalle');
        $idruta = $this->input->post('idruta');
        $data = array('ruta'=>$idruta);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',$data,'id',$id);
        echo $id;
    }
    
    function get_info_proveedor_materia()
    {   
        $id=$this->input->post('id');
        $where = array('id'=>$id);
        $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$where);
        $estatus_colecta=0;
        foreach ($result->result() as $x){
            $estatus_colecta=$x->estatus;
        }
        $html='';
        if($estatus_colecta==0){
            $html='<div class="row">
                <div class="col-sm-4">
                    <fieldset class="form-group">
                        <label>Proveedor</label>
                        <select class="form-control round" style="width:100%" id="idproveedor">
                        </select>
                    </fieldset>
                </div>
                <div class="col-sm-3">
                    <fieldset class="form-group">
                        <label>Materia prima</label>
                        <div class="select_materiaprima"></div>
                    </fieldset>
                </div>
                <div class="col-sm-2">
                    <fieldset class="form-group">
                        <label>Kilos</label>
                        <input type="number" id="kilos_b" class="form-control round">
                    </fieldset>
                </div>
                <div class="col-sm-2">
                    <label>Factura</label><br>
                    <div class="custom-switch custom-switch-success mb-1 mb-xl-0">
                        <input type="checkbox" class="custom-control-input" id="factura_materia" checked>
                        <label class="custom-control-label mr-1" for="factura_materia">
                            <span></span>
                        </label>
                    </div>
                </div>  
                <div class="col-sm-1">
                    <label style="color:#ff000000;">___</label><br>
                    <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="add_materia_bitacora()"><b><i class="ft-plus"></i></b></a>
                </div>
            </div>';
            }
        echo $html;
    }

    /*
    <select class="form-control round" id="materiaprima">
    </select>
    */
    function get_proveedor_materia()
    {
        $id=$this->input->post('id');// id proveedor
        $idreg_detalle=$this->input->post('idreg_detalle');
        $where = array('id'=>$idreg_detalle);
        $bitacora_colecta=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$where);
        $folio=0;
        foreach ($bitacora_colecta->result() as $bc){
            $folio=$bc->folio;
        }
        $whereb = array('folio'=>$folio);
        $get_bascula=$this->ModeloCatalogos->getselectrowwheren('bascula',$whereb);
        $descrip='';
        foreach ($get_bascula->result() as $ba){
            $descrip=$ba->descri;
        }
        $precio_sin=0;
        $precio_con=0;
        $idmateriaprima=0;
        $materiaprima='EL PRODUCTO NO EXISTE EN PROVEEDOR';
        $result_proveedor_material=$this->ModeloBitacora->get_proveedor_material($id,$descrip);
        foreach ($result_proveedor_material as $bt){
            $precio_sin=$bt->precio_sin_factura;
            $precio_con=$bt->precio_con_factura;
            $idmateriaprima=$bt->idmateriaprima;
            $materiaprima=$bt->nombre;
        }
        $html='<input type="hidden" id="materiaprima_b" value="'.$idmateriaprima.'">';
        $html.='<input type="hidden" id="precio_sin" value="'.$precio_sin.'">';
        $html.='<input type="hidden" id="precio_con" value="'.$precio_con.'">';
        $html.='<b>'.$materiaprima.'</b>';
        echo $html;    
    }

    function addregistro_bitacora_colecta_detalle_material(){
        $idbitacora_colecta_detalle = $this->input->post('idbitacora_colecta_detalle');
        $idproveedor = $this->input->post('idproveedor');
        $materiaprima = $this->input->post('materiaprima');
        $precio_sin = $this->input->post('precio_sin');
        $precio_con = $this->input->post('precio_con');
        $factura = $this->input->post('factura');
        $kilos = $this->input->post('kilos');
        $precio=0;
        if($factura==1){
            $multi=$precio_con*$kilos;    
            $precio=$precio_con;
        }else{
            $multi=$precio_sin*$kilos;
            $precio=$precio_sin;
        }
        
        $metodo_pago=0;
        $wherep = array('id_proveedor'=>$idproveedor);
        $result_pro=$this->ModeloCatalogos->getselectrowwheren('proveedores',$wherep);
        foreach ($result_pro->result() as $x){
            $metodo_pago=$x->metodo_pago;
        }
        /// Validar kilos
        $wherebit = array('id'=>$idbitacora_colecta_detalle);
        $bitacora_colecta=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$wherebit);
        $pesoreal=0;
        foreach ($bitacora_colecta->result() as $bc){
            $pesoreal=$bc->pesoreal;
        }
        ////
        $kilosx=0;
        $wherebc = array('idbitacora_colecta_detalle'=>$idbitacora_colecta_detalle,'activo'=>1);
        $result_pro=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle_material',$wherebc);
        foreach ($result_pro->result() as $bc){
            $kilosx+=$bc->kilos;
        }
        $suma=$kilosx+$kilos;
        $validar=0;

        if($suma==$pesoreal){
            $validar=0;
        }else if($suma>$pesoreal){
            $validar=1;
        }
        $id=0;
        if($validar==0){
            $data = array('idbitacora_colecta_detalle'=>$idbitacora_colecta_detalle,'idproveedor'=>$idproveedor,'idmateriaprima'=>$materiaprima,'precio'=>$precio,'kilos'=>$kilos,'total'=>$multi,'metodo_pago'=>$metodo_pago,'factura'=>$factura);
            $id=$this->ModeloCatalogos->Insert('bitacora_colecta_detalle_material',$data);
        }

        $arrayinfo = array('id'=>$id,'validar'=>$validar);
        echo json_encode($arrayinfo);
    }

    function get_tabla_proveedor_materia_costos()
    {   
        $id=$this->input->post('idbitacora_colecta_detalle');
        $pesoreal=$this->input->post('pesoreal');
        $html='<div class="row">
                <div class="col-sm-12"><table class="table table-striped table-bordered" id="datatable_material" style="width: 100%;"> 
                <thead>
                    <tr>
                        <th>Proveedor</th>
                        <th>Material</th>
                        <th>Kilos</th>
                        <th>Precio</th>
                        <th>Total</th>
                        <th>Factura</th>
                        <th></th>
                    </tr>
                </thead>';
                $totalk=0;
                $estatus_colecta=0;
                $result_pro=$this->ModeloBitacora->get_proveedor_material_costos($id);
                $folio=0;
                foreach ($result_pro as $x){
                    $precio=0;
                    $total=0;
                    $folio=$x->folio;
                    if($x->metodo_pago==1){
                        $precio=$x->precio;
                        $multi=$x->precio*$x->kilos;
                        $total='$'.number_format($multi,2,'.',','); 
                    }else{
                        $precio=0;
                        $total='PSC';
                    }
                    $estatus_colecta=$x->estatus;
                    
                    $btn_borrar='';
                    if($x->estatus==0){
                        $btn_borrar='<a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_proveedor_material('.$x->id.')"><b><i class="ft-trash-2"></i></b></a>';
                    }

                    $factura_x='';
                    if($x->factura==1){
                        $factura_x='SI';
                    }else{
                        $factura_x='NO';
                    }
                    $html.='<tr class="row_mc_'.$x->id.'">
                        <td>'.$x->proveedor.'</td>
                        <td>'.$x->materia.'</td>
                        <td><input type="hidden" id="kilos_x" value="'.$x->kilos.'"><input type="hidden" id="pesoreal_x" value="'.$pesoreal.'">'.$x->kilos.'</td>
                        <td>$'.number_format($precio,2,'.',',').'</td>
                        <td>'.$total.'</td>
                        <td>'.$factura_x.'</td>
                        <td>'.$btn_borrar.'</td>
                    </tr>';
                    $totalk+=$x->kilos;
                }
                
                $resta=$pesoreal-$totalk;
                $btn_validar='';
                if($estatus_colecta==0){
                    if($resta==0){
                        $btn_validar='<button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="validar_estatus_folio('.$id.','.$folio.')">Validar folio <span><i class="fa fa-check-square-o"></i></span></button>';
                    }
                }
                $html.='<tbody>
                </tbody>
            </table>
            </div></div>
            <div class="row">
                <div class="col-sm-4">
                    <h4>Total de kilos: <span class="total_kilos">'.$totalk.'</span></h4>
                </div>
                <div class="col-sm-4">
                    <h4>Restan: <span class="total_resta">'.$resta.'</span> .kg</h4>
                </div>
                <div class="col-sm-4 btn_v_mc" align="right">
                    '.$btn_validar.'
                </div>
            </div>';
        echo $html;
    }

    function delete_registro_proveedor(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle_material',$data,'id',$id);
    }

    function estatus_colecta(){  
        $id = $this->input->post('id');
        ///
        $wherebit = array('id'=>$id);
        $bitacora_colecta=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta_detalle',$wherebit);
        $idbitacora_colecta=0;
        foreach ($bitacora_colecta->result() as $bc){
            $idbitacora_colecta=$bc->idbitacora_colecta;
        }
        ///
        $data = array('estatus'=>1);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',$data,'id',$id);
        ///
        $databc = array('estatus_folio'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$databc,'id',$idbitacora_colecta);
    }

    function get_info_gastos()
    {   
        $id=$this->input->post('id'); 
        $wherebc = array('id'=>$id);
        $result_pro=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta',$wherebc);
        $total_folios=0;
        $estatus_gastos=0;
        foreach ($result_pro->result() as $bc){
            $total_folios=$bc->total_folios;
            $estatus_gastos=$bc->estatus_gastos;
        } 
        $html='';
        if($total_folios!=0){
            $get_total_folios=$this->ModeloBitacora->get_total_folios_colecta($id);
            $total_folios_c=0;
            foreach ($get_total_folios as $tf){
                $total_folios_c=$tf->total;
            }
            if($total_folios==$total_folios_c){
                if($estatus_gastos==0){ 
                    $where = array('activo'=>1);
                    $result=$this->ModeloCatalogos->getselectrowwheren('categoria',$where);
                    $html.='<div class="row">
                        <div class="col-sm-5">
                            <fieldset class="form-group">
                                <label>Tipo de gasto</label>
                                <select class="form-control round" id="idtipogasto_g"><option value="0" selected disabled>Selecciona una opción</option>';
                                foreach ($result->result() as $x){
                                         $html.='<option value="'.$x->id.'">'.$x->nombre.'</option>';
                                    }
                                $html.='</select>
                            </fieldset>
                        </div>
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Importe</label>
                                <input type="number" id="importe_g" class="form-control round">
                            </fieldset>
                        </div>
                        <div class="col-sm-1">
                            <label style="color:#ff000000;">___</label><br>
                            <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="add_gatos_bitacora()"><b><i class="ft-plus"></i></b></a>
                        </div>
                    </div>';
                }
            }else{
                $html='';
            }
        }else{
            $html='';
        }    
        echo $html;
    }

    function addregistro_bitacora_colecta_gastos(){
        $idbitacora_colecta = $this->input->post('idbitacora_colecta');
        $idcategoria = $this->input->post('idcategoria');
        $importe = $this->input->post('importe');
        $data = array('idbitacora_colecta'=>$idbitacora_colecta,'idcategoria'=>$idcategoria,'importe'=>$importe,'importe_historial'=>$importe);
        $id=$this->ModeloCatalogos->Insert('bitacora_gastos',$data);
        echo $id;
    }

    function get_tabla_gastos_colecta()
    {   
        $id=$this->input->post('idbitacora_colecta');
        $pesoreal=$this->input->post('pesoreal');
        $wherebc = array('id'=>$id);
        $result_pro=$this->ModeloCatalogos->getselectrowwheren('bitacora_colecta',$wherebc);
        $estatus_gastos=0;
        $total_folios=0;
        foreach ($result_pro->result() as $bc){
            $total_folios=$bc->total_folios;
            $estatus_gastos=$bc->estatus_gastos;
        } 

        $get_total_folios=$this->ModeloBitacora->get_total_folios_colecta($id);
        $total_folios_c=0;
        foreach ($get_total_folios as $tf){
            $total_folios_c=$tf->total;
        }
        $html='';
        if($total_folios==$total_folios_c){
            $html='<div class="row">
                <div class="col-sm-12"><table class="table table-striped table-bordered" id="datatable_gasto" style="width: 100%;"> 
                <thead>
                    <tr>
                        <th>Tipo gasto</th>
                        <th>Importe</th>
                        <th></th>
                    </tr>
                </thead>';
                $totalk=0;
                $estatus_colecta=0;
                $result_pro=$this->ModeloBitacora->get_gastos_colecta($id);
                $suma_total=0;
                foreach ($result_pro as $x){
                    $btn_borrar='';
                    if($estatus_gastos==0){
                    $btn_borrar='<a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_gasto('.$x->id.')"><b><i class="ft-trash-2"></i></b></a>';
                    }
                    $html.='<tr class="row_gs_'.$x->id.'">
                        <td>'.$x->tipogasto.'</td>
                        <td><input type="hidden" id="importe_x" value="'.$x->importe.'">$'.number_format($x->importe,2,'.',',').'</td>
                        <td>'.$btn_borrar.'</td>
                    </tr>';
                    $suma_total+=$x->importe;
                }
                $html.='<tbody>
                </tbody>
            </table>
            </div></div>
            <div class="row">
                <div class="col-sm-4">
                    <h4>Total de gatos: <span class="total_gastos">$'.number_format($suma_total,2,'.',',').'</span></h4>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">';
                    if($estatus_gastos==0){
                    $html.='<button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="validar_estatus_gastos('.$id.')">Validar gastos  <span><i class="fa fa-check-square-o"></i></span></button>';
                    }
                $html.='</div>
            </div>';
        }
        echo $html;
    }

    function delete_registro_gasto(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_gastos',$data,'id',$id);
    }

    function estatus_colecta_gastos(){  
        $id = $this->input->post('id');
        $databc = array('estatus_gastos'=>1);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$databc,'id',$id);
    }

}

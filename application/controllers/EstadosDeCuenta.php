
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EstadosDeCuenta extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEstadosDeCuenta');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,15);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/estadocuentas/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/estadocuentas/formjs');   
    }

    function get_estado_cuenta_operador()
    {   
        $idoperador=$this->input->post('idoperador');
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $result=$this->ModeloEstadosDeCuenta->get_estado_cuenta($idoperador,$anio,$mes);
        $wherer = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes);
        $result_r=$this->ModeloEstadosDeCuenta->get_rendimiento($idoperador,$anio,$mes);
        $total_efectivo=0;
        foreach ($result_r as $r){
            $total_efectivo=$r->total_efectivo;
        } 
        
        ////
        $saldo_final_x=0; 
        ///////////SALDO ANTERIOR//////////////
        $mes_act=$mes-1;

        if($mes_act==0){
           $mes_act=12;
           $anio_validar=$anio-1;
        }else{
           $mes_act=$mes-1;
           $anio_validar=$anio;
        }
        
        $wheresa = array('idoperador'=>$idoperador,'anio'=>$anio_validar,'mes'=>$mes_act);
        $result_sa=$this->ModeloCatalogos->getselectrowwheren('estado_cuenta',$wheresa);
        $saldo_anterior=0;
        foreach ($result_sa->result() as $r){
            $saldo_anterior=$r->saldo_final;
        }

        $whereec = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes);
        $result_ec=$this->ModeloCatalogos->getselectrowwheren('estado_cuenta',$whereec);
        $validar_estatus=0;
        foreach ($result_ec->result() as $ec){
            $validar_estatus=$ec->estatus;
        }         


        $saldo_anteriortxt='$'.number_format($saldo_anterior, 2, '.', ',');
        $html='<div class="row">
                    <div class="col-sm-12"><input type="hidden" id="saldo_anterior_x" value="'.$saldo_anterior.'">
                        <h4>SALDO ANTERIOR: '.$saldo_anteriortxt.'</h4>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-sm-12">
                        <input type="hidden" id="saldo" value="0">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable_estado_cuentas" style="width: 100%;"> 
                                <thead>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Ruta</th>
                                        <th>Deposito</th>
                                        <th>Compra/Gasto</th>
                                        <th>Saldo</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                    $conts=0;
                                    foreach ($result as $x){
                                        $idruta=$x->idruta;
                                        /*if($conts==0){
                                            $total_saldo_suma=$x->saldo+$x->deposito;
                                        }else{
                                            $total_saldo_suma=$x->saldo+$x->deposito;
                                        }
                                        
                                        $total_saldo=$total_saldo_suma-$x->total_compras_gastos;*/
                                        $total_saldo=$x->saldo;
                                        $color_saldo='';
                                        if($x->saldo!=0){
                                            if($total_saldo<0){
                                                $color_saldo='style="color:red"'; 
                                            }
                                        }else{
                                            $total_saldo=0; 
                                        }
                                        
                                        /// Select abre
                                        $select_ruta='<select class="form-control round" id="ruta_id'.$x->id.'" onchange="edit_ruta('.$x->id.')"><option value="0">Selecciona opción</option>';
                                        $result_ruta=$this->ModeloEstadosDeCuenta->get_operador_rutas($x->id);
                                        foreach ($result_ruta as $rut){
                                            if($idruta==$rut->id){
                                                $select_ruta.='<option value="'.$rut->id.'" selected>'.$rut->ruta.'</option>';
                                            }else{
                                                $select_ruta.='<option value="'.$rut->id.'">'.$rut->ruta.'</option>';
                                            }
                                        }
                                        $select_ruta.='</select>';
                                        /// Select cierra
                                        $html.='<tr>
                                            <td>'.date('d/m/Y',strtotime($x->dia)).'</td>
                                            <td>'.$select_ruta.'</td>
                                            <td><input type="number" id="deposito" value="'.$x->deposito.'" class="form-control round deposito'.$x->id.'" oninput="edit_deposito('.$x->id.')"></td>
                                            <td><input type="hidden" id="compras_gastos" value="'.$x->total_compras_gastos.'">$'.number_format($x->total_compras_gastos, 2, '.', ',').'</td>
                                            <td><input type="hidden" id="total_saldo" value="'.$total_saldo.'" class="total_saldo_x'.$conts.' total_saldo'.$x->id.'"><span '.$color_saldo.' class="saldot saldo_'.$conts.'" >$'.number_format($total_saldo, 2, '.', ',').'</span></td>
                                        </tr>';
                                        $conts++;
                                        $saldo_final_x=$total_saldo;
                                    }
                                $html.='</tbody>
                                <tfoot>
                                    <tr>
                                        <th>Fecha</th>
                                        <th>Ruta</th>
                                        <th>Deposito</th>
                                        <th>Compra/Gasto</th>
                                        <th>Saldo</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>';


                $html.='<hr><div class="row">
                    <div class="col-sm-4"><input type="hidden" id="rendimiento_total_efectivo" value="'.$total_efectivo.'">
                        <h5>RENDIMIENTO: $'.number_format($total_efectivo, 2, '.', ',').'</h5>
                    </div>
                    <div class="col-sm-4"><input type="hidden" id="saldo_final" value="'.$saldo_final_x.'">
                        <h5>SALDO FINAL: <span class="saldo_final">$'.number_format($saldo_final_x, 2, '.', ',').'</span></h5>
                    </div>
                </div>';
            $titulo_btn='Guardar';    
            if($validar_estatus==1){
                $titulo_btn='Editar';
            }

            $html.='<hr><div class="row">
                    <div class="col-sm-5">
                        <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_estado_cuenta(0,'.$validar_estatus.')">'.$titulo_btn.' <span><i class="fa fa-floppy-o"></i></span></button>
                        <button type="button" class="btn btn-outline-warning mr-1 mb-1 round" onclick="guardar_estado_cuenta(1,'.$validar_estatus.')">'.$titulo_btn.' y imprimir <span><i class="fa fa fa-file-text-o"></i></span></button>
                    </div>
                </div>';
        echo $html;    
    }

    function update_deposito(){  
        $id = $this->input->post('id');
        $deposito = $this->input->post('deposito');
        $saldo = $this->input->post('saldo');
        $data = array('deposito'=>$deposito,'saldo'=>$saldo);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$id);
    }

    function update_ruta(){  
        $id = $this->input->post('id');
        $idruta = $this->input->post('idruta');
        $data = array('idruta'=>$idruta);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$id);
    }

    function update_estado_cuenta(){  
        $id = $this->input->post('id');
        $rendimiento = $this->input->post('rendimiento');
        $saldo_final = $this->input->post('saldo_final');
        $data = array('rendimiento'=>$rendimiento,
            'saldo_final'=>$saldo_final,
            'estatus'=>1,
        );
        $this->ModeloCatalogos->updateCatalogo('estado_cuenta',$data,'id',$id);
    }

    public function addestado_cuenta()
    {    
        $idoperador=$this->input->post('idoperador');
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $whereu = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes);
        $result=$this->ModeloCatalogos->getselectrowwheren('estado_cuenta',$whereu);
        $idreg=0;
        foreach ($result->result() as $x){
            $idreg=$x->id;
        }
        $data = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes,'personalId'=>$this->idpersonal);
        if($idreg==0){
            $idreg=$this->ModeloCatalogos->Insert('estado_cuenta',$data);  
        }
        echo $idreg;
    }
    
    public function reporte_estados_cuenta($id)
    {    
        $data['get_info']=$this->ModeloEstadosDeCuenta->get_data_estado_cuenta($id);
        $data['get_info_detalle']=$this->ModeloEstadosDeCuenta->get_estado_cuenta($data['get_info']->idoperador,$data['get_info']->anio,$data['get_info']->mes);
        $this->load->view('reportes/reporte_estados_cuenta',$data);
    }
    
}
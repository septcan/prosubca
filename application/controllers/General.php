<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class General extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('encrypt');
    }
    function num_en(){
        $id = $this->input->post('id');
        $encry = $this->encrypt->encode($id);
        echo $encry;
    }
}
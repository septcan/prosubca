<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inicio extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('ModeloCatalogos');
    }
	public function index()	{
            $this->load->view('theme/header');
            $this->load->view('theme/navbar');          
            $this->load->view('theme/inicio');
            $this->load->view('theme/footer');  
	}   

    function update_alert(){  
        $id = $this->input->post('id');
        $data = array('estatus'=>1);
        $this->ModeloCatalogos->updateCatalogo('historial_alertas',$data,'id',$id);
    }

}
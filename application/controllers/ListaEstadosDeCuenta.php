<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaEstadosDeCuenta extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloEstadosDeCuenta');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,17);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/estadocuentas/index');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/estadocuentas/indexjs');   
    }

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloEstadosDeCuenta->List_table($params);
        $tablelistadorow=$this->ModeloEstadosDeCuenta->List_table_total($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function reporte_estados_cuenta($id)
    {    
        $data['get_info']=$this->ModeloEstadosDeCuenta->get_data_estado_cuenta($id);
        $data['get_info_detalle']=$this->ModeloEstadosDeCuenta->get_estado_cuenta($data['get_info']->idoperador,$data['get_info']->anio,$data['get_info']->mes);
        $this->load->view('reportes/reporte_estados_cuenta',$data);
    }

}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ListaRevision extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloRevision');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,12);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/revision/index');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/revision/indexjs');    
    }

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloRevision->List_table($params);
        $tablelistadorow=$this->ModeloRevision->List_table_total($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
}
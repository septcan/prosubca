<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloSession');
        $this->load->helper('url');
    }
	public function index()
	{
            $this->load->view('login/header');
            $this->load->view('login/index');          
            $this->load->view('login/footer');
            $this->load->view('login/pages-logintpl');
            
	}   
    public function session(){
        $usu = $this->input->post('usu');
        $pass = $this->input->post('passw');
        //$respuesta = $usu.' '.$pass;
        $respuesta = $this->ModeloSession->login($usu,$pass);
        echo $respuesta;
    }   
    public function exitlogin() {
        session_destroy();
        redirect('/Sistema');
    }
}
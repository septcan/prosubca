<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificaciones extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('encrypt');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloNotificacion');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->idpersonal=$this->session->userdata('idpersonal');
        }
    }
	public function index()	{
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('notificacion/index');
        $this->load->view('theme/footer');  
        $this->load->view('notificacion/indexjs');
	}   

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloNotificacion->List_table($params,$this->idpersonal);
        $tablelistadorow=$this->ModeloNotificacion->List_table_total($params,$this->idpersonal);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

}
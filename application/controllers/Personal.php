<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Personal extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,1);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

	function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/personal/index');
        $this->load->view('theme/footer');
        $this->load->view('catalogos/personal/indexjs');      
    }
    
	function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloPersonal->List_table($params,$this->perfilid,$this->sucursalId);
        $tablelistadorow=$this->ModeloPersonal->filastotal($params,$this->perfilid,$this->sucursalId);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
	}

    function registro($id=0){
        $data['get_perfiles']=$this->ModeloCatalogos->getselectall('perfiles');
        if($id!=0){
           $data['title']='Edición de';  
 
           $where = array('personalId'=>$id);
           $resutl = $this->ModeloCatalogos->getselectrowwheren('personal',$where);
           foreach ($resutl->result() as $item){
                $data['personalId']=$item->personalId;
                $data['nombre']=$item->nombre;
                $data['puesto']=$item->puesto;
                $data['unidad']=$item->unidad;
                $data['nss']=$item->nss;
                $data['curp']=$item->curp;
                $data['rfc']=$item->rfc;
                $data['correo']=$item->correo;
                $data['contrasenacorreo']=$item->contrasenacorreo;
                $data['celular']=$item->celular;
                $data['domicilio']=$item->domicilio;
                $data['beneficiario']=$item->beneficiario;
                $data['sexo']=$item->sexo;
                $data['estatus']=$item->estatus;
                $data['num_empleado']=$item->num_empleado;
                $data['fecha_ingreso']=$item->fecha_ingreso;
                $data['sueldo']=$item->sueldo;  
                $data['foto']=$item->foto;  
                $data['unidad_text']=$item->foto;  
                $data['acceso']=$item->acceso;
                $data['UsuarioID']=0;
                $data['perfilId']=0;
                $data['Usuario']='';
                $resutlusu = $this->ModeloCatalogos->getselectrowwheren('usuarios',$where);
                foreach ($resutlusu->result() as $usu){
                    $data['UsuarioID']=$usu->UsuarioID;
                    $data['perfilId']=$usu->perfilId;
                    $data['Usuario']=$usu->Usuario;
                }

                $data['unidad_text']='';
                $whereop = array('id'=>$item->unidad);
                $resutl = $this->ModeloCatalogos->getselectrowwheren('unidad',$whereop);
                foreach ($resutl->result() as $op) {
                    $data['unidad_text']=$op->placas;
                }
           }
        }else{
            $data['title']='Nuevo';  
            $data['personalId']=0;
            $data['nombre']='';
            $data['puesto']='';
            $data['unidad']=0;
            $data['nss']='';
            $data['curp']='';
            $data['rfc']='';
            $data['correo']='';
            $data['contrasenacorreo']='';
            $data['celular']='';
            $data['domicilio']='';
            $data['beneficiario']='';
            $data['sexo']='';
            $data['estatus']='';
            $data['num_empleado']='';
            $data['fecha_ingreso']='';
            $data['sueldo']='';
            $data['foto']='';
            $data['unidad_text']='';

            $data['acceso']=0;
            $data['UsuarioID']=0;
            $data['perfilId']=0;
            $data['Usuario']='';
            
        } 

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/personal/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('catalogos/personal/formjs');      
    }
    
    function addregistro(){
        $data = $this->input->post();
        $id = $data['personalId'];
        $idusu = $data['UsuarioID'];
        unset($data['personalId']);
        $datausu['Usuario']=$data['Usuario'];
        if(isset($data['perfilId'])){
            $datausu['perfilId']=$data['perfilId'];
        }
        if(isset($data['acceso'])){
            $data['acceso']=1;
            if(isset($data['contrasena'])){
                $pss_verifica = $data['contrasena'];
                $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
                $datausu['contrasena'] = $pass;
            }
        }else{
            $data['acceso']=0;
        }
        unset($data['UsuarioID']);
        unset($data['Usuario']);
        unset($data['perfilId']);
        unset($data['contrasena']);
        unset($data['contrasena2']);
        unset($data['contrasena']);
        unset($data['contrasena2']);
        $id_reg = 0;
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('personal',$data,'personalId',$id);
            $id_reg=$id;
        }else{
            $data['reg']=$this->fechahoy;
            $id_reg=$this->ModeloCatalogos->Insert('personal',$data);
        }
        if($data['acceso']==1){
            $datausu['personalId'] = $id_reg;
            if($idusu>=1){
                $this->ModeloCatalogos->updateCatalogo('usuarios',$datausu,'UsuarioID',$id);
            }else{
                $this->ModeloCatalogos->Insert('usuarios',$datausu);
            }
        }
        echo $id_reg;
    }

    function validar(){
        $usuario = $this->input->post('usuario');
        /*
        $where = array('usuario'=>$usuario);
        $resutl = $this->ModeloCatalogos->getselectrowwheren('proveedores',$where);
        $resultado=0;
        foreach ($resutl->result() as $row){
            $resultado=1;
        }
        */
        $where = array('Usuario'=>$usuario);
        $resutl=$this->ModeloCatalogos->getselectrowwheren('usuarios',$where);
        foreach ($resutl->result() as $row){
            $resultado=1;
        }
        echo $resultado;
    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('personal',$data,'personalId',$id);
    }

    function file_data(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
            $target_path = 'uploads/empleado';
            $thumb_path = 'uploads/empleado';

            //file name setup
            $filename_err = explode(".",$_FILES['img']['name']);
            $filename_err_count = count($filename_err);
            $file_ext = $filename_err[$filename_err_count-1];
            //if($file_name != ''){
               // $fileName = $file_name.'.'.$file_ext;
            //}else{
                $fileName = $_FILES['img']['name'];
            //}
            $fecha=date('ymd-His');
            //upload image path
            $cargaimagen =$fecha.'.'.$file_ext;
            $upload_image = $target_path.'/'.$cargaimagen;
            
            //upload image
            if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                $data['foto']=$cargaimagen;
                //thumbnail creation
                    //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                    $thumbnail = $thumb_path.'/'.$cargaimagen;
                    list($width,$height) = getimagesize($upload_image);
                    if ($width>3000) {
                        $thumb_width = $width/10;
                        $thumb_height = $height/10;
                    }elseif ($width>2500) {
                        $thumb_width = $width/7.9;
                        $thumb_height = $height/7.9;
                    }elseif ($width>2000) {
                        $thumb_width = $width/6.8;
                        $thumb_height = $height/6.8;
                    }elseif ($width>1500) {
                        $thumb_width = $width/5.1;
                        $thumb_height = $height/5.1;
                    }elseif ($width>1000) {
                        $thumb_width = $width/3.3;
                        $thumb_height = $height/3.3;
                    }elseif ($width>900) {
                        $thumb_width = $width/3;
                        $thumb_height = $height/3;
                    }elseif ($width>800) {
                        $thumb_width = $width/2.6;
                        $thumb_height = $height/2.6;
                    }elseif ($width>700) {
                        $thumb_width = $width/2.3;
                        $thumb_height = $height/2.3;
                    }elseif ($width>600) {
                        $thumb_width = $width/2;
                        $thumb_height = $height/2;
                    }elseif ($width>500) {
                        $thumb_width = $width/1.9;
                        $thumb_height = $height/1.9;
                    }elseif ($width>400) {
                        $thumb_width = $width/1.3;
                        $thumb_height = $height/1.3;
                    }elseif ($width>300) {
                        $thumb_width = $width/1.2;
                        $thumb_height = $height/1.2;
                    }else{
                        $thumb_width = $width;
                        $thumb_height = $height;
                    }
                    $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                    switch($file_ext){
                        case 'jpg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'jpeg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'png':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'PNG':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'gif':
                            $source = imagecreatefromgif($upload_image);
                            break;
                        default:
                            $source = imagecreatefromjpeg($upload_image);
                    }

                    imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                    switch($file_ext){
                        case 'jpg' || 'jpeg':
                            imagejpeg($thumb_create,$thumbnail,100);
                            break;
                        case 'png':
                            imagepng($thumb_create,$thumbnail,100);
                            break;

                        case 'gif':
                            imagegif($thumb_create,$thumbnail,100);
                            break;
                        default:
                            imagejpeg($thumb_create,$thumbnail,100);
                    }

                

                $return = Array('ok'=>TRUE,'img'=>'');
            }
            else{
                $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('personal',$data,'personalId',$id);
    }
    
    function search_unidad(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('unidad',$where,'id',$search);
        echo json_encode($results);    
    }

}
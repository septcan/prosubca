<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Productos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloPersonal');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloGeneral');
        $this->load->model('ModeloProducto');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,1);//  es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }
	public function index()	{
        $data["img"]='';
        $data["sucursalId"]=$this->sucursalId;
        $data["perfilid"]=$this->perfilid;
        if($this->perfilid==1){
           $where_sucu = array('activo'=>1);
        }else{
           $where_sucu = array('sucursalid'=>$this->sucursalId);
        }
		$data['sucursalesrow']=$this->ModeloCatalogos->getselectrowwheren('sucursales',$where_sucu);

		if ($this->perfilid==1) {
            $data['list_block']='';
        }else{
            $data['list_block']='style="display: none;"';
        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/producto/listproducto',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('catalogos/producto/jslistproducto');      
	} 
    public function addproducto($id_e=0,$tipo_e){
        $id = $this->encrypt->decode($id_e);
        $tipo = $this->encrypt->decode($tipo_e);
        if($this->perfilid==1){
           $where_sucu = array('activo'=>1);
        }else{
           $where_sucu = array('sucursalid'=>$this->sucursalId);
        }
        $data['sucursalesrow']=$this->ModeloCatalogos->getselectrowwheren('sucursales',$where_sucu);
        $data['get_categoria']=$this->ModeloCatalogos->getselectrowwheren('categoria',array('activo'=>1));
        $data['perfil']=$this->perfilid;
        $data['sucursalid_personal']=$this->sucursalId;
        
        ///$data['get_piso']=$this->ModeloCatalogos->getselectrowwheren('piso',array('activo'=>1));
        if($id!=0){
           if($tipo==1){
              $data['status']=0;
              $data['title']='Edición del'; 
           }else{
              $data['status']=1; 
              $data['title']='Visualización del'; 
           }

           $data['icon'] = 'fa fa-edit';
           $data['boton']='Editar';
           $where = array('idproducto'=>$id);
           $resutl = $this->ModeloCatalogos->getselectrowwheren('productos',$where);
           foreach ($resutl->result() as $item) {
                $data['idproducto']=$this->encrypt->encode($item->idproducto);
                $data['idpro']=$item->idproducto;
                $data['codigo']=$item->codigo;
                $data['nombre']=$item->nombre;
                $data['img']=$item->img;
                $data['descripcion']=$item->descripcion;
                $data['idcategoria']=$item->idcategoria;
                $data['preciocompra']=$item->preciocompra;
            }
        }else{
            $data['status']=0;
            $data['title']='Nuevo';  
            $data['icon'] = 'fa fa-plus-square';
            $data['boton']='Guardar';
         
            $data['idproducto']=0;
            $data['idpro']=0;
            $data['codigo']='';
            $data['nombre']='';
            $data['img']='';
            $data['descripcion']='';
            $data['idcategoria']=0;
            $data['preciocompra']=0;
        } 

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/producto/addproducto',$data);
        $this->load->view('theme/footer');
        $this->load->view('theme/script_datatable');
        $this->load->view('catalogos/producto/jsproducto');
    }
    public function registro(){
        $data = $this->input->post();
        $id_e = $data['idproducto'];
        $id = $this->encrypt->decode($id_e);
        unset($data['idproducto']);
        $aux = 0;
        $result=0;
        if($id>=1){
            $aux=1;
            $result=$this->ModeloCatalogos->updateCatalogo('productos',$data,'idproducto',$id);
        }else{
            $result=$this->ModeloCatalogos->Insert('productos',$data);
            $aux=0;
        }
        $arraydata = array('aux'=>$aux,'id'=>$result);
        echo json_encode($arraydata);
    } 
    function productostock(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) { 
            $idproducto = $DATA[$i]->idproducto;
            $idsucursal = $DATA[$i]->idsucursal;
            $precio_venta = $DATA[$i]->precio_venta;
            $existencia = $DATA[$i]->existencia;

            $datosp=$this->ModeloProducto->productosexistencias($idsucursal,$idproducto);
            $id=$datosp['idproducto_sucursal'];

            $data['idproducto']=$idproducto;
            $data['idsucursal']=$idsucursal;
            $data['precio_venta']=$precio_venta;
            $data['existencia']=$existencia;
            
            if ($id==0) {
                $this->ModeloCatalogos->Insert('productos_sucursales',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('productos_sucursales',$data,'idproducto_sucursal',$id);
            }
        }
    }
    function delete_registro(){  
        $id_e = $this->input->post('id');
        $id = $this->encrypt->decode($id_e);
        $data = array('activo'=>0,'id_persona_delete'=>$this->idpersonal,'reg_delete'=>$this->fechahoy);
        $this->ModeloCatalogos->updateCatalogo('productos',$data,'idproducto',$id);
    }
    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloProducto->List_table_productos($params,$this->perfilid,$this->sucursalId);
        $tablelistadorow = $this->ModeloProducto->filastotal_productos($params,$this->perfilid,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function add_img_producto(){
        $data = $this->input->post();
        $id = $data['idproducto'];
        unset($data['idproducto']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
                $target_path = 'public/imagen/producto';
                $thumb_path = 'public/imagen/producto';
                //file name setup
                $filename_err = explode(".",$_FILES['img']['name']);
                $filename_err_count = count($filename_err);
                $file_ext = $filename_err[$filename_err_count-1];
                //if($file_name != ''){
                   // $fileName = $file_name.'.'.$file_ext;
                //}else{
                    $fileName = $_FILES['img']['name'];
                //}
                $fecha=date('ymd-His');
                //upload image path
                $cargaimagen =$fecha.'cat'.basename($fileName);
                $upload_image = $target_path.'/'.$cargaimagen;
                
                //upload image
                if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                    $data['img']=$cargaimagen;
                    //thumbnail creation
                        //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                        $thumbnail = $thumb_path.'/'.$cargaimagen;
                        list($width,$height) = getimagesize($upload_image);
                        if ($width>3000) {
                            $thumb_width = $width/10;
                            $thumb_height = $height/10;
                        }elseif ($width>2500) {
                            $thumb_width = $width/7.9;
                            $thumb_height = $height/7.9;
                        }elseif ($width>2000) {
                            $thumb_width = $width/6.8;
                            $thumb_height = $height/6.8;
                        }elseif ($width>1500) {
                            $thumb_width = $width/5.1;
                            $thumb_height = $height/5.1;
                        }elseif ($width>1000) {
                            $thumb_width = $width/3.3;
                            $thumb_height = $height/3.3;
                        }elseif ($width>900) {
                            $thumb_width = $width/3;
                            $thumb_height = $height/3;
                        }elseif ($width>800) {
                            $thumb_width = $width/2.6;
                            $thumb_height = $height/2.6;
                        }elseif ($width>700) {
                            $thumb_width = $width/2.3;
                            $thumb_height = $height/2.3;
                        }elseif ($width>600) {
                            $thumb_width = $width/2;
                            $thumb_height = $height/2;
                        }elseif ($width>500) {
                            $thumb_width = $width/1.9;
                            $thumb_height = $height/1.9;
                        }elseif ($width>400) {
                            $thumb_width = $width/1.3;
                            $thumb_height = $height/1.3;
                        }elseif ($width>300) {
                            $thumb_width = $width/1.2;
                            $thumb_height = $height/1.2;
                        }else{
                            $thumb_width = $width;
                            $thumb_height = $height;
                        }
                        $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                        switch($file_ext){
                            case 'jpg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;
                            case 'jpeg':
                                $source = imagecreatefromjpeg($upload_image);
                                break;

                            case 'png':
                                $source = imagecreatefrompng($upload_image);
                                break;
                            case 'gif':
                                $source = imagecreatefromgif($upload_image);
                                break;
                            default:
                                $source = imagecreatefromjpeg($upload_image);
                        }

                        imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                        switch($file_ext){
                            case 'jpg' || 'jpeg':
                                imagejpeg($thumb_create,$thumbnail,100);
                                break;
                            case 'png':
                                imagepng($thumb_create,$thumbnail,100);
                                break;

                            case 'gif':
                                imagegif($thumb_create,$thumbnail,100);
                                break;
                            default:
                                imagejpeg($thumb_create,$thumbnail,100);
                        }

                    

                    $return = Array('ok'=>TRUE,'img'=>'');
                }
                else{
                    $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
                }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('productos',$data,'idproducto',$id);
    }
}    
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Proveedores extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelProveedor');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,3);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/proveedor/index');
        $this->load->view('theme/footer');
        $this->load->view('catalogos/proveedor/indexjs');      
    }

    public function registro($id=0){
        if($id!=0){
            $data['title']='Edición de';  
            $where = array('id_proveedor'=>$id);
            $resutl = $this->ModeloCatalogos->getselectrowwheren('proveedores',$where);
            foreach ($resutl->result() as $item) {
                $data['id_proveedor']=$item->id_proveedor;
                $data['nombre']=$item->nombre;
                $data['nombre_fiscal']=$item->nombre_fiscal;
                $data['direccion']=$item->direccion;
                $data['tipo_establecimiento']=$item->tipo_establecimiento;
                $data['telefono']=$item->telefono;
                $data['estatus']=$item->estatus;
                $data['contacto']=$item->contacto;
                $data['rfc']=$item->rfc;
                $data['estado']=$item->estado;
                $data['latitud']=$item->latitud;
                $data['longitud']=$item->longitud;
                $data['dias']=$item->dias; 
                $data['metodo_pago']=$item->metodo_pago; 
                /*
                $data['acceso']=$item->acceso;
                $data['usuario']=$item->usuario;
                $data['contrasena']=$item->contrasena;
                */
            }
        }else{
            $data['title']='Nuevo';  
            $data['id_proveedor']=0;
            $data['nombre']='';
            $data['nombre_fiscal']='';
            $data['direccion']='';
            $data['tipo_establecimiento']='';
            $data['telefono']='';
            $data['estatus']='';
            $data['contacto']='';
            $data['rfc']='';
            $data['estado']='';
            $data['latitud']='';
            $data['longitud']='';
            $data['dias']='';
            $data['metodo_pago']='';
            /*
            $data['acceso']='';
            $data['usuario']='';
            $data['contrasena']='';
            */
        } 

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/proveedor/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('catalogos/proveedor/formjs');     

    }


    function addregistro(){
        $data = $this->input->post();
        $id = $data['id_proveedor'];
        unset($data['id_proveedor']);
        /*
        if(isset($data['acceso'])){
            $data['acceso']=1;
            if(isset($data['contrasena'])){
                $pss_verifica = $data['contrasena'];
                $pass = password_hash($data['contrasena'], PASSWORD_BCRYPT);
                $data['contrasena'] = $pass;
            }
        }else{
            $data['acceso']=0;
        }
        unset($data['contrasena2']);
        */
        $aux = 0;
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('proveedores',$data,'id_proveedor',$id);
            $aux=$id;
        }else{
            $data['reg']=$this->fechahoy;
            $aux=$this->ModeloCatalogos->Insert('proveedores',$data);
        }
        echo $aux;
    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('proveedores',$data,'id_proveedor',$id);
    }
    
    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModelProveedor->List_table_proveedor($params);
        $tablelistadorow=$this->ModelProveedor->filastotal_proveedor($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
     
    /* 
    function validar(){
        $usuario = $this->input->post('usuario');
        $where = array('usuario'=>$usuario);
        $resutl = $this->ModeloCatalogos->getselectrowwheren('proveedores',$where);
        $resultado=0;
        foreach ($resutl->result() as $row){
            $resultado=1;
        }
        $where2 = array('Usuario'=>$usuario);
        $resutl2=$this->ModeloCatalogos->getselectrowwheren('usuarios',$where2);
        foreach ($resutl2->result() as $row){
            $resultado=1;
        }
        echo $resultado;
    }
    */
    function search_materia(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('materiaprima',$where,'nombre',$search);
        echo json_encode($results);    
    }
    
    function delete_registro_mp(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('proveedores_materiaprima',$data,'id',$id);
    }

    function registro_materiaprima(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {      
            $data['idproveedor']=$DATA[$i]->idproveedor;    
            $data['idmateriaprima']=$DATA[$i]->idmateriaprima;
            $data['precio_sin_factura']=$DATA[$i]->precio_sin_factura;
            $data['precio_con_factura']=$DATA[$i]->precio_con_factura;    
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('proveedores_materiaprima',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('proveedores_materiaprima',$data,'id',$DATA[$i]->id);
            }
        }
    }

    function get_tabla_material_prima()
    {
        $id = $this->input->post('id');
        $results=$this->ModelProveedor->get_material($id);
        echo json_encode($results);
    }

}
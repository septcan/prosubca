
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rendimientos extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->library('excel');
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloRendimientos');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,13);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/rendimiento/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/rendimiento/formjs');   
    }

    function search_operador(){
        $search = $this->input->get('search');
        $where = array('estatus'=>1,'puesto'=>2);
        $results=$this->ModeloCatalogos->getselectwherelike('personal',$where,'nombre',$search);
        echo json_encode($results);    
    }

    function search_unidad(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('unidad',$where,'placas',$search);
        echo json_encode($results);    
    }

    public function addrendimiento()
    {    
        $idoperador=$this->input->post('idoperador');
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $unidad=$this->input->post('unidad');
        $whereu = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes,'unidad'=>$unidad);
        $result=$this->ModeloCatalogos->getselectrowwheren('rendimiento',$whereu);
        $idreg=0;
        foreach ($result->result() as $u){
            $idreg=$u->id;
        }
        $data = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes,'unidad'=>$unidad,'personalId'=>$this->idpersonal);
        if($idreg==0){
            $idreg=$this->ModeloCatalogos->Insert('rendimiento',$data);  
        }
        echo $idreg;
    }

    function get_rendimiento_general()
    {   
        $idoperador=$this->input->post('idoperador');
        $anio=$this->input->post('anio');
        $mes=$this->input->post('mes');
        $unidad=$this->input->post('unidad');
        $whereu = array('id'=>$unidad);
        $result_u=$this->ModeloCatalogos->getselectrowwheren('unidad',$whereu);
        $result=$this->ModeloRendimientos->get_rendimiento($idoperador,$anio,$mes,$unidad);
        $result_total=$this->ModeloRendimientos->get_rendimiento_total($idoperador,$anio,$mes,$unidad);
        $result_km=$this->ModeloRendimientos->get_rendimiento_recorrido($idoperador,$anio,$mes,$unidad);
        $result_lts=$this->ModeloRendimientos->get_rendimiento_litros($idoperador,$anio,$mes,$unidad);
        $conbustible='';
        $rendimiento_n=0;


        $precio_litro=0;
        $km_recorridos_r=0;
        $wherer = array('idoperador'=>$idoperador,'anio'=>$anio,'mes'=>$mes,'unidad'=>$unidad);
        $result_r=$this->ModeloCatalogos->getselectrowwheren('rendimiento',$wherer);
        $idrend=0;
        $validar_estatus=0;
        foreach ($result_r->result() as $rd){
            $idrend=$rd->id;
            $precio_litro=$rd->precio_litros;
            $km_recorridos_r=$rd->km_recorridos;
            $validar_estatus=$rd->estatus;
        }
        
        $style_disable='';
        if($validar_estatus==1){
            $style_disable='disabled';
        }

        foreach ($result_u->result() as $u){
            if($u->tipo_combustible==1){
                $conbustible='Disel';
            }else if($u->tipo_combustible==2){
                $conbustible='Gas';
            }else if($u->tipo_combustible==3){
                $conbustible='Gasolina';
            }    
            $rendimiento_n=$u->rendimiento_esperado;
        }
        
        $validar_reg=0;
        foreach ($result_total as $xt) {
            $validar_reg=$xt->total;
        }

        if($validar_reg==0){
            $html='<h3>Sin registros</h3>';
        }else{
            $html='<div class="row">
                    <div class="col-sm-2">
                        <fieldset class="form-group">
                            <label>Combustible:</label>
                        </fieldset>
                    </div>
                    <div class="col-sm-3">
                        <h4>'.$conbustible.'</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <fieldset class="form-group">
                            <label>Litros $:</label>
                        </fieldset>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" id="precio_litro" value="'.$precio_litro.'" class="form-control round" oninput="edit_litros()" '.$style_disable.'>
                    </div>
                    <div class="col-sm-4">
                    </div>
                    <div class="col-sm-2">
                        <fieldset class="form-group">
                            <label>Rendimiento esperado:</label>
                        </fieldset>
                    </div>
                    <div class="col-sm-1">
                        <input type="hidden" id="rendimiento_n" value="'.$rendimiento_n.'"> 
                        <h4>'.$rendimiento_n.'</h4>
                    </div>
                </div><hr>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
                                <thead>
                                    <tr>
                                        <th>Ruta</th>
                                        <th>Fecha</th>
                                        <th>Fac.</th>
                                        <th>Vales</th>
                                        <th>Inicial</th>
                                        <th>Final</th>
                                        <th>Recorrido</th>
                                        <th>Litros</th>
                                        <th>Rendimiento</th>
                                        <th>Unidad</th>
                                    </tr>
                                </thead>
                                <tbody>';
                                    foreach ($result as $x){
                                        $total_ruta=0;
                                        $result=$this->ModeloRendimientos->total_ruta_bitacora_colecta($x->id);
                                        foreach ($result as $ru){
                                            $total_ruta=$ru->ruta; 
                                        }
                                        $kmin=$x->km_inicial/$total_ruta;
                                        $kmfi=$x->km_final/$total_ruta;
                                        $kmre=$x->km_recorrido/$total_ruta;
                                        $kmlt=$x->litros/$total_ruta;

                                        $rendimiento=$kmre/$kmlt;
                                        $rendimiento_decimal = number_format($rendimiento, 2, '.', ',');
                                        $html.='<tr>
                                            <td>'.$x->ruta.'</td>
                                            <td>'.date('d/m/Y',strtotime($x->dia)).'</td>
                                            <td>'.$x->numero_factura.'</td>
                                            <td>'.$x->num_vale.'</td>
                                            <td>'.number_format($kmin, 2, '.', ',').'</td>
                                            <td>'.number_format($kmfi, 2, '.', ',').'</td>
                                            <td>'.number_format($kmre, 2, '.', ',').'</td>
                                            <td>'.number_format($kmlt, 2, '.', ',').'</td>
                                            <td>'.$rendimiento_decimal.'</td>
                                            <td>'.$x->placas.'</td>
                                        </tr>';
                                    }
                                $html.='</tbody>
                                <tfoot>
                                    <tr>
                                        <th>Ruta</th>
                                        <th>Fecha</th>
                                        <th>Fac.</th>
                                        <th>Vales</th>
                                        <th>Inicial</th>
                                        <th>Final</th>
                                        <th>Recorrido</th>
                                        <th>Litros</th>
                                        <th>Rendimiento</th>
                                        <th>Unidad</th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>';


                $html.='<div class="row">
                                <div class="col-sm-12" align="center">
                                    <hr>
                                    <h4>RESUMEN GENERAL</h4>
                                </div>
                            </div>';
                $km_recorridott=0;
                foreach ($result_km as $km){
                    $km_recorridott=$km->km_recorrido;
                }
                
                $litros_consumidos=0;
                foreach ($result_lts as $kmlc){
                    $litros_consumidos=$kmlc->litros;
                }
                 
                if($km_recorridos_r!=0){
                    $km_recorridotts=$km_recorridos_r;
                }

                $total_rendimiento=number_format($km_recorridott, 2)/number_format($litros_consumidos, 2);  
                $litros_consumidos_esp=number_format($km_recorridott, 2)/number_format($rendimiento_n, 2);
                $litros_diferencia=number_format($litros_consumidos, 2)-number_format($litros_consumidos_esp, 2);
                $total_efectivo=number_format($litros_diferencia, 2)*$precio_litro;

                $html.='<div class="row">
                    <div class="col-sm-5">
                        <div class="row">
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>KM RECORRIDOS</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="number" id="km_recorridos" value="'.number_format($km_recorridott, 2).'" class="form-control round" oninput="edit_kmrecorridos()" '.$style_disable.'>
                                </fieldset>    
                            </div>  
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>LITROS CONSUMIDOS</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="hidden" id="litros_consumidos" value="'.number_format($litros_consumidos,2).'">
                                    <h4 class="litros_consumidos_tx">'.number_format($litros_consumidos, 2, '.', ',').'</h4>
                                </fieldset>    
                            </div>
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>RENDIMIENTO</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="hidden" id="total_rendimiento" value="'.$total_rendimiento.'">
                                    <h4 class="total_rendimiento_tx">'.number_format($total_rendimiento, 2, '.', ',').'</h4>
                                </fieldset>    
                            </div>  
                        </div>
                    </div>        
                    <div class="col-sm-2"></div>
                    <div class="col-sm-5">
                        <div class="row"> 
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>LITROS CONSUMIDOS ESPERADOS.</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="hidden" id="litros_consumidos_esp" value="'.$litros_consumidos_esp.'">
                                    <h4 class="litros_consumidos_esp_tx">'.number_format($litros_consumidos_esp, 2, '.', ',').'</h4>
                                </fieldset>    
                            </div>
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>LITROS DE DIFERENCIA</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="hidden" id="litros_diferencia" value="'.$litros_diferencia.'">
                                    <h4 class="litros_diferencia_tx">'.number_format($litros_diferencia, 2, '.', ',').'</h4>
                                </fieldset>    
                            </div>
                            <div class="col-sm-5">
                                <fieldset class="form-group">
                                    <label>TOTAL EFECTIVO</label>
                                </fieldset>
                            </div>
                            <div class="col-sm-7">
                                <fieldset class="form-group">
                                    <input type="hidden" id="total_efectivo" value="'.$total_efectivo.'">
                                    <h4 class="total_efectivo_tx">'.number_format($total_efectivo, 2, '.', ',').'</h4>
                                </fieldset>    
                            </div>  
                        </div>
                    </div>
                </div><hr>';
            $html.='<div class="row">
                    <div class="col-sm-5">';
                        if($validar_estatus==1){
                            $html.='<button type="button" class="btn btn-outline-info round mr-1 mb-1" onclick="editar_rendimiento()">Editar <span><i class="fa fa-edit"></i></span></button>
                            <button type="button" class="btn btn-outline-warning mr-1 mb-1 round" onclick="generar_rendimiento_pdf()">Imprimir <span><i class="fa fa fa-file-text-o"></i></span></button>';
                        }else{
                            $html.='<button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_rendimiento(0)">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
                            <button type="button" class="btn btn-outline-warning mr-1 mb-1 round" onclick="guardar_rendimiento(1)">Guardar y imprimir <span><i class="fa fa fa-file-text-o"></i></span></button>';
                        }
                    $html.='</div>
                </div>';
        }
        echo $html;    
    }

    function update_litros(){  
        $id = $this->input->post('id');
        $litros = $this->input->post('litros');
        $data = array('precio_litros'=>$litros);
        $this->ModeloCatalogos->updateCatalogo('rendimiento',$data,'id',$id);
    }

    function update_kmrecorrido(){  
        $id = $this->input->post('id');
        $km_recorridos = $this->input->post('km_recorridos');
        $data = array('km_recorridos'=>$km_recorridos);
        $this->ModeloCatalogos->updateCatalogo('rendimiento',$data,'id',$id);
    }

    function update_rendimiento(){  
        $id = $this->input->post('id');
        $precio_litro = $this->input->post('precio_litro');
        $rendimiento_n = $this->input->post('rendimiento_n');
        $km_recorridos = $this->input->post('km_recorridos');
        $litros_consumidos = $this->input->post('litros_consumidos');
        $total_rendimiento = $this->input->post('total_rendimiento');
        $litros_consumidos_esp = $this->input->post('litros_consumidos_esp');
        $litros_diferencia = $this->input->post('litros_diferencia');
        $total_efectivo  = $this->input->post('total_efectivo');
        $data = array('precio_litros'=>$precio_litro,
                'km_recorridos'=>$km_recorridos,
                'litros_consumidos'=>$litros_consumidos,
                'rendimiento'=>$total_rendimiento,
                'litros_consumidos_esp'=>$litros_consumidos_esp,
                'litros_diferencia'=>$litros_diferencia,
                'total_efectivo'=>$total_efectivo,
                'rendimiento_esperado'=>$rendimiento_n,
                'estatus'=>1,
            );
        $this->ModeloCatalogos->updateCatalogo('rendimiento',$data,'id',$id);
    }

    public function reporte_rendimiento($id)
    {    
        $data['get_rendimiento']=$this->ModeloRendimientos->get_data_rendimiento($id);
        $data['result_detalles']=$this->ModeloRendimientos->get_rendimiento($data['get_rendimiento']->idoperador,$data['get_rendimiento']->anio,$data['get_rendimiento']->mes,$data['get_rendimiento']->unidad);
        $this->load->view('reportes/reporte_rendimiento',$data);
    }

    function update_registro_rendimiento_estatus(){  
        $id = $this->input->post('id');
        $data = array('estatus'=>0);
        $this->ModeloCatalogos->updateCatalogo('rendimiento',$data,'id',$id);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Revision extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloRevision');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,11);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('operaciones/revision/form');
        $this->load->view('theme/footer');
        $this->load->view('operaciones/revision/formjs');   
        $this->load->view('operaciones/revision/modal');   
    }

    function search_operador(){
        $search = $this->input->get('search');
        $where = array('estatus'=>1,'puesto'=>2);
        $results=$this->ModeloCatalogos->getselectwherelike('personal',$where,'nombre',$search);
        echo json_encode($results);    
    }

    function search_ayudante(){
        $search = $this->input->get('search');
        $where = array('estatus'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('personal',$where,'nombre',$search);
        echo json_encode($results);    
    }

    function get_compras()
    {
        $dia = $this->input->post('dia');
        $operador = $this->input->post('operador');
        $idoperador = $this->input->post('idoperador');
        $result_rutas = $this->ModeloRevision->get_operador_rutas($idoperador,$dia);
        $result_compras = $this->ModeloRevision->get_operador_compras($idoperador,$dia);
        $result_gastos = $this->ModeloRevision->get_operador_gastos($idoperador,$dia);
        $result_compras_total = $this->ModeloRevision->get_operador_compras_total($idoperador,$dia);
        
        $result_bitacora_colecta_unidad = $this->ModeloRevision->get_bitacora_colecta_unida($idoperador,$dia);
        $result_bitacora_colecta = $this->ModeloRevision->get_bitacora_colecta($idoperador,$dia);
        $id_reg=0;
        $km_inicial=0;
        $km_final=0;
        $km_recorrido=0;
        $sueldo=0;
        $tarjeta_pase=0;
        $costo_flete=0;
        $placas='';
        $validar_estatus=0;
        foreach($result_bitacora_colecta as $bc){
            $id_reg=$bc->id;
            $km_inicial=$bc->km_inicial;
            $km_final=$bc->km_final;
            $km_recorrido=$bc->km_recorrido;
            $sueldo=$bc->sueldo;
            $tarjeta_pase=$bc->tarjeta_pase;
            $costo_flete=$bc->costo_flete;
            $placas=$bc->placas;
            $validar_estatus=$bc->estatus;
        }
        
        $validar_estatus_disabled='';
        if($validar_estatus==1){
            $validar_estatus_disabled='disabled';
        }
        
        $placa_unidad='';
        foreach($result_bitacora_colecta_unidad as $bcu){
            $placa_unidad=$bcu->placas; 
        }
        

        if($id_reg!=0){
            $html='<input type="hidden" id="idbitacora_colecta" value="'.$id_reg.'">';
            $html.='<h5>Unidad</h5>
                        <table class="table-striped table-bordered" id="datatable_rutas" style="width: 100%;"> 
                            <thead>
                                <tr>
                                    <th style="padding: 0.6rem 1rem;">REFERENCIA DE PLACAS</th>
                                    <th style="padding: 0.6rem 1rem;"><input class="form-control round" value="'.$placa_unidad.'" disabled></th>
                                    <th style="padding: 0.6rem 1rem;">PLACAS</th>
                                    <th style="padding: 0.6rem 1rem;"><input class="form-control round" id="unidad_colecta" value="'.$placas.'" disabled></th>
                                    <th style="padding: 0.6rem 1rem;">';
                                        if($validar_estatus==0){
                                            $html.='<a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="editar_unidad_general('.$id_reg.')"><b><i class="ft-edit"></i></b></a>';
                                        }
                                    $html.='</th>
                                </tr>
                            </thead>
                        </table>';
            $html.='<div class="row">
                    <div class="col-sm-12">
                        <h5>Rutas</h5> 
                        <table class="table table-striped table-bordered" id="datatable_rutas" style="width: 100%;"> 
                            <thead>
                                <tr>
                                    <th>RUTA</th>
                                    <th>REFERENCIA ORIGEN</th>
                                    <th></th>
                                    <th>UNIDAD</th>
                                    <th>REFERENCIA DE PLACAS POR RUTA</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>';
                            foreach($result_rutas as $ru){
                                $html.='<tr><td><span class="ruta_'.$ru->id.'">'.$ru->ruta.'</td><td>'.$ru->origen.'</td><td>';
                                    if($validar_estatus==0){
                                        $html.='<a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="editar_ruta('.$ru->id.')"><b><i class="ft-edit"></i></b></a>';
                                    }
                                    $html.='</td>
                                    <td><span class="unidad_t uni_'.$ru->id.'">'.$ru->placas_uni.'</td><td>'.$ru->placas.'</td><td>';
                                    if($validar_estatus==0){
                                        $html.='<a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="editar_unidad('.$ru->id.','.$ru->idbitacora_colecta.')"><b><i class="ft-edit"></i></b></a>
                                        </td>';
                                    }
                                    $html.='</tr>';
                            }
                        $html.='</tbody>
                        </table>
                    </div>
                    
            </div>';
            /*
            <div class="col-sm-4">
                        <h5>Unidad</h5>
                        <table class="table table-striped table-bordered" id="datatable_rutas" style="width: 100%;"> 
                            <thead>
                                <tr>
                                    <th colspan="2">REFERENCIA DE PLACAS</th>
                                </tr>
                                <tr>
                                    <td colspan="2"><input class="form-control round" value="'.$placa_unidad.'" disabled></td>
                                </tr>
                                <tr>
                                    <th colspan="2">PLACAS</th>
                                </tr>
                                <tr>
                                    <td><input class="form-control round" id="unidad_colecta" value="'.$placas.'" disabled></td>
                                    <td><a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="editar_unidad('.$id_reg.')"><b><i class="ft-edit"></i></b></a></td>
                                </tr>
                            </thead>
                        </table>
                    </div>
            */
            $html.='<div class="row">
                    <div class="col-sm-12">
                        <h5>COMPRAS</h5>
                        <table class="table table-striped table-bordered" id="datatable_compras" style="width: 100%;"> 
                            <thead>
                                <tr>
                                    <th>PROVEEDOR</th>
                                    <th>PRODUCTO</th>
                                    <th>SE FAC.</th>
                                    <th>FACTURA</th>
                                    <th>CANTIDAD</th>
                                    <th>PRECIO</th>
                                    <th>IMPORTE</th>
                                    <!--<th>VALIDAR quitar columna</th>-->
                                </tr>
                            </thead>
                            <tbody>';
                            $suma_compras=0;
                            $suma_kilos=0;
                            foreach($result_compras as $x){
                                $total_c=0;
                                $precio_c=0;
                                if($x->metodo_pago==2){
                                    $total_c=0;
                                    $precio_c=0;
                                    $suma_compras+=0; 
                                }else{
                                    $total_c=$x->total;
                                    $precio_c=$x->precio;
                                    $suma_compras+=$x->total; 
                                }
                                $select='<select class="form-control round" id="idfactura_c_'.$x->idbitacora_colecta_detalle_material.'" onchange="edit_factura('.$x->idbitacora_colecta_detalle_material.','.$x->metodo_pago.')" '.$validar_estatus_disabled.'>
                                    <option value="0"'; if($x->factura==0) $select.='selected'; $select.='>NO</option>
                                    <option value="1"'; if($x->factura==1) $select.='selected'; $select.='>SI</option>
                                </select>'; 
                                $html.='<tr><td>'.$x->nombre.'</td><td>'.$x->materia.'</td><td>'.$select.'</td><td><input type="text" class="form-control round" id="factura_c'.$x->idbitacora_colecta_detalle_material.'" value="'.$x->numero_factura.'" oninput="agregar_factura('.$x->idbitacora_colecta_detalle_material.')" '.$validar_estatus_disabled.'></td><td id="kilos_cantidad_'.$x->idbitacora_colecta_detalle_material.'">'.$x->kilos.'</td><td><span class="idcompra_'.$x->idbitacora_colecta_detalle_material.'">$'.number_format($precio_c,2,'.',',').'</span></td><td><input type="hidden" class="total_compras_c'.$x->idbitacora_colecta_detalle_material.'" id="total_compras_c" value="'.$total_c.'"><span id="total_importe_'.$x->idbitacora_colecta_detalle_material.'">$'.number_format($total_c,2,'.',',').'</span></td></tr>';
                                $suma_kilos+=$x->kilos;  
                            }
                            /*
                            <!--<td><div class="custom-switch custom-switch-success custom-control-inline mb-1 mb-xl-0">
                                    <input type="checkbox" class="custom-control-input" id="validar_compra'.$x->idbitacora_colecta_detalle_material.'" onclick="check_validar_compra('.$x->idbitacora_colecta_detalle_material.')" '; if($x->validar==1){$html.='checked';} $html.='>
                                    <label class="custom-control-label mr-1" for="validar_compra'.$x->idbitacora_colecta_detalle_material.'">
                                        <span></span>
                                    </label>
                                </div></td>-->
                            */
                            $html.='</tbody>
                        </table>
                        <input type="hidden" id="suma_compras" value="'.$suma_compras.'">
                    </div>
                </div>
                ';   
                $html.='<div class="row">
                    <div class="col-sm-12">
                        <h5>GASTOS  </h5>
                        <table class="table table-striped table-bordered" id="datatable_gasto" style="width: 100%;"> 
                            <thead>
                                <tr>
                                    <th>FECHA</th>
                                    <th>DESCRIPCIÓN</th>
                                    <th>FACTURA</th>
                                    <th>EDITAR GASTO</th>
                                    <th>TOTAL</th>
                                    <th>VALIDAR</th>
                                </tr>
                            </thead>
                            <tbody>';
                                $suma_gastos=0;
                                foreach($result_gastos as $g){
                                    /// validar si es psc
                                    $total='$0.00';
                                    //if($g){
                                        $total='$'.number_format($g->importe,2,'.',',');
                                    //}else{

                                    //}
                                    $html.='<tr>
                                        <td>'.date('d/m/Y',strtotime($g->dia)).'</td>
                                        <td>'.$g->categoria.'</td>
                                        <td><input type="text" class="form-control round" id="factura_g'.$g->idgastos.'" value="'.$g->numero_factura.'" oninput="agregar_factura_gastos('.$g->idgastos.')" '.$validar_estatus_disabled.'></td>
                                        <td><input type="number" class="form-control round" id="editargasto_'.$g->idgastos.'" oninput="editar_gasto('.$g->idgastos.')" value="'.$g->importe_edicion.'" '.$validar_estatus_disabled.'><input type="hidden" class="total_gasto_int_'.$g->idgastos.'" id="total_gasto" value="'.$g->importe.'"></td>
                                        <td><span class="total_gasto_'.$g->idgastos.'">'.$total.'</span></td>
                                        <td><div class="custom-switch custom-switch-success custom-control-inline mb-1 mb-xl-0">
                                            <input type="checkbox" class="custom-control-input" id="validar_gatos'.$g->idgastos.'" onclick="check_validar_gasto('.$g->idgastos.')" '; if($g->validar==1){$html.='checked';} $html.=' '.$validar_estatus_disabled.'>
                                            <label class="custom-control-label mr-1" for="validar_gatos'.$g->idgastos.'">
                                                <span></span>
                                            </label>
                                        </div></td>
                                    </tr>'; 
                                    $suma_gastos+=$g->importe;
                                }
                            $html.='</tbody>
                        </table>
                    </div>
                </div>';  
                $suma_compras_gastos=$suma_compras+$suma_gastos;
                $html.='<div class="row">
                    <div class="col-sm-3">
                        <h5>Total compras: <span class="total_compras">$'.number_format($suma_compras,2,'.',',').'</span></h5>
                    </div>
                    <div class="col-sm-3">
                        <input type="hidden" id="suma_gastos" value="'.$suma_gastos.'">
                        <h5>Total gastos: <span class="text_gatos_global">$'.number_format($suma_gastos,2,'.',',').'</span></h5>
                    </div>
                </div>
                <input type="hidden" id="compras_gastos" value="'.$suma_compras_gastos.'">
                <input type="hidden" id="total_kilos" value="'.$suma_kilos.'">';
                if($validar_estatus==0){
                $html.='<div class="row">
                        <div class="col-sm-3">
                            <fieldset class="form-group">
                                <label>Tipo de vale</label>
                                <select class="form-control round" id="tipo_gas" style="width: 100%">
                                    <option value="0" disabled="" selected="">Selecciona opción</option>
                                    <option value="1">Gas</option>
                                    <option value="2">Diésel</option>
                                    <option value="3">Gasolina</option>
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-sm-3">
                            <fieldset class="form-group">
                                <label>Número de vale</label>
                                <input type="number" id="tipo_vale" class="form-control round">
                            </fieldset>
                        </div>
                        <div class="col-sm-3">
                            <fieldset class="form-group">
                                <label>Litros</label>
                                <input type="number" id="litros_gas" class="form-control round">
                            </fieldset>
                        </div>
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Precio</label>
                                <input type="number" id="precio" class="form-control round">
                            </fieldset>
                        </div>
                        <div class="col-sm-1">
                            <fieldset class="form-group">
                                <label></label><br>
                                <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" id="btn_add_gas" title="Agregar" onclick="add_gas()"><b><i class="ft-plus"></i></b></a>
                            </fieldset>
                        </div>
                    </div>';
                }
                    
                $html.='<div class="text_tabla_gas">
                    <div class="row">
                        <div class="col-sm-12"><table class="table table-striped table-bordered" id="total_vale_gas" style="width: 100%;"> 
                        <thead>
                            <tr>
                                <th>Tipo de vale</th>
                                <th>Número de vale</th>
                                <th>Litros</th>
                                <th>Precio</th>
                                <th>Importe</th>
                                <th></th>
                            </tr>
                        </thead>';
                        $where = array('idbitacora_colecta'=>$id_reg,'activo'=>1);
                        $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_vale',$where);
                        $total_vale=0;
                        foreach ($result->result() as $x){
                            $tipo='';
                            if($x->tipo_vale==1){
                                $tipo='Gas';
                            }else if($x->tipo_vale==2){
                                $tipo='Diésel';
                            }else if($x->tipo_vale==3){
                                $tipo='Gasolina';
                            }
                            $total_vale+=$x->importe;
                            $html.='<tr class="row_vale_'.$x->id.'">
                                <td>'.$tipo.'</td>
                                <td>'.$x->num_vale.'</td>
                                <td>'.$x->litros.'</td>
                                <td>$'.number_format($x->precio,2,'.',',').'</td>
                                <td><input type="hidden" id="importe_gas" value="'.$x->importe.'">$'.number_format($x->importe,2,'.',',').'</td>
                                <td>';
                                if($validar_estatus==0){
                                    $html.='<a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_gasto_vale('.$x->id.')"><b><i class="ft-trash-2"></i></b></a>';
                                }
                                $html.='</td>
                            </tr>';
                        }
                        $html.='<tbody>
                        </tbody>
                    </table>
                    </div></div>
                    <div class="row">
                        <div class="col-sm-4">
                            <input type="hidden" id="total_vale" value="'.$total_vale.'">
                            <h4>Total: <span class="total_vale_text">$'.number_format($total_vale,2,'.',',').'</span></h4>
                        </div>
                    </div>
                </div>';

                $html.='<div class="row">
                            <div class="col-sm-12">
                                <h5>Sueldo de operador</h5>
                                <table class="table table-striped table-bordered" style="width: 100%;"> 
                                    <thead>
                                        <tr>
                                            <td><b>Operador</b></td>
                                            <td>'.$operador.'</td>
                                            <td><b>Sueldo</b></td>
                                            <td><input type="hidden" id="sueldoope" value="'.$sueldo.'">$'.number_format($sueldo,2,'.',',').'</td>
                                        </tr>
                                    </thead>
                                </table>    
                            </div>
                        </div>';    
                if($validar_estatus==0){        
                $html.='<h5>Agregar ayudante</h5><div class="row">
                        <div class="col-sm-5">
                            <fieldset class="form-group">
                                <label>Ayudante</label>
                                <select class="form-control round" id="idayudante" style="width: 100%">
                                </select>
                            </fieldset>
                        </div>
                        <div class="col-sm-1">
                            <fieldset class="form-group">';
                            
                                $html.='<label></label><br>
                                <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" id="btn_add_ayudante" title="Agregar" onclick="add_ayudante()"><b><i class="ft-plus"></i></b></a>';
                
                            $html.='</fieldset>
                        </div>
                    </div>';
                }
                $html.='<div class="text_tabla_ayudante">
                    <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-striped table-bordered" id="tabla_ayudante" style="width: 100%;"> 
                                <thead>
                                    <tr>
                                        <th>Ayudante</th>
                                        <th>Sueldo</th>
                                    </tr>
                                </thead>';

                                $sueldoayu=0;
                                $where = array('idbitacora_colecta'=>$id_reg,'activo'=>1);
                                $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_ayudante',$where);
                                foreach ($result->result() as $x){
                                    $get_personal=$this->ModeloCatalogos->getselectrowwheren_row('personal',array('personalId'=>$x->personalId));
                                    $html.='<tr class="row_ayudante_'.$x->id.'">
                                        <td>'.$get_personal->nombre.'</td>
                                        <td>$'.number_format($x->sueldo,2,'.',',').'</td>
                                        <td>';
                                        if($validar_estatus==0){
                                            $html.='<a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_ayudante('.$x->id.')"><b><i class="ft-trash-2"></i></b></a></td>';
                                        }
                                    $html.='</tr>';
                                    $sueldoayu+=$x->sueldo;
                                }
                                $html.='<tbody>
                                </tbody>
                            </table><input type="hidden" id="sueldoayudante" value="'.$sueldoayu.'">
                        </div>
                    </div>
                </div>';
                $html.='<hr>    
                    <div class="row">
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Kilómetraje inicial</label>
                                <input type="number" id="km_inicial" value="'.$km_inicial.'" class="form-control round" oninput="calcular_km()" '.$validar_estatus_disabled.'>
                            </fieldset>
                        </div>
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Kilómetraje final</label>
                                <input type="number" id="km_final" value="'.$km_final.'" class="form-control round" oninput="calcular_km()" '.$validar_estatus_disabled.'>
                            </fieldset>
                        </div>
                        <div class="col-sm-3">
                            <fieldset class="form-group">
                                <label>Kilómetraje recorridos</label><br>
                                <h4 id="km_recorrido">'.$km_recorrido.'</h4>
                            </fieldset>
                        </div>
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Tarjeta pase</label>
                                <input type="number" id="tarjeta_pase" value="'.$tarjeta_pase.'" class="form-control round" oninput="calculo_flete()" '.$validar_estatus_disabled.'>
                            </fieldset>
                        </div>
                        <div class="col-sm-2">
                            <fieldset class="form-group">
                                <label>Costo de flete</label><br>
                                <h4 id="costo_flete">$'.number_format($costo_flete,2,'.',',').'</h4>
                            </fieldset>
                        </div>
                    </div>';
                $html.='<hr>    
                    
                    <div class="row">
                        <div class="col-sm-3">
                            <h5>Total compras y gastos</h5>
                            <h4 id="totalcomprasgastos">$'.number_format($suma_compras_gastos,2,'.',',').'</h4>
                        </div>
                        <div class="col-sm-5">
                            <h5>Total materia prima</h5>';
                            $html.='<table class="table m-0" id="datatable" style="width: 100%;"> 
                                    <thead>';
                            $total_kilos_materia=0;        
                            foreach ($result_compras_total as $xt){
                                $html.='<tr>
                                    <th width="70%" style="font-size: 14px;">'.$xt->materia.'</th>
                                    <th width="30" style="font-size: 14px;">'.$xt->kilos.'</th>
                                </tr>';
                                $total_kilos_materia+=$xt->kilos;
                            }
                            $html.='<tr>
                                    <th width="70%" style="font-size: 15px;">Kilos totales</th>
                                    <th width="30" style="font-size: 15px;">'.$total_kilos_materia.'</th>
                                </tr>';
                            $html.='</thead>
                                </table>';
                        $html.='</div>
                        <div class="col-sm-4" align="right">';
                            if($validar_estatus==0){
                                $html.='<button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_colecta('.$id_reg.')">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
                                <button type="button" class="btn btn-outline-warning mr-1 mb-1 round" onclick="guardar_colecta('.$id_reg.',1)">Guardar y imprimir <span><i class="fa fa fa-file-text-o"></i></span></button>';
                            }else{
                                $html.='<button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="editar_colecta('.$id_reg.')">Editar <span><i class="fa fa-edit"></i></span></button>
                                <button type="button" class="btn btn-outline-warning mr-1 mb-1 round" onclick="guardar_colecta('.$id_reg.',2)">Imprimir <span><i class="fa fa fa-file-text-o"></i></span></button>';
                            }
                        $html.='</div>
                    </div>';
        }else{
            $html='<h3>Sin registros</h3>';
        }
        echo $html;
    }

    function add_tipo_vale()
    {
        $tipo=$this->input->post('tipo');
        $vale=$this->input->post('vale');
        $litros=$this->input->post('litros');
        $precio=$this->input->post('precio');
        $idbitacora_colecta=$this->input->post('idbitacora_colecta');
        $mult=$litros*$precio;
        $data = array('idbitacora_colecta'=>$idbitacora_colecta,
            'tipo_vale'=>$tipo,
            'num_vale'=>$vale,
            'litros'=>$litros,
            'precio'=>$precio,
            'importe'=>$mult,
            'personalId'=>$this->idpersonal);
        $this->ModeloCatalogos->Insert('bitacora_vale',$data);
    }
    
    function get_tabla_gas()
    {   
        $idbitacora_colecta=$this->input->post('idbitacora_colecta');
        $html='';
        $html.='<div class="row">
            <div class="col-sm-12"><table class="table table-striped table-bordered"id="total_vale_gas" style="width: 100%;"> 
            <thead>
                <tr>
                    <th>Tipo de vale</th>
                    <th>Número de vale</th>
                    <th>Litros</th>
                    <th>Precio</th>
                    <th>Importe</th>
                    <th></th>
                </tr>
            </thead>';
            $where = array('idbitacora_colecta'=>$idbitacora_colecta,'activo'=>1);
            $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_vale',$where);
            $total_vale=0;
            foreach ($result->result() as $x){
                $tipo='';
                if($x->tipo_vale==1){
                    $tipo='Gas';
                }else if($x->tipo_vale==2){
                    $tipo='Diésel';
                }else if($x->tipo_vale==3){
                    $tipo='Gasolina';
                }
                $total_vale+=$x->importe;
                $html.='<tr class="row_vale_'.$x->id.'">
                    <td>'.$tipo.'</td>
                    <td>'.$x->num_vale.'</td>
                    <td>'.$x->litros.'</td>
                    <td>$'.number_format($x->precio,2,'.',',').'</td>
                    <td><input type="hidden" id="importe_gas" value="'.$x->importe.'">$'.number_format($x->importe,2,'.',',').'</td>
                    <td><a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_gasto_vale('.$x->id.')"><b><i class="ft-trash-2"></i></b></a></td>
                </tr>';
            }
            $html.='<tbody>
            </tbody>
        </table>
        </div></div>
        <div class="row">
            <div class="col-sm-4">
                <input type="hidden" id="total_vale" value="'.$total_vale.'">
                <h4>Total: <span class="total_vale_text">$'.number_format($total_vale,2,'.',',').'</span></h4>
            </div>
        </div>';
        echo $html;
    }

    function delete_registro_vale(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_vale',$data,'id',$id);
    }

    function update_registro_factura(){  
        $id = $this->input->post('id');
        $validar_factura = $this->input->post('validar_factura');
        $precio=0;
        $total=0;
        $result=$this->ModeloRevision->get_precio_factura($id);
        foreach ($result as $x){
            if($validar_factura==1){
                $precio=$x->precio_con_factura;
                $total=$x->kilos*$x->precio_con_factura;
            }else{
                $precio=$x->precio_sin_factura;
                $total=$x->kilos*$x->precio_sin_factura;

            }
        }
        
        $data = array('factura'=>$validar_factura,'precio'=>$precio,'total'=>$total);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle_material',$data,'id',$id);
        
        echo $precio;
    }

    function update_agregar_factura(){  
        $id = $this->input->post('id');
        $factura = $this->input->post('factura');
        $data = array('numero_factura'=>$factura);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle_material',$data,'id',$id);
    }

    function update_validar_check(){  
        $id = $this->input->post('id');
        $validar = $this->input->post('validar');
        $data = array('validar'=>$validar);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle_material',$data,'id',$id);
    }

    function update_agregar_factura_gasto(){  
        $id = $this->input->post('id');
        $factura = $this->input->post('factura');
        $data = array('numero_factura'=>$factura);
        $this->ModeloCatalogos->updateCatalogo('bitacora_gastos',$data,'id',$id);
    }

    function update_validar_gasto_check(){  
        $id = $this->input->post('id');
        $validar = $this->input->post('validar');
        $data = array('validar'=>$validar);
        $this->ModeloCatalogos->updateCatalogo('bitacora_gastos',$data,'id',$id);
    }

    function update_gasto(){  
        $id = $this->input->post('id');
        $gasto = $this->input->post('gasto');
        $data = array('importe'=>$gasto,'importe_edicion'=>$gasto);
        $this->ModeloCatalogos->updateCatalogo('bitacora_gastos',$data,'id',$id);
    }

    function update_kilometraje(){  
        $idbitacora_colecta = $this->input->post('idbitacora_colecta');
        $km_final = $this->input->post('km_final');
        $km_inicial = $this->input->post('km_inicial');
        $km_recorrido = $this->input->post('km_recorrido');
        $data = array('km_final'=>$km_final,'km_inicial'=>$km_inicial,'km_recorrido'=>$km_recorrido);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$idbitacora_colecta);
    }

    function update_tarjeta_pase_costo_flete(){  
        $tarjeta_pase = $this->input->post('tarjeta_pase');
        $costo_flete = $this->input->post('costo_flete');
        $idbitacora_colecta = $this->input->post('idbitacora_colecta');
        $data = array('tarjeta_pase'=>$tarjeta_pase,'costo_flete'=>$costo_flete);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$idbitacora_colecta);
    }

    function add_ayudaten_colecta()
    {
        $idbitacora_colecta=$this->input->post('idbitacora_colecta');
        $idayudante=$this->input->post('idayudante');
        $sueldo=0;
        $result_ope=$this->ModeloCatalogos->getselectrowwheren('personal',array('personalId'=>$idayudante));
        foreach ($result_ope->result() as $op){
            $sueldo=$op->sueldo;
        }
        $data = array('idbitacora_colecta'=>$idbitacora_colecta,
            'personalId'=>$idayudante,
            'sueldo'=>$sueldo);
        $this->ModeloCatalogos->Insert('bitacora_ayudante',$data);
    }


    function get_tabla_ayudante()
    {   
        $idbitacora_colecta=$this->input->post('idbitacora_colecta');
        $html='';
        $html.='<div class="row">
            <div class="col-sm-12"><table class="table table-striped table-bordered" id="tabla_ayudante" style="width: 100%;"> 
            <thead>
                <tr>
                    <th>Ayudante</th>
                    <th>Sueldo</th>
                </tr>
            </thead>';
            $where = array('idbitacora_colecta'=>$idbitacora_colecta,'activo'=>1);
            $result=$this->ModeloCatalogos->getselectrowwheren('bitacora_ayudante',$where);
            $sueldoayu=0;
            foreach ($result->result() as $x){
                $get_personal=$this->ModeloCatalogos->getselectrowwheren_row('personal',array('personalId'=>$x->personalId));
                $html.='<tr class="row_ayudante_'.$x->id.'">
                    <td>'.$get_personal->nombre.'</td>
                    <td><input type="hidden" id="sueldoayudante_t" value="'.$x->sueldo.'">$'.number_format($x->sueldo,2,'.',',').'</td>
                    <td><a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="modaldelete_ayudante('.$x->id.')"><b><i class="ft-trash-2"></i></b></a></td>
                </tr>';
                $sueldoayu+=$x->sueldo;
            }
            $html.='<tbody>
            </tbody>
        </table><input type="hidden" id="sueldoayudante" value="'.$sueldoayu.'">
        </div></div>';
        echo $html;
    }

    function delete_registro_ayudante(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_ayudante',$data,'id',$id);
    }

    function search_ruta(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('rutas',$where,'ruta',$search);
        echo json_encode($results);    
    }

    function update_registro_ruta(){  
        $id = $this->input->post('id_colecta_detalle');
        $idruta = $this->input->post('idruta');
        $data = array('ruta'=>$idruta);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',$data,'id',$id);
    }

    function search_unidad(){
        $search = $this->input->get('search');
        $where = array('activo'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('unidad',$where,'placas',$search);
        echo json_encode($results);    
    }
    
    function update_registro_unidad_colecta(){  
        $id = $this->input->post('id_colecta');
        $idunidad = $this->input->post('idunidad');
        $data = array('idunidad'=>$idunidad);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$id);
    }

    function update_registro_unidad_colecta_detalle(){  
        $id = $this->input->post('id_colecta');
        $idunidad = $this->input->post('idunidad');
        $val_uni = $this->input->post('val_uni');
        $idcolecta = $this->input->post('id_colecta');
        $data = array('idunidad'=>$idunidad);
        if($val_uni==1){
            $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',$data,'idbitacora_colecta',$idcolecta);
        }else{
            $this->ModeloCatalogos->updateCatalogo('bitacora_colecta_detalle',$data,'id',$id);
        }
        
    }

    function update_registro_bitacora_colecta(){  
        $id = $this->input->post('id');
        $total_compras = $this->input->post('total_compras');
        $total_gastos = $this->input->post('total_gastos');
        $total_compras_gastos = $this->input->post('total_compras_gastos');
        $total_kilos = $this->input->post('total_kilos');
        $data = array('estatus'=>1,'total_compras'=>$total_compras,'total_gastos'=>$total_gastos,'total_compras_gastos'=>$total_compras_gastos,'total_kilos'=>$total_kilos);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$id);
        $this->alerta_operador($id);
    }

    function alerta_operador($id){  
        $result=$this->ModeloRevision->get_operador_gastos_reporte($id);
        foreach ($result as $x){
            if($x->importe_edicion!=0){
                $concepto='Tipo de gasto: '.$x->categoria.' el gasto es de $'.number_format($x->importe_historial,2,'.',',').' y se modifico a $'.number_format($x->importe_edicion,2,'.',',');
                $data = array('idbitacora_gastos'=>$x->idgastos,'concepto'=>$concepto,'idoperador'=>$x->idoperador,'dia'=>$x->dia,'submenu'=>11,'tipo'=>'Gasto');
                $this->ModeloCatalogos->Insert('historial_alertas',$data);      
            }
        }
    }

    public function reporte_revision($id)
    {   
         
        $result=$this->ModeloRevision->get_bitacora_colecta_reporte($id);
        foreach ($result as $op){
            $data['operador']=$op->nombre;
            $data['dia']=date('d/m/Y',strtotime($op->dia));    
            $data['unidad']=$op->placas;
            $data['sueldo']=$op->sueldo;
            $data['km_inicial']=$op->km_inicial;
            $data['km_final']=$op->km_final;
            $data['km_recorrido']=$op->km_recorrido;
            $data['tarjeta_pase']=$op->tarjeta_pase;
            $data['costo_flete']=$op->costo_flete;
        }
        $where = array('idbitacora_colecta'=>$id,'activo'=>1);
        $result_vale=$this->ModeloCatalogos->getselectrowwheren('bitacora_vale',$where);
        $whereay = array('idbitacora_colecta'=>$id,'activo'=>1);
        $data['result_compra']=$this->ModeloRevision->get_operador_compras_reporte($id);
        $data['result_gasto']=$this->ModeloRevision->get_operador_gastos_reporte($id);
        $data['result_vale']=$result_vale;
        $data['result_ayudante']=$this->ModeloCatalogos->getselectrowwheren('bitacora_ayudante',$whereay);
        $data['result_compras_total']=$this->ModeloRevision->get_operador_compras_total_reporte($id);
        $this->load->view('reportes/reporte_revision',$data);
    }

    function update_registro_colecta_estatus(){  
        $id = $this->input->post('id');
        $data = array('estatus'=>0);
        $this->ModeloCatalogos->updateCatalogo('bitacora_colecta',$data,'id',$id);
    }


}
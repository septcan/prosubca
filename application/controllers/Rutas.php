<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rutas extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModelRuta');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,7);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/rutas/index');
        $this->load->view('theme/footer');
        $this->load->view('catalogos/rutas/indexjs');  
        $this->load->view('catalogos/rutas/modal');       
    }

    function registro($id=0)
    {   
        if($id!=0){
            $data['title']='Edición de';  
            $where = array('id'=>$id);
            $resutl = $this->ModeloCatalogos->getselectrowwheren('rutas',$where);
            foreach ($resutl->result() as $item) {
                $data['id']=$item->id;
                $data['ruta']=$item->ruta;
            }
        }else{
            $data['title']='Nueva';  
            $data['id']=0;
            $data['ruta']='';
        } 
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/rutas/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('catalogos/rutas/formjs');  
    }

/*    function search_operador(){
        $search = $this->input->get('search');
        $where = array('activo'=>1,'puesto'=>2);
        $results=$this->ModeloCatalogos->getselectwherelike('personal',$where,'nombre',$search);
        echo json_encode($results);    
    }
*/
    function addregistro(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('rutas',$data,'id',$id);
        }else{
            $data['reg']=$this->fechahoy;
            $data['idpersonal']=$this->idpersonal;
            $id=$this->ModeloCatalogos->Insert('rutas',$data);
        }
        echo $id;
    }

    /*function search_proveedor(){
        $search = $this->input->get('search');
        $where = array('activo'=>1,'estatus'=>1);
        $results=$this->ModeloCatalogos->getselectwherelike('proveedores',$where,'nombre',$search);
        echo json_encode($results);    
    }*/

    /*function delete_proveedor(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('proveedores_materiaprima',$data,'id',$id);
    }*/

    /*function registro_proveedores(){
        $datos = $this->input->post('data');
        $DATA = json_decode($datos);
        for ($i=0;$i<count($DATA);$i++) {      
            $data['idruta']=$DATA[$i]->idruta;    
            $data['idproveedor']=$DATA[$i]->idproveedor;    
            if($DATA[$i]->id==0){
                $this->ModeloCatalogos->Insert('rutas_detalles',$data);
            }else{
                $this->ModeloCatalogos->updateCatalogo('rutas_detalles',$data,'id',$DATA[$i]->id);
            }
        }
    }*/

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModelRuta->List_table($params);
        $tablelistadorow=$this->ModelRuta->filastotal($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('rutas',$data,'id',$id);
    }

    /*public function get_tabla_rutas()
    {   
        $id = $this->input->post('id');
        $html='<table class="table table-striped table-bordered" id="datatable_expedientes" style="width: 100%;"> 
            <thead>
                <tr>
                    <th>Razón social</th>
                    <th>Dirección</th>
                    <th>Teléfono</th>
                </tr>
            </thead>
            <tbody>';
                $resutl = $this->ModelRuta->get_rutas($id);
                foreach ($resutl as $x) {
                    $html.='<tr class="row_exp_'.$x->id.'"><td>'.$x->nombre.'</td><td>'.$x->direccion.'</td><td>'.$x->telefono.'</td></tr>';
                }
            $html.='</tbody>
        </table>';
        echo $html;
    }*/

    /*function get_tabla_rutas_form()
    {
        $id = $this->input->post('id');
        $results=$this->ModelRuta->get_rutas($id);
        echo json_encode($results);
    }*/
    
}
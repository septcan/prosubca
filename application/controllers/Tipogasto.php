<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tipogasto extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloTipogasto');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,10);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/tipogasto/index');
        $this->load->view('theme/footer');
        $this->load->view('catalogos/tipogasto/indexjs');   
    }

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloTipogasto->List_table($params);
        $tablelistadorow=$this->ModeloTipogasto->List_table_total($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    
    function addregistro(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        $id_reg=0;
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('categoria',$data,'id',$id);
            $id_reg=$id;
        }else{
            $data['reg']=$this->fechahoy;
            $id_reg=$this->ModeloCatalogos->Insert('categoria',$data);
        }
        echo $id_reg;
    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('categoria',$data,'id',$id);
    }

}

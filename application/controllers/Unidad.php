<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unidad extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloCatalogos');
        $this->load->model('ModeloUnidad');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            $this->idpersonal=$this->session->userdata('idpersonal');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,5);// idperfil y id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
        date_default_timezone_set('America/Mexico_City');
        $this->fechahoy = date('Y-m-d G:i:s');
        $this->fechaactual = date('Y-m-d');
    }

    function index(){
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/unidad/index');
        $this->load->view('theme/footer');
        $this->load->view('catalogos/unidad/indexjs');   
        $this->load->view('catalogos/unidad/modal');   
    }

    function getData(){
        $params = $this->input->post();
        $tablelistado = $this->ModeloUnidad->List_table($params);
        $tablelistadorow=$this->ModeloUnidad->List_table_total($params);
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }

    public function registro($id=0){
        if($id!=0){
            $data['status']=0;
            $data['title']='Edición de ';  
            $data['icon'] = 'fa fa-edit';
            $where = array('id'=>$id);
            $resutl = $this->ModeloCatalogos->getselectrowwheren('unidad',$where);
            foreach ($resutl->result() as $item) {
                $data['id']=$item->id;
                $data['propiedad']=$item->propiedad;
                $data['num_economico']=$item->num_economico;
                $data['placas']=$item->placas;
                $data['unidad_descripcion']=$item->unidad_descripcion;
                $data['marca_modelo']=$item->marca_modelo;
                $data['num_serie']=$item->num_serie;
                $data['num_motor']=$item->num_motor;
                $data['tipo']=$item->tipo;
                $data['num_tag']=$item->num_tag;
                $data['tipo_combustible']=$item->tipo_combustible;
                $data['num_cilindros']=$item->num_cilindros;
                $data['verificacion']=$item->verificacion;
                $data['operador']=$item->operador;
                $data['tarjeton_carga']=$item->tarjeton_carga;
                $data['compania_seguro']=$item->compania_seguro;
                $data['licencia_mercantil']=$item->licencia_mercantil;
                $data['dictamen_gas']=$item->dictamen_gas;
                $data['inciso_poliza']=$item->inciso_poliza;
                $data['no_poliza']=$item->no_poliza;
                $data['fecha_vencimineto_seguro']=$item->fecha_vencimineto_seguro;
                $data['verificacion_estatal']=$item->verificacion_estatal;
                $data['verificacion_federal']=$item->verificacion_federal;
                $data['img_trasera']=$item->img_trasera;
                $data['img_delantera']=$item->img_delantera;
                $data['img_izq']=$item->img_izq;
                $data['img_der']=$item->img_der;
                $data['rendimiento_esperado']=$item->rendimiento_esperado;
                $data['verificacion_fin']=$item->verificacion_fin;
                $whereop = array('personalId'=>$item->operador);
                $data['operador_text']='';
                $resutl = $this->ModeloCatalogos->getselectrowwheren('personal',$whereop);
                foreach ($resutl->result() as $op) {
                    $data['operador_text']=$op->nombre;
                }
            }
        }else{
            $data['title']='Nueva';  
         
            $data['id']=0;
            $data['propiedad']='';
            $data['num_economico']='';
            $data['placas']='';
            $data['unidad_descripcion']='';
            $data['marca_modelo']='';
            $data['num_serie']='';
            $data['num_motor']='';
            $data['tipo']='';
            $data['num_tag']='';
            $data['tipo_combustible']='';
            $data['num_cilindros']='';
            $data['verificacion']='';
            $data['operador']='';
            $data['tarjeton_carga']='';
            $data['compania_seguro']='';
            $data['licencia_mercantil']='';
            $data['dictamen_gas']='';
            $data['inciso_poliza']='';
            $data['no_poliza']='';
            $data['fecha_vencimineto_seguro']='';
            $data['verificacion_estatal']='';
            $data['verificacion_federal']='';
            $data['img_trasera']='';
            $data['img_delantera']='';
            $data['img_izq']='';
            $data['img_der']='';
            $data['operador_text']='';
            $data['verificacion_fin']='';
            $data['rendimiento_esperado']='';
        } 

        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('catalogos/unidad/form',$data);
        $this->load->view('theme/footer');
        $this->load->view('catalogos/unidad/formjs',$data);     

    }

    function delete_registro(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
    }
    
    function addregistro(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
        }else{
            $data['reg']=$this->fechahoy;
            $data['idpersonal_reg']=$this->idpersonal;
            $id=$this->ModeloCatalogos->Insert('unidad',$data);
        }
        echo $id;
    }

    function file_data_tra(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
            $target_path = 'uploads/unidad/img_tra';
            $thumb_path = 'uploads/unidad/img_tra';

            //file name setup
            $filename_err = explode(".",$_FILES['img']['name']);
            $filename_err_count = count($filename_err);
            $file_ext = $filename_err[$filename_err_count-1];
            //if($file_name != ''){
               // $fileName = $file_name.'.'.$file_ext;
            //}else{
                $fileName = $_FILES['img']['name'];
            //}
            $fecha=date('ymd-His');
            //upload image path
            $cargaimagen =$fecha.'.'.$file_ext;
            $upload_image = $target_path.'/'.$cargaimagen;
            
            //upload image
            if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                $data['img_trasera']=$cargaimagen;
                //thumbnail creation
                    //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                    $thumbnail = $thumb_path.'/'.$cargaimagen;
                    list($width,$height) = getimagesize($upload_image);
                    if ($width>3000) {
                        $thumb_width = $width/10;
                        $thumb_height = $height/10;
                    }elseif ($width>2500) {
                        $thumb_width = $width/7.9;
                        $thumb_height = $height/7.9;
                    }elseif ($width>2000) {
                        $thumb_width = $width/6.8;
                        $thumb_height = $height/6.8;
                    }elseif ($width>1500) {
                        $thumb_width = $width/5.1;
                        $thumb_height = $height/5.1;
                    }elseif ($width>1000) {
                        $thumb_width = $width/3.3;
                        $thumb_height = $height/3.3;
                    }elseif ($width>900) {
                        $thumb_width = $width/3;
                        $thumb_height = $height/3;
                    }elseif ($width>800) {
                        $thumb_width = $width/2.6;
                        $thumb_height = $height/2.6;
                    }elseif ($width>700) {
                        $thumb_width = $width/2.3;
                        $thumb_height = $height/2.3;
                    }elseif ($width>600) {
                        $thumb_width = $width/2;
                        $thumb_height = $height/2;
                    }elseif ($width>500) {
                        $thumb_width = $width/1.9;
                        $thumb_height = $height/1.9;
                    }elseif ($width>400) {
                        $thumb_width = $width/1.3;
                        $thumb_height = $height/1.3;
                    }elseif ($width>300) {
                        $thumb_width = $width/1.2;
                        $thumb_height = $height/1.2;
                    }else{
                        $thumb_width = $width;
                        $thumb_height = $height;
                    }
                    $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                    switch($file_ext){
                        case 'jpg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'jpeg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;

                        case 'png':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'gif':
                            $source = imagecreatefromgif($upload_image);
                            break;
                        default:
                            $source = imagecreatefromjpeg($upload_image);
                    }

                    imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                    switch($file_ext){
                        case 'jpg' || 'jpeg':
                            imagejpeg($thumb_create,$thumbnail,100);
                            break;
                        case 'png':
                            imagepng($thumb_create,$thumbnail,100);
                            break;

                        case 'gif':
                            imagegif($thumb_create,$thumbnail,100);
                            break;
                        default:
                            imagejpeg($thumb_create,$thumbnail,100);
                    }

                

                $return = Array('ok'=>TRUE,'img'=>'');
            }
            else{
                $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
    }

    function file_data_del(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
            $target_path = 'uploads/unidad/img_del';
            $thumb_path = 'uploads/unidad/img_del';

            //file name setup
            $filename_err = explode(".",$_FILES['img']['name']);
            $filename_err_count = count($filename_err);
            $file_ext = $filename_err[$filename_err_count-1];
            //if($file_name != ''){
               // $fileName = $file_name.'.'.$file_ext;
            //}else{
                $fileName = $_FILES['img']['name'];
            //}
            $fecha=date('ymd-His');
            //upload image path
            $cargaimagen =$fecha.'.'.$file_ext;
            $upload_image = $target_path.'/'.$cargaimagen;
            
            //upload image
            if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                $data['img_delantera']=$cargaimagen;
                //thumbnail creation
                    //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                    $thumbnail = $thumb_path.'/'.$cargaimagen;
                    list($width,$height) = getimagesize($upload_image);
                    if ($width>3000) {
                        $thumb_width = $width/10;
                        $thumb_height = $height/10;
                    }elseif ($width>2500) {
                        $thumb_width = $width/7.9;
                        $thumb_height = $height/7.9;
                    }elseif ($width>2000) {
                        $thumb_width = $width/6.8;
                        $thumb_height = $height/6.8;
                    }elseif ($width>1500) {
                        $thumb_width = $width/5.1;
                        $thumb_height = $height/5.1;
                    }elseif ($width>1000) {
                        $thumb_width = $width/3.3;
                        $thumb_height = $height/3.3;
                    }elseif ($width>900) {
                        $thumb_width = $width/3;
                        $thumb_height = $height/3;
                    }elseif ($width>800) {
                        $thumb_width = $width/2.6;
                        $thumb_height = $height/2.6;
                    }elseif ($width>700) {
                        $thumb_width = $width/2.3;
                        $thumb_height = $height/2.3;
                    }elseif ($width>600) {
                        $thumb_width = $width/2;
                        $thumb_height = $height/2;
                    }elseif ($width>500) {
                        $thumb_width = $width/1.9;
                        $thumb_height = $height/1.9;
                    }elseif ($width>400) {
                        $thumb_width = $width/1.3;
                        $thumb_height = $height/1.3;
                    }elseif ($width>300) {
                        $thumb_width = $width/1.2;
                        $thumb_height = $height/1.2;
                    }else{
                        $thumb_width = $width;
                        $thumb_height = $height;
                    }
                    $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                    switch($file_ext){
                        case 'jpg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'jpeg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;

                        case 'png':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'gif':
                            $source = imagecreatefromgif($upload_image);
                            break;
                        default:
                            $source = imagecreatefromjpeg($upload_image);
                    }

                    imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                    switch($file_ext){
                        case 'jpg' || 'jpeg':
                            imagejpeg($thumb_create,$thumbnail,100);
                            break;
                        case 'png':
                            imagepng($thumb_create,$thumbnail,100);
                            break;

                        case 'gif':
                            imagegif($thumb_create,$thumbnail,100);
                            break;
                        default:
                            imagejpeg($thumb_create,$thumbnail,100);
                    }

                

                $return = Array('ok'=>TRUE,'img'=>'');
            }
            else{
                $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
    }

    function file_data_izq(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
            $target_path = 'uploads/unidad/img_izq';
            $thumb_path = 'uploads/unidad/img_izq';

            //file name setup
            $filename_err = explode(".",$_FILES['img']['name']);
            $filename_err_count = count($filename_err);
            $file_ext = $filename_err[$filename_err_count-1];
            //if($file_name != ''){
               // $fileName = $file_name.'.'.$file_ext;
            //}else{
                $fileName = $_FILES['img']['name'];
            //}
            $fecha=date('ymd-His');
            //upload image path
            $cargaimagen =$fecha.'.'.$file_ext;
            $upload_image = $target_path.'/'.$cargaimagen;
            
            //upload image
            if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                $data['img_izq']=$cargaimagen;
                //thumbnail creation
                    //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                    $thumbnail = $thumb_path.'/'.$cargaimagen;
                    list($width,$height) = getimagesize($upload_image);
                    if ($width>3000) {
                        $thumb_width = $width/10;
                        $thumb_height = $height/10;
                    }elseif ($width>2500) {
                        $thumb_width = $width/7.9;
                        $thumb_height = $height/7.9;
                    }elseif ($width>2000) {
                        $thumb_width = $width/6.8;
                        $thumb_height = $height/6.8;
                    }elseif ($width>1500) {
                        $thumb_width = $width/5.1;
                        $thumb_height = $height/5.1;
                    }elseif ($width>1000) {
                        $thumb_width = $width/3.3;
                        $thumb_height = $height/3.3;
                    }elseif ($width>900) {
                        $thumb_width = $width/3;
                        $thumb_height = $height/3;
                    }elseif ($width>800) {
                        $thumb_width = $width/2.6;
                        $thumb_height = $height/2.6;
                    }elseif ($width>700) {
                        $thumb_width = $width/2.3;
                        $thumb_height = $height/2.3;
                    }elseif ($width>600) {
                        $thumb_width = $width/2;
                        $thumb_height = $height/2;
                    }elseif ($width>500) {
                        $thumb_width = $width/1.9;
                        $thumb_height = $height/1.9;
                    }elseif ($width>400) {
                        $thumb_width = $width/1.3;
                        $thumb_height = $height/1.3;
                    }elseif ($width>300) {
                        $thumb_width = $width/1.2;
                        $thumb_height = $height/1.2;
                    }else{
                        $thumb_width = $width;
                        $thumb_height = $height;
                    }
                    $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                    switch($file_ext){
                        case 'jpg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'jpeg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;

                        case 'png':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'gif':
                            $source = imagecreatefromgif($upload_image);
                            break;
                        default:
                            $source = imagecreatefromjpeg($upload_image);
                    }

                    imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                    switch($file_ext){
                        case 'jpg' || 'jpeg':
                            imagejpeg($thumb_create,$thumbnail,100);
                            break;
                        case 'png':
                            imagepng($thumb_create,$thumbnail,100);
                            break;

                        case 'gif':
                            imagegif($thumb_create,$thumbnail,100);
                            break;
                        default:
                            imagejpeg($thumb_create,$thumbnail,100);
                    }

                

                $return = Array('ok'=>TRUE,'img'=>'');
            }
            else{
                $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
    }

    function file_data_der(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        /*Imagen*/
        if (isset($_FILES['img'])) {
            //===========================================================================
            $target_path = 'uploads/unidad/img_der';
            $thumb_path = 'uploads/unidad/img_der';

            //file name setup
            $filename_err = explode(".",$_FILES['img']['name']);
            $filename_err_count = count($filename_err);
            $file_ext = $filename_err[$filename_err_count-1];
            //if($file_name != ''){
               // $fileName = $file_name.'.'.$file_ext;
            //}else{
                $fileName = $_FILES['img']['name'];
            //}
            $fecha=date('ymd-His');
            //upload image path
            $cargaimagen =$fecha.'.'.$file_ext;
            $upload_image = $target_path.'/'.$cargaimagen;
            
            //upload image
            if(move_uploaded_file($_FILES['img']['tmp_name'],$upload_image)){
                $data['img_der']=$cargaimagen;
                //thumbnail creation
                    //$this->ModeloCatalogos->categoriaaddimg($cargaimagen,$idcat);
                    $thumbnail = $thumb_path.'/'.$cargaimagen;
                    list($width,$height) = getimagesize($upload_image);
                    if ($width>3000) {
                        $thumb_width = $width/10;
                        $thumb_height = $height/10;
                    }elseif ($width>2500) {
                        $thumb_width = $width/7.9;
                        $thumb_height = $height/7.9;
                    }elseif ($width>2000) {
                        $thumb_width = $width/6.8;
                        $thumb_height = $height/6.8;
                    }elseif ($width>1500) {
                        $thumb_width = $width/5.1;
                        $thumb_height = $height/5.1;
                    }elseif ($width>1000) {
                        $thumb_width = $width/3.3;
                        $thumb_height = $height/3.3;
                    }elseif ($width>900) {
                        $thumb_width = $width/3;
                        $thumb_height = $height/3;
                    }elseif ($width>800) {
                        $thumb_width = $width/2.6;
                        $thumb_height = $height/2.6;
                    }elseif ($width>700) {
                        $thumb_width = $width/2.3;
                        $thumb_height = $height/2.3;
                    }elseif ($width>600) {
                        $thumb_width = $width/2;
                        $thumb_height = $height/2;
                    }elseif ($width>500) {
                        $thumb_width = $width/1.9;
                        $thumb_height = $height/1.9;
                    }elseif ($width>400) {
                        $thumb_width = $width/1.3;
                        $thumb_height = $height/1.3;
                    }elseif ($width>300) {
                        $thumb_width = $width/1.2;
                        $thumb_height = $height/1.2;
                    }else{
                        $thumb_width = $width;
                        $thumb_height = $height;
                    }
                    $thumb_create = imagecreatetruecolor($thumb_width,$thumb_height);
                    switch($file_ext){
                        case 'jpg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;
                        case 'jpeg':
                            $source = imagecreatefromjpeg($upload_image);
                            break;

                        case 'png':
                            $source = imagecreatefrompng($upload_image);
                            break;
                        case 'gif':
                            $source = imagecreatefromgif($upload_image);
                            break;
                        default:
                            $source = imagecreatefromjpeg($upload_image);
                    }

                    imagecopyresized($thumb_create,$source,0,0,0,0,$thumb_width,$thumb_height,$width,$height);
                    switch($file_ext){
                        case 'jpg' || 'jpeg':
                            imagejpeg($thumb_create,$thumbnail,100);
                            break;
                        case 'png':
                            imagepng($thumb_create,$thumbnail,100);
                            break;

                        case 'gif':
                            imagegif($thumb_create,$thumbnail,100);
                            break;
                        default:
                            imagejpeg($thumb_create,$thumbnail,100);
                    }

                

                $return = Array('ok'=>TRUE,'img'=>'');
            }
            else{
                $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
            }
            //===========================================================================
        }
        $this->ModeloCatalogos->updateCatalogo('unidad',$data,'id',$id);
    }

    function addregistro_expediente(){
        $data = $this->input->post();
        $id = $data['id'];
        unset($data['id']);
        if($id>=1){
            $this->ModeloCatalogos->updateCatalogo('unidades_expedientes',$data,'id',$id);
        }else{
            $data['reg']=$this->fechahoy;
            $id=$this->ModeloCatalogos->Insert('unidades_expedientes',$data);
        }
        echo $id;
    }

    function file_data_archivo(){
        $data = $this->input->post();
        $id = $data['id'];
        $upload_folder ='uploads/unidad/unidad_expediente';
        $nombre_archivo = $_FILES['img']['name'];
        $tipo_archivo = $_FILES['img']['type'];
        $tamano_archivo = $_FILES['img']['size'];
        $tmp_archivo = $_FILES['img']['tmp_name'];
        //$archivador = $upload_folder . '/' . $nombre_archivo;
        $cadena =str_replace(' ', '', $nombre_archivo);
        $fecha=date('ymd-His');
        $newfile='r_'.$fecha.'_'.$cadena;        
        $archivador = $upload_folder . '/'.$newfile;
        if (!move_uploaded_file($tmp_archivo, $archivador)) {
            $return = Array('ok' => FALSE, 'msg' => 'Ocurrió un error al subir el archivo. No pudo guardarse.', 'status' => 'error');
        }else{
            $arrayfile = array('archivo' => $newfile);
            $this->ModeloCatalogos->updateCatalogo('unidades_expedientes',$arrayfile,'id',$id);
            $return = Array('ok'=>TRUE);
        }
        echo json_encode($return);
    }

    public function get_tabla_expedientes()
    {   
        $id = $this->input->post('id');
        $html='<table class="table table-striped table-bordered" id="datatable_expedientes" style="width: 100%;"> 
            <thead>
                <tr>
                    <th>Nombre</th>
                    <th>Archivo</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>';
                $resutl = $this->ModeloUnidad->get_expedientes($id);
                foreach ($resutl as $x) {
                    $html.='<tr class="row_exp_'.$x->id.'"><td>'.$x->nombre.'</td><td><button type="button" title="Archivo" class="btn btn-sm btn-outline-info id_exp_'.$x->id.'" data-archivo="'.$x->archivo.'" onclick="abrir_archivo('.$x->id.')"><i class="fa fa-archive"></i></button></td><td><button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenerexpediente_'.$x->id.'" data-archivo_nombre="'.$x->nombre.'" onclick="modaldelete_expendiente('.$x->id.')"><i class="fa fa-trash-o"></i></button></td></tr>';
                }
            $html.='</tbody>
        </table>';
        echo $html;
    }

    function delete_registro_expediente(){  
        $id = $this->input->post('id');
        $data = array('activo'=>0);
        $this->ModeloCatalogos->updateCatalogo('unidades_expedientes',$data,'id',$id);
    }
}
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CI_Controller {
    function __construct(){
        parent::__construct();
        $this->load->model('ModeloUsuario');
        $this->load->model('ModeloCatalogos');
        $this->load->library('encrypt');
        if (!$this->session->userdata('logeado')){
            redirect('/Login');
        }else{
            $this->perfilid=$this->session->userdata('perfilid');
            $this->sucursalId=$this->session->userdata('sucursalId');
            //ira el permiso del modulo
            $permiso=$this->ModeloCatalogos->getviewpermiso($this->perfilid,2);// es el id del submenu
            if ($permiso==0) {
                redirect('/Sistema');
            }
        }
    }
	public function index()	{
        $data["sucursalId"]=$this->sucursalId;
        $data['sucursalesrow']=$this->ModeloCatalogos->getselectrowwheren('sucursales',array('activo'=>1));
        if ($this->perfilid==1) {
            $data['list_block']='';
        }else{
            $data['list_block']='style="display: none;"';
        }
        $data['usurios'] = $this->ModeloCatalogos->getselectall('usuarios');
        //$data['personal'] = $this->ModeloCatalogos->getselectrowwheren('personal',array('activo'=>1));
        $data['personal'] = $this->ModeloUsuario->getpersonal($this->perfilid,$this->sucursalId);
        if($this->perfilid==1){
          $data['perfiles'] = $this->ModeloCatalogos->getselectall('perfiles');
        }else{
          $data['perfiles']=$this->ModeloUsuario->getusuarios();
        }
        $this->load->view('theme/header');
        $this->load->view('theme/navbar');          
        $this->load->view('configuracion/usuarios',$data);
        $this->load->view('theme/footer'); 
        $this->load->view('theme/script_datatable');  
        $this->load->view('configuracion/usuariojs');   
	}   
    function getData(){
        $params = $this->input->post();
        $tablelistado=$this->ModeloUsuario->List_table($params,$this->perfilid,$this->sucursalId);
        $tablelistadorow=$this->ModeloUsuario->filastotal($params,$this->perfilid,$this->sucursalId); 
        $json_data = array(
            "draw"            => intval( $params['draw'] ),   
            "recordsTotal"    => intval($tablelistadorow),  
            "recordsFiltered" => intval($tablelistadorow),
            "data"            => $tablelistado->result(),
            "query"           =>$this->db->last_query()   
        );
        echo json_encode($json_data);
    }
    function addregistro()
    {
        $data = $this->input->post();
        $id_e=$data['UsuarioID'];
        $id = $this->encrypt->decode($id_e);
        unset($data['UsuarioID']);
        $pass=$data['contrasena'];
        $Usuario =$data['Usuario'];
        if ($id==0) {
            $resultado=0;
            $where = array('Usuario' =>$Usuario);
            $resultado=$this->ModeloUsuario->existe_usuario_v($Usuario,$id);
            /*
            foreach ($existeusuario->result() as $row) {
                $resultado=1;
            }
            */
            if($resultado==1){
               $result=4;
            }else{
                $tusu=$this->ModeloUsuario->numusuarios();
                $tusupermitidos=$this->ModeloUsuario->numusuariospermitidos(); 
                $num_usu=intval($tusu);
                $num_permitidos=intval($tusupermitidos);
                if ($num_usu<$num_permitidos){
                    $pass = password_hash($pass, PASSWORD_BCRYPT);
                    $data['contrasena']=$pass;
                    $this->ModeloCatalogos->Insert('usuarios',$data);
                    $personalId=$data['personalId'];
                    $perfilId=$data['perfilId'];
                    if($perfilId==1){
                        $wherep=array('tipo'=>1);
                    }else{
                        $wherep=array('tipo'=>0);
                    }
                    $this->ModeloCatalogos->updateCatalogo('personal',$wherep,'personalId',$personalId);
                    $result=1;
                }else{
                    $result=2;
                }
            }
        }else{
            if ($pass=='xxxxxx') {
                unset($data['contrasena']);
            }else{
                $pass = password_hash($pass, PASSWORD_BCRYPT);
                $data['contrasena']=$pass;
            }
            $where = array('Usuario' =>$Usuario,'UsuarioID'=>$id);
            $igualusuario=$this->ModeloCatalogos->getselectrowwheren('usuarios',$where);
            $resultupdate=0;
            foreach ($igualusuario->result() as $row) {
                $resultupdate=1;
            }
            if($resultupdate==1){
               $personalId=$data['personalId'];
               $perfilId=$data['perfilId'];
               if($perfilId==1){
                  $wherep=array('tipo'=>1);
               }else{
                  $wherep=array('tipo'=>0);
               }
               $this->ModeloCatalogos->updateCatalogo('personal',$wherep,'personalId',$personalId);
               $this->ModeloCatalogos->updateCatalogo('usuarios',$data,'UsuarioID',$id);
               $result=3;
            }else{
                $where = array('Usuario' =>$Usuario);
                $existeusuario=$this->ModeloCatalogos->getselectrowwheren('usuarios',$where);
                $resultado=0;
                foreach ($existeusuario->result() as $row) {
                    $resultado=1;
                }
                if($resultado==1){
                   $result=4;
                }else{
                   $personalId=$data['personalId'];
                   $perfilId=$data['perfilId'];
                   if($perfilId==1){
                      $wherep=array('tipo'=>1);
                   }else{
                      $wherep=array('tipo'=>0);
                   }
                   $this->ModeloCatalogos->updateCatalogo('personal',$wherep,'personalId',$personalId);
                   $this->ModeloCatalogos->updateCatalogo('usuarios',$data,'UsuarioID',$id);
                   $result=3; 
                }
            }
        } 
            
        echo $result;
    }
    function validar(){
        $Usuario = $this->input->post('usuario');
        $id = $this->encrypt->decode($this->input->post('idusu'));
        //var_dump($this->input->post());
        //$where = array('Usuario' =>$Usuario,);
        //$resultado=0;
        //if($idusu>=1){
            $resultado=$this->ModeloUsuario->existe_usuario_v($Usuario,$id);
        /*
        }else{
            $result=$this->ModeloUsuario->existe_usuario($Usuario);
        }
        $resultado=0;
        if($idusu>=1){
            foreach ($result->result() as $row) {
                $resultado=0;
            }
        }else{ 
            foreach ($result->result() as $row) {
                $resultado=1;
            }
        } 
        */   
        echo $resultado;
    }
    function delete_registro()
    {   $id_e = $this->input->post('id');
        $id = $this->encrypt->decode($id_e);
        $this->ModeloCatalogos->deleteCatalogo('usuarios','UsuarioID',$id);
    }
}
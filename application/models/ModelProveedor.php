<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelProveedor extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function List_table_proveedor($params){
        $columns = array( 
            0=>'p.id_proveedor',
            1=>'p.nombre',
            2=>'p.nombre_fiscal',
            3=>'p.direccion',
            4=>'p.tipo_establecimiento',
            5=>'p.telefono',
            6=>'p.estatus',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('proveedores AS p');
        $where = array(
            'p.activo'=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query; 
    } 

    function filastotal_proveedor($params){
        $search=$params['search']['value'];
        if($search!=''){
          $whereb= "p.activo=1";
          $where= $whereb." AND p.nombre LIKE '%".$search."%' or ";
          $where= $whereb." AND p.nombre_fiscal LIKE '%".$search."%' or ";
          $where.= $whereb." AND p.direccion LIKE '%".$search."%' or "; 
          $where.= $whereb." AND p.tipo_establecimiento LIKE '%".$search."%' or "; 
          $where.= $whereb." AND p.telefono LIKE '%".$search."%' or "; 
          $where.= $whereb." AND p.estatus LIKE '%".$search."%' or "; 
        }else{
          $where= "p.activo=1"; 
        }
        $strq = "SELECT COUNT(*) as total 
                FROM proveedores as p
                where ".$where;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }

    function get_material($id){
        $strq = "SELECT pm.id,pm.idmateriaprima,m.nombre,pm.precio_sin_factura,pm.precio_con_factura
            FROM proveedores_materiaprima AS pm
            INNER JOIN materiaprima AS m ON m.id=pm.idmateriaprima
            WHERE pm.activo=1 AND pm.idproveedor=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }
    
}
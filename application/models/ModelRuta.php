<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModelRuta extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function List_table($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.ruta',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('rutas AS r');
        $this->db->join('personal AS p','p.personalId = r.idpersonal');
        $where = array(
            'r.activo'=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query; 
    } 

    function filastotal($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.ruta',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('rutas r');
        $this->db->join('personal AS p','p.personalId = r.idpersonal');
        $this->db->where(array('r.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        } 
        $query=$this->db->get();
        return $query->row()->total;           
    }

    /*function get_rutas($id){
        $strq="SELECT * FROM rutas_detalles AS ru
        INNER JOIN proveedores AS pr ON pr.id_proveedor=ru.idproveedor
        WHERE ru.idruta=$id AND ru.activo=1 ORDER BY id DESC";
        $jquery=$this->db->query($strq);
        return $jquery->result();
    }*/
    
}
<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloBascula extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function List_table($params){
        $columns = array( 
          0=>'b.id',
          1=>'b.folio',
          2=>'b.clvprov',
          3=>'b.razsoc',
          4=>'b.fecfol',
          5=>'b.fecsal',
          6=>'b.horaent',
          7=>'b.horasal',
          8=>'b.chofer',
          9=>'b.placas',
          10=>'b.transporte',
          11=>'b.procede',
          12=>'b.origen',
          13=>'b.clvprod',
          14=>'b.descri',
          15=>'b.clvope',
          16=>'b.nombre',
          17=>'b.peso_envdo',
          18=>'b.peso_empaq',
          19=>'b.pbruto',
          20=>'b.tara',
          21=>'b.neto',
          22=>'b.peso_real',
          23=>'b.cancel',
          24=>'b.ciclo',
          25=>'b.manual',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('bascula b');
        $where = array(
                'b.activo'=>1
            ); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query; 
    } 

    public function List_table_total($params){

        $columns = array( 
          0=>'b.id',
          1=>'b.folio',
          2=>'b.clvprov',
          3=>'b.razsoc',
          4=>'b.fecfol',
          5=>'b.fecsal',
          6=>'b.horaent',
          7=>'b.horasal',
          8=>'b.chofer',
          9=>'b.placas',
          10=>'b.transporte',
          11=>'b.procede',
          12=>'b.origen',
          13=>'b.clvprod',
          14=>'b.descri',
          15=>'b.clvope',
          16=>'b.nombre',
          17=>'b.peso_envdo',
          18=>'b.peso_empaq',
          19=>'b.pbruto',
          20=>'b.tara',
          21=>'b.neto',
          22=>'b.peso_real',
          23=>'b.cancel',
          24=>'b.ciclo',
          25=>'b.manual',
        );

        $this->db->select('COUNT(*) as total');
        $this->db->from('bascula b');
        $where = array(
                'b.activo'=>1
            ); 
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_expedientes($id){
        $strq="SELECT * FROM unidades_expedientes WHERE idunidad=$id AND activo=1 ORDER BY id DESC";
        $jquery=$this->db->query($strq);
        return $jquery->result();
    } 
}

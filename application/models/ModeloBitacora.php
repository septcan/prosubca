<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloBitacora extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_proveedor_material($id,$materia){
        $strq = "SELECT pm.id,pm.idmateriaprima,m.nombre,pm.precio_sin_factura,pm.precio_con_factura
            FROM proveedores_materiaprima AS pm
            INNER JOIN materiaprima AS m ON m.id=pm.idmateriaprima
            WHERE pm.activo=1 AND pm.idproveedor=$id AND m.nombre='$materia'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_proveedor_material_costos($id){
        $strq = "SELECT bcd.id,p.nombre AS proveedor,m.nombre AS materia,bcd.precio,bcd.kilos,bcd.total,bcd.metodo_pago,bc.estatus,bc.folio,bcd.factura
            FROM bitacora_colecta_detalle AS bc
            INNER JOIN bitacora_colecta_detalle_material AS bcd ON bcd.idbitacora_colecta_detalle=bc.id 
            INNER JOIN proveedores AS p ON p.id_proveedor=bcd.idproveedor
            INNER JOIN materiaprima AS m ON m.id=bcd.idmateriaprima
            WHERE bc.activo=1 AND bcd.activo=1 AND bc.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_total_folios($dia,$operador){
        $strq = "SELECT COUNT(*) AS total
            FROM bascula
            WHERE fecfol='$dia' AND razsoc='$operador' ";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_gastos_colecta($id){
        $strq = "SELECT bg.id,c.nombre AS tipogasto,bg.importe
            FROM bitacora_gastos AS bg
            INNER JOIN categoria AS c ON c.id=bg.idcategoria 
            WHERE bg.activo=1 AND bg.idbitacora_colecta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_total_folios_colecta($id){
        $strq = "SELECT COUNT(*) AS total
            FROM bitacora_colecta_detalle
            WHERE estatus=1 AND idbitacora_colecta=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_total_folios_ayudante($dia,$operador){
        $strq = "SELECT COUNT(*) AS total
            FROM bascula
            WHERE fecfol='$dia' AND chofer='$operador' ";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_gastos_colecta_ayudante($id){
        $strq = "SELECT bg.id,c.nombre AS tipogasto,bg.importe,b.estatus
            FROM bitacora_colecta_ayudate AS b
            INNER JOIN ayudante_bitacora_gastos AS bg ON bg.idbitacora_colecta_ayudante=b.id
            INNER JOIN categoria AS c ON c.id=bg.idcategoria 
            WHERE bg.activo=1 AND b.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

}
<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloCatalogos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function Insert($Tabla,$data){
        $this->db->insert($Tabla, $data);
        $id=$this->db->insert_id();
        return $id;
    }
    function updateCatalogo($Tabla,$data,$idname,$id){
        $this->db->set($data);
        $this->db->where($idname, $id);
        $this->db->update($Tabla);
        return $id;
    }
    public function deleteCatalogo($table,$idname,$id){
        $this->db->where($idname, $id);
        $this->db->delete($table);
    }
    public function getselectall($table)
    {   $this->db->select('*');
        $this->db->from($table);
        $query=$this->db->get(); 
        return $query;
    }
    function updatestock($Tabla,$value,$masmeno,$value2,$idname,$id){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id ";
        $query = $this->db->query($strq);
        return $id;
    }
    function updatestock2($Tabla,$value,$masmeno,$value2,$idname,$id,$idname2,$id2){
        $strq = "UPDATE $Tabla SET $value = $value $masmeno $value2 where $idname=$id and $idname=$id";
        $query = $this->db->query($strq);
        return $id;
    }
    function getselectrowwheren($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query;
    }
    function getselectrowwheren_row($table,$where){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($where);
        $query=$this->db->get(); 
        return $query->row();
    }
    public function getselectwherelike2($tables,$where,$likec1,$likev1,$likec2,$likev2){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $this->db->or_like($likec2,$likev2, 'both');  
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }
    public function getselectwherelike1($tables,$where,$likec1,$likev1){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $query=$this->db->get();
        //$this->db->close();
        return $query;
    }

    public function getselectwherelike($tables,$where,$likec1,$likev1){
        $this->db->select("*");
        $this->db->from($tables);
        $this->db->where($where);
        $this->db->like($likec1,$likev1, 'both'); 
        $query=$this->db->get();
        //$this->db->close();
        return $query->result();
    }
    
    function getviewpermiso($perfil,$modulo){
        $strq = "SELECT COUNT(*) as total FROM `perfiles_detalles` WHERE perfilId=$perfil AND MenusubId=$modulo";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    function codebar($id){
        $strq = "SELECT codigo FROM `ticket_bar` WHERE barId=$id";
        $query = $this->db->query($strq);
        $codigo=0;
        foreach ($query->result() as $row) {
            $codigo=$row->codigo;
        }
        return $codigo; 
    }
      
    //================ Modificacion del stoct1 Fin ===========    
    function getbackup(){
        $strq="SELECT bac.backupid,bac.name,bac.reg,per.nombre FROM backup as bac inner JOIN personal as per on per.personalId=bac.personalId ORDER BY `bac`.`backupid` DESC limit 10";
            $resp=$this->db->query($strq);
            return $resp;
    } 

    public function buscar_paciente($nom,$idemple){
        $sql='SELECT * FROM pacientes WHERE activo=1 AND nombre LIKE "%'.$nom.'%"  or apll_paterno LIKE "%'.$nom.'%" or apll_materno LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }   

    public function buscar_medico($nom,$idemple){
        $sql='SELECT * FROM personal WHERE activo=1 AND nombre LIKE "%'.$nom.'%"  or apellidopaterno LIKE "%'.$nom.'%" or apellidomaterno LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function buscar_ruta($nom){
        $sql='SELECT * FROM rutas WHERE activo=1 AND ruta LIKE "%'.$nom.'%"';
        $query = $this->db->query($sql);
        return $query->result();
    }   

}

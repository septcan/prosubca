<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloEstadosDeCuenta extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_estado_cuenta($id,$anio,$mes){
        $strq = "SELECT bc.id,bc.dia,bc.total_compras_gastos,bc.deposito,bc.idruta,bc.saldo,ru.ruta
            FROM bitacora_colecta  AS bc
            LEFT JOIN rutas AS ru ON ru.id=bc.idruta
            WHERE bc.estatus=1 AND bc.idoperador=$id AND MONTH(bc.dia) = $mes AND YEAR(bc.dia) = $anio";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_rutas($id){
        $strq = "SELECT ru.id,ru.ruta
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN rutas AS ru ON ru.id=pc.ruta
            WHERE bc.id=$id GROUP BY pc.ruta";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_rendimiento($id,$anio,$mes){
        $strq = "SELECT SUM(total_efectivo) AS total_efectivo
            FROM rendimiento
            WHERE idoperador=$id AND anio=$anio AND mes=$mes";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function List_table($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.anio',
            3=>'r.mes',
            4=>'r.rendimiento',
            5=>'r.saldo_final'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('estado_cuenta  AS r');
        $this->db->join('personal AS p','p.personalId=r.idoperador');
        $where = array(
            'r.activo'=>1,
            'r.estatus='=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
          $search=$params['search']['value'];
          $this->db->group_start();
          foreach($columns as $c){
              $this->db->or_like($c,$search);
          }
          $this->db->group_end();
          
      }
      $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
      $this->db->limit($params['length'],$params['start']);
      $query=$this->db->get();
      return $query;
    }

     function List_table_total($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.anio',
            3=>'r.mes',
            4=>'r.rendimiento',
            5=>'r.saldo_final'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('estado_cuenta  AS r');
        $this->db->join('personal AS p','p.personalId=r.idoperador');
        $where = array(
            'r.activo'=>1,
            'r.estatus='=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_data_estado_cuenta($id){
        $strq = "SELECT ec.*,p.nombre
            FROM estado_cuenta  AS ec
            INNER JOIN personal AS p ON p.personalId=ec.idoperador
            WHERE ec.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

}

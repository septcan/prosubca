<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloGeneral extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function List_table_sucu($params){
            $columns = array( 
                0=>'s.sucursalid',
                1=>'s.sucursal',
                2=>'s.direccion',
                3=>'s.telefono',
                4=>'s.logo'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('sucursales AS s');
            $where = array(
                's.activo'=>1
            ); 
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function filastotal_sucu($params){
            $search=$params['search']['value'];
            if($search!=''){
              $whereb= "s.activo=1";
              $where= $whereb." AND s.sucursalid LIKE '%".$search."%' or ";
              $where.= $whereb." AND s.sucursal LIKE '%".$search."%' or "; 
              $where.= $whereb." AND s.direccion LIKE '%".$search."%' or "; 
              $where.= $whereb." AND s.telefono LIKE '%".$search."%' or "; 
              $where.= $whereb." AND s.logo LIKE '%".$search."%' "; 
            }else{
              $where= "s.activo=1"; 
            }
            $strq = "SELECT COUNT(*) as total 
                    FROM sucursales as s
                    where ".$where;
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
                $total =$row->total;
            } 
            return $total;
    }
    ////// Proveedor
    
    ////// Piso
    function List_table_piso($params,$perfil,$sucursal_admin){
        $columns = array( 
            0=>'idpiso',
            1=>'nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('piso');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$sucursal_admin
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function filastotal_piso($params,$perfil,$sucursal){
        $columns = array( 
            0=>'idpiso',
            1=>'nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('piso');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$sucursal
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    /////////// Total de piso 
    function getpiso($sucursal){
        $strq = "SELECT COUNT(*) as total FROM piso WHERE activo=1 AND sucursalid=$sucursal";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    ////// Tipo de habitacion
    function List_table_tipo($params,$perfil,$sucursal_admin){
        $columns = array( 
            0=>'idtipo',
            1=>'nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('tipo');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$sucursal_admin
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function filastotal_tipo($params,$perfil,$sucursal){
        $columns = array( 
            0=>'idtipo',
            1=>'nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('tipo');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$sucursal
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    /////////// Total de habitaciones 
    /*
    function get_total_habitaciones($sucursal){
        $strq = "SELECT COUNT(*) as total FROM habitacion WHERE activo=1 AND sucursalid=$sucursal";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total=$row->total;
        }
        return $total; 
    }
    */
    //<!-------------Pacientes------------->//
    function List_table_pacientes($params){
            $columns = array( 
                0=>'idpaciente',
                1=>'nombre',
                2=>'apll_paterno',
                3=>'apll_materno',
                4=>'fecha_nacimiento',
                5=>'correo',
                6=>'celular',
                7=>'foto',
                8=>'sexo'
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('pacientes');
            $where = array(
                'activo'=>1
            );     
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();  
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function filastotal_pacientes($params){
         $columns = array( 
            0=>'idpaciente',
            1=>'nombre',
            2=>'apll_paterno',
            3=>'apll_materno',
            4=>'fecha_nacimiento',
            5=>'correo',
            6=>'celular'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('pacientes');
        $where = array(
            'activo'=>1
        );     
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c2){
                $this->db->or_like($c2,$search);
            }
            $this->db->group_end();  
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ////// Categorias
    function List_table_categorias($params){
        $columns = array( 
            0=>'idcategoria',
            1=>'categoria',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('categoria');        
        $where = array(
                    'activo'=>1
                );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function filastotal_categorias($params){
        $columns = array( 
            0=>'idcategoria',
            1=>'categoria',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('categoria');
        $where = array(
                    'activo'=>1
                );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ////// Huesped
    function filastotal_huesped($params,$perfil,$idsucursal){
        $columns2 = array( 
            0=>'idhuesped',
            1=>'sucursalid',
            2=>'nombre',
            3=>'correo',
            4=>'telefono',
            5=>'celular',
            6=>'observaciones_cliente'
        );
        $select2="";
        foreach ($columns2 as $c2) {
            $select2.="$c2, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('huesped');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$idsucursal,
            );
        }      
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c2){
                $this->db->or_like($c2,$search);
            }
            $this->db->group_end();  
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///// Huesped
    function List_table_huesped($params,$perfil,$idsucursal){
        $columns = array( 
        0=>'idhuesped',
        1=>'sucursalid',
        2=>'nombre',
        3=>'correo',
        4=>'telefono',
        5=>'celular',
        6=>'observaciones_cliente'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('huesped');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$idsucursal,
            );
        }      
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    ////// salon
    function filastotal_salon($params,$perfil,$idsucursal){
        $columns2 = array( 
            0=>'idsalon',
            1=>'sucursalid',
            2=>'nombre',
            3=>'detalles',
            4=>'precio',
        );
        $select2="";
        foreach ($columns2 as $c2) {
            $select2.="$c2, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('salon');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$idsucursal,
            );
        }      
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns2 as $c2){
                $this->db->or_like($c2,$search);
            }
            $this->db->group_end();  
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
    ///// Salon
    function List_table_salon($params,$perfil,$idsucursal){
        $columns = array( 
        0=>'idsalon',
        1=>'sucursalid',
        2=>'nombre',
        3=>'detalles',
        4=>'precio',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('salon');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'activo'=>1
                );
            }else{
                $where = array(
                    'activo'=>1,
                    'sucursalid'=>$sucursal
                );
            }
        }else{
            $where = array(
                'activo'=>1,
                'sucursalid'=>$idsucursal,
            );
        }      
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    /* <--- Recepción ---> */
    public function getListcitas($inicio,$fin){
        $strq = "SELECT c.*, CONCAT(p.nombre,' ', p.apll_paterno,' ', p.apll_materno) As paciente, TIMESTAMPDIFF(YEAR,p.fecha_nacimiento,CURDATE()) AS edad , p.celular, p.sexo, CONCAT(pe.nombre,' ', pe.apellidopaterno,' ', pe.apellidomaterno) As medico,DATE_FORMAT(c.fecha_cita, '%d-%m-%Y')  as fecha_c,DATE_FORMAT(c.hora_inicio,'%h:%i %p') as hora_inicio_c,DATE_FORMAT(c.hora_fin,'%h:%i %p') as hora_fin_c,c.cita_estatus
                    FROM agenda_citas as c 
                    LEFT JOIN pacientes AS p ON p.idpaciente = c.idpaciente
                    LEFT JOIN personal AS pe ON pe.personalId = c.personalId
                    where c.activo=1 AND c.fecha_cita BETWEEN '$inicio' AND '$fin'";
        $query = $this->db->query($strq);
        return $query->result();
    }
}

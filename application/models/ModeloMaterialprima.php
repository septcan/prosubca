<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloMaterialprima extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function List_table($params){
        $columns = array( 
            0=>'m.id',
            1=>'m.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('materiaprima m');
        $where = array(
            'm.activo'=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query; 
    } 

    public function List_table_total($params){

        $columns = array( 
            0=>'m.id',
            1=>'m.nombre',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('materiaprima m');
        $this->db->where(array('m.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }

}
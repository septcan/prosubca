<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloNotificacion extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function List_table($params,$id){
        $columns = array( 
            0=>'ha.id',
            1=>'ha.tipo',
            2=>'ha.concepto',
            3=>'ha.dia',
            4=>'p.nombre',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('historial_alertas ha');
        $this->db->join('personal AS p','p.personalId = ha.idoperador','left');
        $where = array(
            'ha.activo'=>1,'ha.idoperador'=>$id
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query; 
    } 

    public function List_table_total($params,$id){

        $columns = array( 
            0=>'ha.id',
            1=>'ha.tipo',
            2=>'ha.concepto',
            3=>'ha.dia',
            4=>'p.nombre',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('historial_alertas ha');
        $this->db->join('personal AS p','p.personalId = ha.idoperador','left');
        $where = array(
            'ha.activo'=>1,'ha.idoperador'=>$id
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }

}
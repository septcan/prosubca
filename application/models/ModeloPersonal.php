<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloPersonal extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function List_table($params,$perfil,$sucursal_admin){
      $columns = array( 
          0=>'p.personalId',
          1=>'p.nombre',
          2=>'p.puesto',
          3=>'p.unidad',
          4=>'p.nss',
          5=>'p.curp',
          6=>'p.rfc',
          7=>'p.correo',
          8=>'p.celular',
          9=>'p.estatus',
          10=>'p.num_empleado',
          11=>'p.foto',
          12=>'p.sexo',
          13=>'u.placas',
      );

      $select="";
      foreach ($columns as $c) {
          $select.="$c, ";
      }
      $this->db->select($select);
      $this->db->from('personal AS p');
      $this->db->join('unidad AS u','u.id = p.unidad','left');
      $where = array(
                  'p.activo'=>1,
                  'p.tipo!='=>1
              );
      $this->db->where($where);
      if( !empty($params['search']['value']) ) {
          $search=$params['search']['value'];
          $this->db->group_start();
          foreach($columns as $c){
              $this->db->or_like($c,$search);
          }
          $this->db->group_end();
          
      }
      $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
      $this->db->limit($params['length'],$params['start']);
      $query=$this->db->get();
      return $query;
    }

    function filastotal($params,$perfil,$sucursal_admin){
        $columns = array( 
            0=>'p.personalId',
            1=>'p.nombre',
            2=>'p.puesto',
            3=>'p.unidad',
            4=>'p.nss',
            5=>'p.curp',
            6=>'p.rfc',
            7=>'p.correo',
            8=>'p.celular',
            9=>'p.estatus',
            10=>'p.num_empleado',
            11=>'u.placas',
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('personal p');
        $this->db->join('unidad AS u','u.id = p.unidad','left');
        $where = array(
                      'p.activo'=>1,
                      'p.tipo !='=>1
                  );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
 
}

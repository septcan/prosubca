<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloProducto extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function productosexistencias($sucu,$pro) {
        $strq = "SELECT * FROM productos_sucursales where idproducto=$pro and idsucursal=$sucu";
        $query = $this->db->query($strq);
        //$this->db->close();
        $idproducto_sucursal=0;
        $precio_venta=0;
        $existencia=0;
        
        foreach ($query->result() as $row) {
            $idproducto_sucursal = $row->idproducto_sucursal;
            $precio_venta =$row->precio_venta;
            $existencia =$row->existencia;
        } 
        $array=array('idproducto_sucursal'=>$idproducto_sucursal,'precio_venta'=>$precio_venta,'existencia'=>$existencia);

        return $array;
    }
    /// Productos
    function List_table_productos($params,$perfil,$sucursal_admin){
        $columns = array( 
            0=>'p.idproducto',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'c.categoria',
            4=>'ps.precio_venta',
            5=>'ps.existencia',
            6=>'p.img'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('productos AS p');
        $this->db->join('categoria AS c','c.idcategoria = p.idcategoria');
        $this->db->join('productos_sucursales AS ps','ps.idproducto = p.idproducto');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'p.activo'=>1
                );
            }else{
                $where = array(
                    'p.activo'=>1,
                    'ps.idsucursal'=>$sucursal
                );
            }
        }else{
            $where = array(
                'p.activo'=>1,
                'ps.idsucursal'=>$sucursal_admin
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        //echo $this->db->get_compiled_select();
        $query=$this->db->get();
        // print_r($query); die;
        return $query;
    }
    function filastotal_productos($params,$perfil,$sucursal_admin){
        $columns = array( 
            0=>'p.idproducto',
            1=>'p.codigo',
            2=>'p.nombre',
            3=>'c.categoria',
            4=>'ps.precio_venta',
            5=>'ps.existencia',
            6=>'p.img'
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(1) as total');
        $this->db->from('productos AS p');
        $this->db->join('categoria AS c','c.idcategoria = p.idcategoria');
        $this->db->join('productos_sucursales AS ps','ps.idproducto = p.idproducto');
        if($perfil==1){
            $sucursal=$params['sucu'];
            if ($sucursal==0) {
                $where = array(
                    'p.activo'=>1
                );
            }else{
                $where = array(
                    'p.activo'=>1,
                    'ps.idsucursal'=>$sucursal
                );
            }
        }else{
            $where = array(
                'p.activo'=>1,
                'ps.idsucursal'=>$sucursal_admin
            );
        }    
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }
}
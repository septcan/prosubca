<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRecepcion extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function get_habitacion_piso_sucu($activo,$piso,$sucu){
        $strq ="SELECT h.idhabitacion,h.numero,h.idtipo,h.status,t.nombre AS tipo,h.idpiso
                FROM habitacion AS h
                INNER JOIN tipo AS t ON t.idtipo = h.idtipo
                where h.activo=$activo AND h.idpiso=$piso AND h.sucursalid=$sucu";
        $query = $this->db->query($strq);
        return $query->result();
    }
}    
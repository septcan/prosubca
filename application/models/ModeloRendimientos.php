<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRendimientos extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_rendimiento($id,$anio,$mes,$unidad){
        $strq = "SELECT bc.id,ru.ruta,bc.dia,bc.km_inicial,bc.km_final,bc.km_recorrido,un.placas,(select GROUP_CONCAT(numero_factura SEPARATOR' ,') AS numero_factura from bitacora_gastos where idbitacora_colecta = bc.id AND activo=1 AND idcategoria=2 AND numero_factura!='' ) as numero_factura,(select GROUP_CONCAT(num_vale SEPARATOR' ,') AS num_vale from bitacora_vale where idbitacora_colecta = bc.id AND activo=1) as num_vale,(select SUM(litros) AS litros from bitacora_vale where idbitacora_colecta = bc.id AND activo=1) as litros
            FROM bitacora_colecta  AS bc
            INNER JOIN bitacora_colecta_detalle AS bcd ON bcd.idbitacora_colecta=bc.id
            INNER JOIN rutas AS ru ON ru.id=bcd.ruta
            LEFT JOIN unidad AS un ON un.id=bcd.idunidad
            WHERE bcd.activo=1 AND bc.estatus=1 AND bc.idoperador=$id AND bc.idunidad=$unidad AND MONTH(bc.dia) = $mes AND YEAR(bc.dia) = $anio GROUP BY bcd.ruta,bcd.idunidad,bc.dia";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_rendimiento_total($id,$anio,$mes,$unidad){
        $strq = "SELECT COUNT(*) AS total
            FROM bitacora_colecta  AS bc
            INNER JOIN bitacora_colecta_detalle AS bcd ON bcd.idbitacora_colecta=bc.id
            INNER JOIN rutas AS ru ON ru.id=bcd.ruta
            LEFT JOIN unidad AS un ON un.id=bcd.idunidad
            WHERE bcd.activo=1 AND bc.estatus=1 AND bc.idoperador=$id AND bc.idunidad=$unidad AND MONTH(bc.dia) = $mes AND YEAR(bc.dia) = $anio GROUP BY bcd.ruta,bcd.idunidad";
        $query = $this->db->query($strq);
        return $query->result();
    }

    public function total_ruta_bitacora_colecta($id)
    {
        $strq = "SELECT COUNT(*) AS ruta FROM 
                (SELECT COUNT(*) AS ruta  FROM bitacora_colecta_detalle WHERE idbitacora_colecta=$id AND activo=1 GROUP BY ruta) AS ruta";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_rendimiento_recorrido($id,$anio,$mes,$unidad){
        $strq = "SELECT SUM(bc.km_recorrido) AS km_recorrido
            FROM bitacora_colecta  AS bc
            WHERE bc.estatus=1 AND bc.idoperador=$id AND bc.idunidad=$unidad AND MONTH(bc.dia) = $mes AND YEAR(bc.dia) = $anio";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_rendimiento_litros($id,$anio,$mes,$unidad){
        $strq = "SELECT SUM(bv.litros) AS litros
            FROM bitacora_colecta  AS bc
            INNER JOIN bitacora_vale AS bv ON bv.idbitacora_colecta=bc.id
            WHERE bv.activo=1 AND bc.estatus=1 AND bc.idoperador=$id AND bc.idunidad=$unidad AND MONTH(bc.dia) = $mes AND YEAR(bc.dia) = $anio";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function List_table($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.anio',
            3=>'r.mes',
            4=>'r.total_efectivo',
            5=>'u.placas'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('rendimiento  AS r');
        $this->db->join('personal AS p','p.personalId=r.idoperador');
        $this->db->join('unidad AS u','u.id=r.unidad');
        $where = array(
            'r.activo'=>1,
            'r.estatus='=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
          $search=$params['search']['value'];
          $this->db->group_start();
          foreach($columns as $c){
              $this->db->or_like($c,$search);
          }
          $this->db->group_end();
          
      }
      $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
      $this->db->limit($params['length'],$params['start']);
      $query=$this->db->get();
      return $query;
    }

     function List_table_total($params){
        $columns = array( 
            0=>'r.id',
            1=>'p.nombre',
            2=>'r.anio',
            3=>'r.mes',
            4=>'r.total_efectivo',
            5=>'u.placas'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('rendimiento  AS r');
        $this->db->join('personal AS p','p.personalId=r.idoperador');
        $this->db->join('unidad AS u','u.id=r.unidad');
        $where = array(
            'r.activo'=>1,
            'r.estatus='=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_data_rendimiento($id){
        $strq = "SELECT r.*,p.nombre,u.placas,u.tipo_combustible
            FROM rendimiento  AS r
            INNER JOIN personal AS p ON p.personalId=r.idoperador
            INNER JOIN unidad AS u ON u.id=r.unidad
            WHERE r.id=$id";
        $query = $this->db->query($strq);
        return $query->row();
    }

}
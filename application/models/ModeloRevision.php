<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloRevision extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    function get_bitacora_colecta($id,$dia){
        $strq = "SELECT bc.*,un.placas
            FROM bitacora_colecta AS bc
            LEFT JOIN unidad AS un ON un.id=bc.idunidad
            WHERE bc.activo=1 AND bc.idoperador=$id AND bc.dia='$dia'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_rutas($id,$dia){
        $strq = "SELECT pc.ruta,ru.ruta,ba.origen,pc.id,ba.placas,pc.idunidad,un.placas AS placas_uni,pc.idbitacora_colecta
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            LEFT JOIN rutas AS ru ON ru.id=pc.ruta
            INNER JOIN bascula AS ba ON ba.folio=pc.folio
            LEFT JOIN unidad AS un ON un.id=pc.idunidad
            WHERE bc.activo=1 AND pc.activo=1 AND bc.idoperador=$id AND bc.dia='$dia'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_compras($id,$dia){
        $strq = "SELECT p.nombre,mp.nombre AS materia,pm.precio,pm.kilos,pm.total,pm.factura,pm.id AS idbitacora_colecta_detalle_material,pm.numero_factura,pm.validar,pm.metodo_pago
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN bitacora_colecta_detalle_material AS pm ON pm.idbitacora_colecta_detalle=pc.id 
            INNER JOIN proveedores AS p ON p.id_proveedor=pm.idproveedor
            INNER JOIN materiaprima AS mp ON mp.id=pm.idmateriaprima
            WHERE bc.activo=1 AND pm.activo=1 AND bc.idoperador=$id AND bc.dia='$dia'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_gastos($id,$dia){
        $strq = "SELECT bc.dia,ca.nombre AS categoria,bg.importe,bg.id AS idgastos,bg.numero_factura,bg.validar,bg.importe_edicion
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_gastos AS bg ON bg.idbitacora_colecta=bc.id
            INNER JOIN categoria AS ca ON ca.id=bg.idcategoria
            WHERE bc.activo=1 AND bg.activo=1 AND bc.idoperador=$id AND bc.dia='$dia'";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_compras_total($id,$dia){
        $strq = "SELECT p.nombre,mp.nombre AS materia,pm.precio,SUM(pm.kilos) AS kilos,pm.total
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN bitacora_colecta_detalle_material AS pm ON pm.idbitacora_colecta_detalle=pc.id 
            INNER JOIN proveedores AS p ON p.id_proveedor=pm.idproveedor
            INNER JOIN materiaprima AS mp ON mp.id=pm.idmateriaprima
            WHERE bc.activo=1 AND bc.idoperador=$id AND bc.dia='$dia' GROUP BY pm.idmateriaprima";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_bitacora_colecta_unida($id,$dia){
        $strq = "SELECT bc.id,pc.ruta,ba.origen,pc.folio,ba.folio,ba.placas
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN bascula AS ba ON ba.folio=pc.folio
            WHERE bc.activo=1 AND pc.activo=1 AND bc.idoperador=$id AND bc.dia='$dia' GROUP BY bc.id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_bitacora_colecta_reporte($id){
        $strq = "SELECT pe.nombre,bc.dia,bc.sueldo,bc.km_inicial,bc.km_final,bc.km_recorrido,bc.tarjeta_pase,bc.costo_flete,un.placas
            FROM bitacora_colecta AS bc
            LEFT JOIN personal AS pe ON pe.personalId=bc.idoperador
            LEFT JOIN unidad AS un ON un.id=bc.idunidad
            WHERE bc.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_compras_reporte($id){
        $strq = "SELECT p.nombre,mp.nombre AS materia,pm.precio,pm.kilos,pm.total,pm.factura,pm.id AS idbitacora_colecta_detalle_material,pm.numero_factura,pm.validar,pm.metodo_pago
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN bitacora_colecta_detalle_material AS pm ON pm.idbitacora_colecta_detalle=pc.id 
            INNER JOIN proveedores AS p ON p.id_proveedor=pm.idproveedor
            INNER JOIN materiaprima AS mp ON mp.id=pm.idmateriaprima
            WHERE bc.activo=1 AND pm.activo=1 AND bc.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_gastos_reporte($id){
        $strq = "SELECT bc.dia,ca.nombre AS categoria,bg.importe,bg.id AS idgastos,bg.numero_factura,bg.validar,bg.importe_edicion,bg.importe_historial,bc.idoperador
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_gastos AS bg ON bg.idbitacora_colecta=bc.id
            INNER JOIN categoria AS ca ON ca.id=bg.idcategoria
            WHERE bc.activo=1 AND bg.activo=1 AND bc.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function get_operador_compras_total_reporte($id){
        $strq = "SELECT p.nombre,mp.nombre AS materia,pm.precio,SUM(pm.kilos) AS kilos,pm.total
            FROM bitacora_colecta AS bc
            INNER JOIN bitacora_colecta_detalle AS pc ON pc.idbitacora_colecta=bc.id
            INNER JOIN bitacora_colecta_detalle_material AS pm ON pm.idbitacora_colecta_detalle=pc.id 
            INNER JOIN proveedores AS p ON p.id_proveedor=pm.idproveedor
            INNER JOIN materiaprima AS mp ON mp.id=pm.idmateriaprima
            WHERE bc.activo=1 AND bc.id=$id GROUP BY pm.idmateriaprima";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function List_table($params){
        $columns = array( 
            0=>'bc.id',
            1=>'p.nombre',
            2=>'bc.dia',
            3=>'bc.costo_flete'
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('bitacora_colecta  AS bc');
        $this->db->join('personal AS p','p.personalId=bc.idoperador','left');
        $where = array(
            'bc.activo'=>1,
            'bc.estatus='=>1
        );
      $this->db->where($where);
      if( !empty($params['search']['value']) ) {
          $search=$params['search']['value'];
          $this->db->group_start();
          foreach($columns as $c){
              $this->db->or_like($c,$search);
          }
          $this->db->group_end();
          
      }
      $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
      $this->db->limit($params['length'],$params['start']);
      $query=$this->db->get();
      return $query;
    }

    function List_table_total($params){
        $columns = array( 
            0=>'bc.id',
            1=>'p.nombre',
            2=>'bc.dia',
            3=>'bc.costo_flete'
        );
        $this->db->select('COUNT(1) as total');
        $this->db->from('bitacora_colecta  AS bc');
        $this->db->join('personal AS p','p.personalId=bc.idoperador','left');
        $where = array(
            'bc.activo'=>1,
            'bc.estatus='=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();   
        }
        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_precio_factura($id){
        $strq = "SELECT pm.*,bcd.kilos
            FROM bitacora_colecta_detalle_material AS bcd
            INNER JOIN proveedores_materiaprima AS pm ON pm.idmateriaprima=bcd.idmateriaprima
            WHERE pm.idproveedor=bcd.idproveedor AND pm.activo=1 AND bcd.id=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

}

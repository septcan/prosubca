<?php
//ini_set("session.cookie_lifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia esta funcion no funciona en php7
//ini_set("session.gc_maxlifetime","86400");// 0 asta que el navegador se cierre o especificar los segundos, 86400 un dia esta funcion no funciona en php7
/*
$a=session_id();
if(empty($a)) session_start();
*/
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloSession extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function login($usu,$pass) {
        $strq = "SELECT usu.UsuarioID,per.personalId,per.nombre,usu.perfilId,usu.contrasena,perf.perfil,per.sexo
                 FROM usuarios as usu
                 inner join personal as per on per.personalId = usu.personalId
                 inner join perfiles as perf on perf.perfilId = usu.perfilId
                 where per.activo=1 AND usu.Usuario ='$usu'";
        $count = 0;
        $passwo =0;
        $query = $this->db->query($strq);
        $this->db->close();
        foreach ($query->result() as $row) {
            $passwo =$row->contrasena;
            $id = $row->UsuarioID;
            $nom =$row->nombre;
            $perfil = $row->perfilId; 
            $idpersonal = $row->personalId;
            $perfiln = $row->perfil;
            $sexo = $row->sexo;
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                        'logeado' => true,
                        'usuarioid' => $id,
                        'nombre' => $nom,
                        'perfilid'=>$perfil,
                        'idpersonal'=>$idpersonal,
                        'perfiln'=>$perfiln,
                        'sexo'=>$sexo,
                        //'idproveedor'=>0,
                    );
                $this->session->set_userdata($data);
                $count=1;
                //$count=$passwo.'/'.$id.'/'.$nom.'/'.$perfil.'/'.$idpersonal;
            }
            /*
                 si es cero aqui se agregara los mismos parametros pero para el aseso de los residentes si es que lo piden
            */
        } 
        /*
        $strq2 = "SELECT pro.id_proveedor AS UsuarioID,pro.nombre,pro.perfilid,pro.contrasena,perf.perfil
                FROM proveedores AS pro
                inner join perfiles as perf on perf.perfilId = pro.perfilid
                WHERE pro.activo=1 AND pro.acceso=1 AND pro.usuario = '$usu'";
        $query2 = $this->db->query($strq2);

        foreach ($query2->result() as $row2) {
            $passwo =$row2->contrasena;
            $id = $row2->UsuarioID;
            $nom =$row2->nombre;
            $perfil = $row2->perfilid; 
            $idpersonal = 0;
            $perfiln = $row2->perfil; 
            $sexo = 1;
            $verificar = password_verify($pass,$passwo);
            if ($verificar) {
                $data = array(
                    'logeado' => true,
                    'usuarioid' => $id,
                    'nombre' => $nom,
                    'perfilid'=>$perfil,
                    'idpersonal'=>$idpersonal,
                    'perfiln'=>$perfiln,
                    'sexo'=>$sexo,
                    'idproveedor'=>$id,
                );
                $this->session->set_userdata($data);
                $count=1;
            }
        } 
        */
        
        echo $count;
    }

    public function menus($perfil){
        $strq ="SELECT distinct men.MenuId,men.Nombre,men.Icon,count(perfd.MenusubId) AS cantidadmenu from menu as men, menu_sub as mens, perfiles_detalles as perfd where men.MenuId=mens.MenuId and perfd.MenusubId=mens.MenusubId and perfd.PerfilId='$perfil' and men.MenuId!=4 GROUP BY men.MenuId  ORDER BY men.MenuId ASC ";
        //$strq="CALL SP_GET_MENUS($perfil)";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus($perfil,$menu){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId='$menu' and menus.MenuId!=4 ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query;

    }
    public function submenus_1($perfil){
        $strq ="SELECT menus.MenusubId,menus.Nombre, menus.Pagina, menus.Icon,menus.MenuId from menu_sub as menus, perfiles_detalles as perfd WHERE perfd.MenusubId=menus.MenusubId and perfd.PerfilId='$perfil' and menus.MenuId=4 ORDER BY menus.orden ASC";
        $query = $this->db->query($strq);
        return $query;

    }
    function getpersona($id)
    {
        $strq = "SELECT nombre FROM personal WHERE personalId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getperfil($id)
    {
        $strq = "SELECT perfil FROM perfiles WHERE perfilId=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getconfig()
    {
        $strq = "SELECT * FROM configuracion WHERE id=1";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getalertas($id)
    {
        $strq = "SELECT p.nombre AS operador,ha.dia,ha.concepto,ha.tipo,ha.id FROM historial_alertas AS ha 
            INNER JOIN personal AS p ON p.personalId=ha.idoperador
            WHERE ha.activo=1 AND ha.estatus=0 AND ha.idoperador=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }

    function getalertas_total($id)
    {
        $strq = "SELECT COUNT(*) AS total FROM historial_alertas AS ha 
            INNER JOIN personal AS p ON p.personalId=ha.idoperador
            WHERE ha.activo=1 AND ha.estatus=0 AND ha.idoperador=$id";
        $query = $this->db->query($strq);
        return $query->result();
    }


}

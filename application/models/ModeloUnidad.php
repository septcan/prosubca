<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUnidad extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    
    function List_table($params){
        $columns = array( 
            0=>'u.id',
            1=>'u.propiedad',
            2=>'u.num_economico',
            3=>'u.placas',
            4=>'u.unidad_descripcion',
            5=>'u.marca_modelo',
            6=>'u.num_serie',
        );
        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select($select);
        $this->db->from('unidad u');
        $where = array(
            'u.activo'=>1
        );
        $this->db->where($where);
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columns as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();
            
        }
        $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
        $this->db->limit($params['length'],$params['start']);
        $query=$this->db->get();
        return $query; 
    } 

    public function List_table_total($params){

        $columns = array( 
            0=>'u.id',
            1=>'u.propiedad',
            2=>'u.num_economico',
            3=>'u.placas',
            4=>'u.unidad_descripcion',
            5=>'u.marca_modelo',
            6=>'u.num_serie',
        );

        $select="";
        foreach ($columns as $c) {
            $select.="$c, ";
        }
        $this->db->select('COUNT(*) as total');
        $this->db->from('unidad u');
        $this->db->where(array('u.activo'=>1));
        if( !empty($params['search']['value']) ) {
            $search=$params['search']['value'];
            $this->db->group_start();
            foreach($columnsSearch as $c){
                $this->db->or_like($c,$search);
            }
            $this->db->group_end();  
        }            

        $query=$this->db->get();
        return $query->row()->total;
    }

    function get_expedientes($id){
        $strq="SELECT * FROM unidades_expedientes WHERE idunidad=$id AND activo=1 ORDER BY id DESC";
        $jquery=$this->db->query($strq);
        return $jquery->result();
    } 


}
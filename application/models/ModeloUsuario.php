<?php
defined('BASEPATH') OR exit ('No direct script access allowed');

class ModeloUsuario extends CI_Model {
    public function __construct() {
        parent::__construct();
    }
    function filastotal($params,$perfil,$sucursal_admin){
        if($perfil==1){
            $sucu=$params['sucu'];
            if($sucu==0){
                $where="p.activo=1";
            }else{
                $where="p.activo=1 AND p.sucursalid=".$sucu;
            }
        }else{
            $where="p.activo=1 AND p.tipo !=1 AND p.sucursalid=$sucursal_admin";
        }
            $strq = "SELECT COUNT(*) as total 
                    FROM usuarios AS u
                    INNER JOIN personal AS p ON p.personalId = u.personalId
                    INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
                    INNER JOIN sucursales AS s ON s.sucursalid = p.sucursalid
                    where ".$where;
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
                $total =$row->total;
            } 
            return $total;
    }
    function List_table($params,$perfil,$sucursal_admin){
            $columns = array( 
                0=>'u.UsuarioID',
                1=>'u.Usuario',
                2=>'pe.perfil',
                3=>'p.nombre',
                4=>'p.apellidopaterno',
                5=>'p.apellidomaterno',
                6=>'pe.perfilId',
                7=>'p.personalId',
                8=>'s.sucursal',
            );
            $select="";
            foreach ($columns as $c) {
                $select.="$c, ";
            }
            $this->db->select($select);
            $this->db->from('usuarios AS u');
            $this->db->join('personal AS p','p.personalId = u.personalId');
            $this->db->join('perfiles AS pe','pe.perfilId = u.perfilId');
            $this->db->join('sucursales AS s','s.sucursalid = p.sucursalid');
            if($perfil==1){
                $sucursal=$params['sucu'];
                if ($sucursal==0) {
                    $where = array(
                        'p.activo'=>1
                    );
                }else{
                    $where = array(
                        'p.activo'=>1,
                        'p.sucursalid'=>$sucursal
                    );
                }
            }else{
                $where = array(
                        'p.activo'=>1,
                        'p.sucursalid'=>$sucursal_admin,
                        'p.tipo !='=>1
                    );
            }
            $this->db->where($where);
            if( !empty($params['search']['value']) ) {
                $search=$params['search']['value'];
                $this->db->group_start();
                foreach($columns as $c){
                    $this->db->or_like($c,$search);
                }
                $this->db->group_end();
                
            }
            $this->db->order_by($columns[$params['order'][0]['column']], $params['order'][0]['dir']);
            $this->db->limit($params['length'],$params['start']);
            //echo $this->db->get_compiled_select();
            $query=$this->db->get();
            // print_r($query); die;
            return $query;
    }
    function numusuarios(){
        $strq = "SELECT count(*) as total FROM usuarios AS u
                 INNER JOIN personal AS p ON p.personalId=u.personalId
                 WHERE p.activo=1";
        $query = $this->db->query($strq);
        $total=0;
        foreach ($query->result() as $row) {
            $total =$row->total;
        } 
        return $total;
    }
    function numusuariospermitidos(){
        $strq = "SELECT usuariosmaximo FROM configuracion where id=1";
        $query = $this->db->query($strq);
        $usuariosmaximo=0;
        foreach ($query->result() as $row) {
            $usuariosmaximo =$row->usuariosmaximo;
        } 
        return $usuariosmaximo;
    }
    function getpersonal($perfil,$sucursal_admin){
        if($perfil==1){
           $where="";
        }else{
           $where="AND p.sucursalid=$sucursal_admin"; 
        }
        $strq = "SELECT p.personalId,p.nombre,p.apellidopaterno,p.apellidomaterno,s.sucursal FROM personal AS p 
                INNER JOIN sucursales AS s ON s.sucursalid = p.sucursalid where p.activo=1 $where";
        $query = $this->db->query($strq);
        return $query->result();
    }
    public function getusuarios()
    {
        $strq = "SELECT * FROM perfiles where perfilId!=1";
        $query = $this->db->query($strq);
        return $query;
    }
    public function existe_usuario($nombre){
        $total = 0;
        $where="p.activo=1 AND u.Usuario='$nombre'";
        $strq = "SELECT COUNT(*) as total 
                    FROM usuarios AS u
                    INNER JOIN personal AS p ON p.personalId = u.personalId
                    where ".$where;
            $query = $this->db->query($strq);
            $this->db->close();
            foreach ($query->result() as $row) {
                $total =$row->total;
            } 
            return $total;
    }
    public function existe_usuario_v($nombre,$idusu){
            $total = 0;
            if($idusu>=1){
                $where="p.activo=1 AND u.Usuario='".$nombre."' AND UsuarioID=".$idusu;
                $strq = "SELECT COUNT(*) as total 
                        FROM usuarios AS u
                        INNER JOIN personal AS p ON p.personalId = u.personalId
                        where ".$where;
                $query = $this->db->query($strq);
                $this->db->close();
                foreach ($query->result() as $row) {
                    $total =$row->total;
                } 
                if($total>=1){
                    $total = 0;
                }else{
                    $where="p.activo=1 AND u.Usuario='$nombre'";
                    $strq = "SELECT COUNT(*) as total 
                        FROM usuarios AS u
                        INNER JOIN personal AS p ON p.personalId = u.personalId
                        where ".$where;
                        $query = $this->db->query($strq);
                        foreach ($query->result() as $row) {
                            $total=$row->total;
                        }
                        if($total>=1){
                            $total = 1;
                        }else{
                            $total = 0;
                        }
                }
            }else{
                $where="p.activo=1 AND u.Usuario='$nombre'";
                    $strq = "SELECT COUNT(*) as total 
                        FROM usuarios AS u
                        INNER JOIN personal AS p ON p.personalId = u.personalId
                        where ".$where;
                        $query = $this->db->query($strq);
                        foreach ($query->result() as $row) {
                            $total=$row->total;
                        }
                        if($total>=1){
                            $total = 1;
                        }else{
                            $total = 0;
                        }
            }
            return $total;
    }
}      
/*
SELECT u.UsuarioID,u.Usuario,pe.nombre,p.nombre,p.apellidopaterno,p.apellidomaterno FROM usuarios AS u 
INNER JOIN personal AS p ON p.personalId = u.personalId
INNER JOIN perfiles AS pe ON pe.perfilId = u.perfilId
WHERE p.activo = 1
*/
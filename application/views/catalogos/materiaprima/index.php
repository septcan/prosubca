<style>.zoom {transition: transform .2s;}.zoom:hover {transform: scale(2); }</style>
<section">
	<div class="row">
		<div class="col-12">
			<div class="card">	
				<div class="card-header">
					<div class="row">
						<div class="col-12">
					        <h3 style="border-bottom-style: inset;"><b>Listado de materia prima</b></h3>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="row">
							<div class="col-8">
								<div class="table-responsive">
									<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
										<thead>
											<tr>
												<th>#</th>
												<th>Nombre</th>
												<th></th>
											</tr>
										</thead>
										<tbody>
										</tbody>
										<tfoot>
										    <tr>
											    <th>#</th>
												<th>Nombre</th>
												<th></th>
											</tr>
										</tfoot>
									</table>
								</div>
							</div>
							<div class="col-4">
								<h3><span class="title_material">Nueva</span> materia prima</h3>
								<form class="form" method="post" role="form" id="form_registro">
								    <input type="hidden" name="id" id="id_reg">
								    <div class="row">
									<div class="col-sm-12">
											<fieldset class="form-group">
												<label>Nombre</label>
												<input type="text" name="nombre" id="nombre" class="form-control round">
											</fieldset>
										</div>
									</div>	
								</form>	
								<hr>
								<div class="row">
									<div class="col-sm-12"> 
									    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_registro()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
		                            </div>	
		                        </div>	
		                    </div>    
						</div>		
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

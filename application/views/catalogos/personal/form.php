<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fileinput/fileinput.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b><?php echo $title ?> empleado</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<h5>Datos Generales</h5>
						<form class="form" method="post" role="form" id="form_registro">
						    <input type="hidden" name="personalId" id="personalId" value="<?php echo $personalId ?>">
						    <div class="row">
							<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número de trabajador</label>
										<input type="number" name="num_empleado" class="form-control round" value="<?php echo $num_empleado ?>">
									</fieldset>
								</div>
								<div class="col-sm-5">
									<fieldset class="form-group">
										<label>Nombre</label>
										<input type="text" name="nombre" class="form-control round" value="<?php echo $nombre ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Puesto</label>
										<select class="form-control round" name="puesto">
											<option value="0" disabled selected>Selecciona una opción</option>
											<option value="1" <?php if($puesto==1) echo 'selected' ?>>Administrativo</option>
											<option value="2" <?php if($puesto==2) echo 'selected' ?>>Operador</option>
											<option value="3" <?php if($puesto==3) echo 'selected' ?>>Ayudante</option>
										</select>
									</fieldset>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Unidad</label>
										<select class="form-control round" name="unidad" id="unidad">
											<?php if($unidad!=0){ ?>
												<option value="<?php echo $unidad ?>"><?php echo $unidad_text ?></option>
											<?php } ?>	
										</select>
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Nss</label>
										<input type="text" name="nss" class="form-control round" value="<?php echo $nss ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Curp</label>
										<input type="text" name="curp" class="form-control round" value="<?php echo $curp ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>RFC</label>
										<input type="text" name="rfc" class="form-control round" value="<?php echo $rfc ?>">
									</fieldset>
								</div>
							    <div class="col-sm-4">
									<fieldset class="form-group">
										<label>Correo</label>
										<input type="email" name="correo" class="form-control round" value="<?php echo $correo ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Contraseña</label>
										<input type="text" name="contrasenacorreo" class="form-control round" value="<?php echo $contrasenacorreo ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Celular</label>
										<input type="text" name="celular" class="form-control round" value="<?php echo $celular ?>">
									</fieldset>
								</div>
								<div class="col-sm-8">
									<fieldset class="form-group">
										<label>Domicilio</label>
										<input type="text" name="domicilio" class="form-control round" value="<?php echo $domicilio ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Beneficiario</label>
										<input type="text" name="beneficiario" class="form-control round" value="<?php echo $beneficiario ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Sexo</label>
										<select class="form-control round" name="sexo">
											<option value="1" <?php if($sexo==1) echo 'selected' ?>>Hombre</option>
											<option value="2" <?php if($sexo==2) echo 'selected' ?>>Mujer</option>
										</select>
									</fieldset>
								</div>

								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Estatus</label>
										<select class="form-control round" name="estatus">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($estatus==1) echo 'selected' ?>>Activo</option>
											<option value="2" <?php if($estatus==2) echo 'selected' ?>>Inactivo</option>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="row">
							    <div class="col-sm-4">
									<fieldset class="form-group">
										<label>Fecha de ingreso</label>
										<input type="date" name="fecha_ingreso" class="form-control round" value="<?php echo $fecha_ingreso ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Sueldo diario</label>
										<input type="number" name="sueldo" class="form-control round" value="<?php echo $sueldo ?>">
									</fieldset>
								</div>
								<div class="col-4">
								    <div class="form-group">
										<label class="control-label">Foto</label><br>
										<input id="foto" type="file">
									</div> 
								</div>	
                            </div>
							<hr>
							<?php 
							    $acceso_check='';
								$acceso_style='style="display:none"';
							    if($acceso==1){
								    $acceso_check='checked';  
									$acceso_style='style="display:block"';
							    }
							?>
							<div class="row">
							    <div class="col-sm-4">
								    <div class="custom-switch custom-switch-success custom-control-inline mb-1 mb-xl-0">
										<input type="checkbox" class="custom-control-input" id="acceso_sistema" name="acceso" onclick="btn_acceso()" <?php echo $acceso_check ?>>
										<label class="custom-control-label mr-1" for="acceso_sistema">
											<span>Acceso al sistema</span>
										</label>
									</div>
                                </div>	
                            </div>	
							
							<br>
							<div class="text_acceso" <?php echo $acceso_style ?>> 
							    <input type="hidden" name="UsuarioID" id="UsuarioID" value="<?php echo $UsuarioID ?>">
								<div class="row">
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Usuario</label>
											<input type="text" id="Usuario" name="Usuario" class="form-control round" value="<?php echo $Usuario ?>">
										</fieldset>
									</div>
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Perfil</label>
											<select class="form-control round" name="perfilId">
												<option value="0" disabled selected>Selecciona una opción</option>
												<?php foreach($get_perfiles->result() as $p) { 
													if($p->perfilId!=4){
												?>
												    <option value="<?php echo $p->perfilId ?>" <?php if($p->perfilId==$perfilId) echo 'selected' ?>><?php echo $p->perfil ?></option>
												<?php } 
												    }
												?>
											</select>
										</fieldset>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Contraseña</label>
											<input type="password" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list"  class="form-control round" disabled>
										</fieldset>
									</div>
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Verificar contraseña</label>
											<input type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" class="form-control round" disabled>
										</fieldset>
									</div>
									<div class="col-sm-2">
									    <label>   </label><br>
									    <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" onclick="btn_bloquear()"><i class="ft-lock"></i></a>
                                    </div>	
								</div>
                            </div>	
						</form>	
						<hr>
						<div class="row">
							<div class="col-sm-12"> 
							    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_empleado()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
								<a href="<?php echo base_url();?>Personal" class="btn btn-outline-secondary round mr-1 mb-1">Regresar <span><i class="fa fa-arrow-left"></i></span></a>
                            </div>	
                        </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

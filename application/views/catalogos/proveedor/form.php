<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b><?php echo $title ?> proveedor</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<h5>Datos Generales</h5>
						<form class="form" method="post" role="form" id="form_proveedor">
						    <input type="hidden" name="id_proveedor" id="id_proveedor" value="<?php echo $id_proveedor ?>">
						    <div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Razón social</label>
										<input type="text" name="nombre" class="form-control round" value="<?php echo $nombre ?>">
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Tipo de establecimiento</label>
										<input type="text" name="tipo_establecimiento" class="form-control round" value="<?php echo $tipo_establecimiento ?>">
									</fieldset>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Contacto</label>
										<input type="text" name="contacto" class="form-control round" value="<?php echo $contacto ?>">
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>RFC</label>
										<input type="text" name="rfc" class="form-control round" value="<?php echo $rfc ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Dirección fiscal</label>
										<input type="text" name="nombre_fiscal" class="form-control round" value="<?php echo $nombre_fiscal ?>">
									</fieldset>
								</div>
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Dirección de recolección</label>
										<input type="text" name="direccion" class="form-control round" value="<?php echo $direccion ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Estado</label>
										<input type="text" name="estado" class="form-control round" value="<?php echo $estado ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Teléfono</label>
										<input type="text" name="telefono" class="form-control round" value="<?php echo $telefono ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Estatus</label>
										<select class="form-control round" name="estatus">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($estatus==1) echo 'selected' ?>>Activo</option>
											<option value="2" <?php if($estatus==2) echo 'selected' ?>>Inactivo</option>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-2">
									<fieldset class="form-group">
										<label>Días por pasar</label>
										<input type="number" name="dias" min="1" max="7" class="form-control round" value="<?php echo $dias ?>">
									</fieldset>
								</div>	
								<div class="col-sm-5">
									<fieldset class="form-group">
										<label>Coordenada latitud</label>
										<input type="text" name="latitud" class="form-control round" value="<?php echo $latitud ?>">
									</fieldset>
								</div>
								<div class="col-sm-5">
									<fieldset class="form-group">
										<label>Coordenada longitud</label>
										<input type="text" name="longitud" class="form-control round" value="<?php echo $longitud ?>">
									</fieldset>
								</div>
								<div class="col-sm-5">
									<fieldset class="form-group">
										<label>Método de pago </label>
										<select class="form-control round" name="metodo_pago">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($metodo_pago==1) echo 'selected' ?>>Efectivo</option>
											<option value="2" <?php if($metodo_pago==2) echo 'selected' ?>>Transferencia</option>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<h5>Materia prima</h5>
								</div>
								<div class="col-sm-5">
						        	<fieldset class="form-group">
						        		<label>Materia prima</label>
										<select class="form-control round" id="materiaprima">
										</select>
									</fieldset>
						        </div>
						        <div class="col-sm-2">
						        	<fieldset class="form-group">
						        		<label>Precio sin factura</label>
										<input type="number" id="precio_sin_factura" class="form-control round">
									</fieldset>
						        </div>
						        <div class="col-sm-2">
						        	<fieldset class="form-group">
						        		<label>Precio con factura</label>
										<input type="number" id="precio_con_factura" class="form-control round">
									</fieldset>
						        </div>
						        <div class="col-sm-2">
						        	<label style="color:#ff000000;">___</label><br>
						        	<a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="add_materia()"><b><i class="ft-plus"></i></b></a>
						        </div>
							</div>		
							<div class="row">
								<div class="col-sm-10">
									<table class="table table-striped table-bordered" id="datatable_mp" style="width: 100%;"> 
										<thead>
											<tr>
												<th style="width:60%">Nombre</th>
												<th>Precio sin factura</th>
												<th>Precio con factura</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody id="tbody_pm">
										</tbody>
									</table>	
								</div>	
							</div>	
							<hr>
							<?php /*
							    $acceso_check='';
								$acceso_style='style="display:none"';
							    if($acceso==1){
								    $acceso_check='checked';  
									$acceso_style='style="display:block"';
							    }
							?>
							<div class="row">
							    <div class="col-sm-4">
								    <div class="custom-switch custom-switch-success custom-control-inline mb-1 mb-xl-0">
										<input type="checkbox" class="custom-control-input" id="acceso_sistema" name="acceso" onclick="btn_acceso()" <?php echo $acceso_check ?>>
										<label class="custom-control-label mr-1" for="acceso_sistema">
											<span>Acceso al sistema</span>
										</label>
									</div>
                                </div>	
                            </div>	
							<br>
							<?php 
							    $contrasena_disabled='';
							    if($id_proveedor!=0){
                                    $contrasena_disabled='disabled'; 
								}else{
                                    $contrasena_disabled=''; 
								} 
							?>
							<div class="text_acceso" <?php echo $acceso_style ?>> 
								<div class="row">
									<div class="col-sm-4">
										<fieldset class="form-group">
											<label>Usuario</label>
											<input type="text" id="usuario" name="usuario" class="form-control round" value="<?php echo $usuario ?>">
										</fieldset>
									</div>
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Contraseña</label>
											<input type="password" autocomplete="new-password" name="contrasena" id="contrasena" aria-autocomplete="list"  class="form-control round" <?php echo $contrasena_disabled ?>>
										</fieldset>
									</div>
									<div class="col-sm-3">
										<fieldset class="form-group">
											<label>Verificar contraseña</label>
											<input type="password" name="contrasena2" id="contrasena2" autocomplete="new-password" class="form-control round" <?php echo $contrasena_disabled ?>>
										</fieldset>
									</div>
									<div class="col-sm-2">
									    <label>   </label><br>
									    <a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" onclick="btn_bloquear()"><i class="ft-lock"></i></a>
                                    </div>	
								</div>
                            </div>
                            */ 
                            ?> 	
						</form>	
						<hr>
						<div class="row">
							<div class="col-sm-12"> 
							    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_proveedor()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
								<a href="<?php echo base_url();?>Proveedores" class="btn btn-outline-secondary round mr-1 mb-1">Regresar <span><i class="fa fa-arrow-left"></i></span></a>
                            </div>	
                        </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

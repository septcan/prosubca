<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b><?php echo $title ?> ruta</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<h5>Datos Generales</h5>
						<form class="form" method="post" role="form" id="form_registro">
						    <input type="hidden" name="id" id="idreg" value="<?php echo $id ?>">
						    <!-- <div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Operador</label>
										<select class="form-control round" name="idpersonal" id="idpersonal">
                                            <?php // if($idpersonal!=0){ ?>
												<option value="<?php // echo $idpersonal ?>"><?php // echo $nombretxt ?></option>
											<?php // } ?>	
										</select>
									</fieldset>
								</div>
							</div> -->
							<div class="row">
								<div class="col-sm-12">
									<fieldset class="form-group">
						        		<label>Ruta</label>
										<input type="text" name="ruta" class="form-control" value="<?php echo $ruta ?>">
									</fieldset>
								</div>
							</div>	
							<!-- <div class="row">
								<div class="col-sm-8">
									<h5>Rutas de proveedores</h5>
									<div class="row">
								        <div class="col-sm-9">
								        	<fieldset class="form-group">
								        		<label>Razón social</label>
												<select class="form-control round" id="proveedor">
												</select>
											</fieldset>
											
								        </div>
								        <div class="col-sm-3"><br>
								        	<a type="button" class="btn btn-warning btn-icon round mr-1 mb-1" title="Agregar" onclick="add_proveedor()"><b><i class="ft-plus"></i></b></a>
								        </div>	
								    </div>    	
									
									<table class="table table-striped table-bordered" id="datatable_mp" style="width: 100%;"> 
										<thead>
											<tr>
												<th>Razón social</th>
												<th>Dirección</th>
												<th>Acciones</th>
											</tr>
										</thead>
										<tbody id="tbody_pm">
										</tbody>
									</table>	
								</div>	
							</div>	
							<hr> -->
						</form>	
						<hr>
						<div class="row">
							<div class="col-sm-12"> 
							    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_registro()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
								<a href="<?php echo base_url();?>Rutas" class="btn btn-outline-secondary round mr-1 mb-1">Regresar <span><i class="fa fa-arrow-left"></i></span></a>
                            </div>	
                        </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

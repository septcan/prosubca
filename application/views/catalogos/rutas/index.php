<style>.zoom {transition: transform .2s;}.zoom:hover {transform: scale(2); }</style>
<section">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-9">
					        <h3 style="border-bottom-style: inset;"><b>Listado de rutas</b></h3>
                        </div>	
						<div class="col-3">
					        <a href="<?php echo base_url() ?>Rutas/registro" class="btn btn-outline-info round mr-1 mb-1" style="width: 100%;"><span><i class="fa fa-map-marker"></i></span> Nueva ruta </a>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
								<thead>
									<tr>
								     	<th>#</th>
										<th>Rutas</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
								<tr>
							    	<th>#</th>
									<th>Rutas</th>
									<th>Acciones</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

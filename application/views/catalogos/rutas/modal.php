<div class="modal fade text-left" id="modal_rutas_listado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Rutas</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="tabla_rutas"></div>
					</div>
				</div>		
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		    </div>
		</div>
	</div>
</div>
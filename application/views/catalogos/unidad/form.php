<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fileinput/fileinput.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<style type="text/css">
	.file-preview-image{
		width: 100% !important;
	}
</style>
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b><?php echo $title ?> unidad</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<h5>Datos Generales</h5>
						<form class="form" method="post" role="form" id="form_registro">
						    <input type="hidden" name="id" id="id" value="<?php echo $id ?>">
						    <div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Propiedad</label>
										<input type="text" name="propiedad" class="form-control round" value="<?php echo $propiedad ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número económico</label>
										<input type="number" name="num_economico" class="form-control round" value="<?php echo $num_economico ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Placas</label>
										<input type="text" name="placas" class="form-control round" value="<?php echo $placas ?>">
									</fieldset>
								</div>
							</div>	
							<div class="row">
								<div class="col-sm-9">
									<fieldset class="form-group">
										<label>Unidad/descripción</label>
										<input type="text" name="unidad_descripcion" class="form-control round" value="<?php echo $unidad_descripcion ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Rendimiento esperado</label>
										<input type="number" name="rendimiento_esperado" class="form-control round" value="<?php echo $rendimiento_esperado ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Marca/modelo </label>
										<input type="text" name="marca_modelo" class="form-control round" value="<?php echo $marca_modelo ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número de serie</label>
										<input type="text" name="num_serie" class="form-control round" value="<?php echo $num_serie ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número motor</label>
										<input type="text" name="num_motor" class="form-control round" value="<?php echo $num_motor ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Tipo</label>
										<input type="text" name="tipo" class="form-control round" value="<?php echo $tipo ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número de tag IAVE</label>
										<input type="text" name="num_tag" class="form-control round" value="<?php echo $num_tag ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Tipo de Combustible</label>
										<select class="form-control round" name="tipo_combustible">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($tipo_combustible==1) echo 'selected' ?>>Disel</option>
											<option value="2" <?php if($tipo_combustible==2) echo 'selected' ?>>Gas</option>
											<option value="3" <?php if($tipo_combustible==3) echo 'selected' ?>>Gasolina</option>
										</select>
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Número de Cilindros</label>
										<input type="number" name="num_cilindros" class="form-control round" value="<?php echo $num_cilindros ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<!-- <div class="col-sm-6">
									<fieldset class="form-group">
										<label>Operador actual</label>
										<select class="form-control round" name="operador" id="operador">
											<?php //if($operador!=0){ ?>
												<option value="<?php // echo $operador ?>"><?php // echo $operador_text ?></option>
											<?php // } ?>	
										</select>
									</fieldset>
								</div> -->
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Verificación inicial</label>
										<input type="date" name="verificacion" class="form-control round" value="<?php echo $verificacion ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Verificación final</label>
										<input type="date" name="verificacion_fin" class="form-control round" value="<?php echo $verificacion_fin ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Tarjetón de carga</label>
										<input type="date" name="tarjeton_carga" class="form-control round" value="<?php echo $tarjeton_carga ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Compañia de seguro</label>
										<input type="text" name="compania_seguro" class="form-control round" value="<?php echo $compania_seguro ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">	
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Licencia mercantil</label>
										<input type="date" name="licencia_mercantil" class="form-control round" value="<?php echo $licencia_mercantil ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Dictamen de gas</label>
										<input type="date" name="dictamen_gas" class="form-control round" value="<?php echo $dictamen_gas ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Inciso de póliza </label>
										<input type="number" name="inciso_poliza" class="form-control round" value="<?php echo $inciso_poliza ?>">
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>No. póliza</label>
										<input type="number" name="no_poliza" class="form-control round" value="<?php echo $no_poliza ?>">
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Fecha de vencimiento de seguro</label>
										<input type="date" name="fecha_vencimineto_seguro" class="form-control round" value="<?php echo $fecha_vencimineto_seguro ?>">
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Verificación estatal</label>
										<select class="form-control round" name="verificacion_estatal">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($verificacion_estatal==1) echo 'selected' ?>>Aplica</option>
											<option value="2" <?php if($verificacion_estatal==2) echo 'selected' ?>>No aplica</option>
										</select>
									</fieldset>
								</div>
								<div class="col-sm-4">
									<fieldset class="form-group">
										<label>Verificación federal</label>
										<select class="form-control round" name="verificacion_federal">
											<option value="0" disabled selected>Selecciona una opción</option>
                                            <option value="1" <?php if($verificacion_federal==1) echo 'selected' ?>>Aplica</option>
											<option value="2" <?php if($verificacion_federal==2) echo 'selected' ?>>No aplica</option>
										</select>
									</fieldset>
								</div>
							</div>
							<div class="row">
								<div class="col-3">
								    <div class="form-group" >
										<label class="control-label">Foto trasera</label><br>
										<input id="img_trasera" type="file" style="width: 100%;">
									</div> 
								</div>
								<div class="col-3">
								    <div class="form-group">
										<label class="control-label">Foto delantera</label><br>
										<input id="img_delantera" type="file">
									</div> 
								</div>
								<div class="col-3">
								    <div class="form-group">
										<label class="control-label">Foto lateral izquierdo</label><br>
										<input id="img_izq" type="file">
									</div> 
								</div>
								<div class="col-3">
								    <div class="form-group">
										<label class="control-label">Foto lateral derecho</label><br>
										<input id="img_der" type="file">
									</div> 
								</div>	
							</div>
						</form>	
						<hr>
						<div class="row">
							<div class="col-sm-12"> 
							    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guardar_registro()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
								<a href="<?php echo base_url();?>Unidad" class="btn btn-outline-secondary round mr-1 mb-1">Regresar <span><i class="fa fa-arrow-left"></i></span></a>
                            </div>	
                        </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<script src="<?php echo base_url(); ?>assets/fileinput/fileinput.js"></script>
<script src="<?php echo base_url(); ?>app-assets/vendors/js/select2.full.min.js"></script>
<script src="<?php echo base_url(); ?>public/js/catalogos/form_unidad.js?v=<?php echo date('ymdHgi') ?>" type="text/javascript"></script>
<script type="text/javascript">
	<?php if ($img_trasera!='') { ?>
		var url1 = '<?php echo base_url().'uploads/unidad/img_tra/'.$img_trasera?>';	
	<?php } ?>
	
	$("#img_trasera").fileinput({
        <?php if ($img_trasera!='') { ?>
            initialPreview: [url1],
            initialPreviewAsData: true,
        <?php } ?> 
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png"],
        browseLabel: 'Seleccionar imagen',
        //uploadUrl: 'Productos/upload',
        maxFilePreviewSize: 1000,
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });

    <?php if ($img_delantera!='') { ?>
		var url1 = '<?php echo base_url().'uploads/unidad/img_del/'.$img_delantera?>';	
	<?php } ?>
	
	$("#img_delantera").fileinput({
        <?php if ($img_delantera!='') { ?>
            initialPreview: [url1],
            initialPreviewAsData: true,
        <?php } ?>
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png"],
        browseLabel: 'Seleccionar imagen',
        //uploadUrl: 'Productos/upload',
        maxFilePreviewSize: 1000,
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });

    <?php if ($img_izq!='') { ?>
		var url1 = '<?php echo base_url().'uploads/unidad/img_izq/'.$img_izq?>';	
	<?php } ?>
	
	$("#img_izq").fileinput({
        <?php if ($img_izq!='') { ?>
            initialPreview: [url1],
            initialPreviewAsData: true,
        <?php } ?>
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png"],
        browseLabel: 'Seleccionar imagen',
        //uploadUrl: 'Productos/upload',
        maxFilePreviewSize: 1000,
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });

    <?php if ($img_der!='') { ?>
		var url1 = '<?php echo base_url().'uploads/unidad/img_der/'.$img_der?>';	
	<?php } ?>
	
	$("#img_der").fileinput({
        <?php if ($img_der!='') { ?>
            initialPreview: [url1],
            initialPreviewAsData: true,
        <?php } ?>
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png"],
        browseLabel: 'Seleccionar imagen',
        //uploadUrl: 'Productos/upload',
        maxFilePreviewSize: 1000,
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });
</script>

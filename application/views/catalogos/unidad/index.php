<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fileinput/fileinput.min.css">
<section>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-9">
					        <h3 style="border-bottom-style: inset;"><b>Listado de unidades</b></h3>
                        </div>	
						<div class="col-3">
					        <a href="<?php echo base_url() ?>Unidad/registro" class="btn btn-outline-info round mr-1 mb-1" style="width: 100%;"><span><i class="fa fa-truck"></i></span> Nueva unidad </a>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
								<thead>
									<tr>
								     	<th>#</th>
										<th>Propiedad</th>
										<th>Número económico</th>
										<th>Placas</th>
										<th>Unidad/Descripción</th>
										<th>Marca/Modelo</th>
										<th>Número de serie</th>
										<th>Expedientes</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr>
								     	<th>#</th>
										<th>Propiedad</th>
										<th>Número económico</th>
										<th>Placas</th>
										<th>Unidad/Descripción</th>
										<th>Marca/Modelo</th>
										<th>Número de serie</th>
										<th>Expedientes</th>
										<th>Acciones</th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>

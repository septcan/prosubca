<div class="modal fade text-left" id="modal_archivo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Nuevo expediente</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
		        <div class="row">
					<div class="col-12">
					    <div class="form-group" >
							<label class="control-label">Archivo</label><br>
							<input id="archivo_file" type="file" style="width: 100%;">
						</div> 
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-12">
						<form class="form" method="post" role="form" id="form_registro">
							<fieldset class="form-group">
								<label>Nombre</label>
								<input type="text" name="nombre" id="nombre_f" class="form-control round">
								<input type="hidden" name="id" class="form-control round" value="0">
							</fieldset>
						</form>
					</div>
				</div>	
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-outline-info" onclick="guardar_registro()">Guardar</button>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade text-left" id="modal_archivo_listado" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Expedientes</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
				<div class="row">
					<div class="col-12">
						<div class="tabla_expedientes"></div>
					</div>
				</div>		
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		    </div>
		</div>
	</div>
</div>
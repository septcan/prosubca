<header class="page-header">
	<h2>Listado de productos</h2>
		<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url(); ?>Inicio">
					<i class="fa fa-home"></i>
				</a>
			</li>
		</ol>
		<a class="sidebar-right-toggle"></a>
	    </div>
</header>
<!-- inicio: page -->
<!-- start: page -->
<div class="col-md-8">
    <section class="panel">	
		<div class="panel-body">
			<div class="row">
				<div class="col-md-11">
					<table class="table mb-none" id="datatable">
						<thead>
							<tr>
								<th>#</th>
								<th>Código</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</section>
</div>
<div class="col-md-4">
    <section class="panel">	
    	<header class="panel-heading">
				<h2 class="panel-title"><span class="text_titulo">Nueva</span> categoría</h2>
		</header>
		<div class="panel-body">
			<div class="row">
	            <div class="col-md-12">
	           	    <form class="form" method="post" role="form" id="form_categoria">
				        <input type="hidden" name="idcategoria" id="idcategoria">
	                    <div class="row">
							<div class="col-sm-12">
								<div class="form-group">
									<label class="control-label">Categoria<span class="required">*</span></label>
									<input type="text" name="categoria" id="categoria" class="form-control">
								</div>
							</div>
						</div>
	           	    </form>
	            </div> 
		    </div>
		</div>  
		<footer class="panel-footer">
			<button class="btn btn-success btnguardar"><i class="fa fa-plus-square"></i><span class="btn_titulo">Guardar</span></button>
			<span class="btn_cancel"> </span>   
		</footer>	 
	</section> 
</div>

<!-- Modal Bootstrap -->
<div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-wrapper">
					<div class="modal-text text-center">
						<h4>¿Seguro que deseas eliminar la categoría <span class="text_nombre"></span>?</h4>
						<input type="hidden" id="id_categoria">
						<button class="btn btn-danger modal-confirm" data-dismiss="modal" onclick="delete_registro()"><i class="fa fa-trash"></i> Confirmar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			  
			</div>
		</div>
	</div>
</div>	
<input style="display:none" type="text" name="fakeusernameremembered"/>
<input style="display:none" type="password" name="fakepasswordremembered"/>
<input style="display:none" type="password" name="pwd" autocomplete="new-password">
<header class="page-header">
	<h2>Usuarios</h2>
	<div class="right-wrapper pull-right">
		<ol class="breadcrumbs">
			<li>
				<a href="<?php echo base_url(); ?>Inicio">
					<i class="fa fa-home"></i>
				</a>
			</li>
		</ol>
		<a class="sidebar-right-toggle"></a>
	</div>
</header>
<!-- start: page -->
<div class="row">
    <div class="col-md-12">
		<section class="panel">
            <header>
				<div class="row">
					<div class="col-md-3">
						<div class="input-group mb-md" <?php echo $list_block;?>>
							<span class="input-group-addon">
								<i class="fa fa-institution"></i>
							</span>
							<select class="form-control" id="sucursalselected" onchange="recargartable()">
								<option value="0">Todas</option>
								<?php foreach ($sucursalesrow->result() as $row) { ?>
				                            <option value="<?php echo $row->sucursalid;?>" <?php if ($sucursalId==$row->sucursalid) { echo 'selected'; } ?> ><?php echo $row->sucursal?></option>
				                        <?php } ?>
							</select>
						</div>
					</div>
					<div class="col-md-7">
						
					</div>
					<div class="col-md-2" align="right">
						<a type="button" class="mb-xs mt-xs mr-xs btn btn-primary" onclick="modalregistro()"><i class="fa fa-user-plus"></i> Nuevo</a>
					</div>
				</div>
		    </header>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12">
						<table class="table mb-none" id="datatable">
							<thead>
								<tr>
									<th>#</th>
									<th>Usuario</th>
									<th>Perfil</th>
									<th>Personal</th>
									<th>Sucursal</th>
									<th></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>
						
					</div>

				</div>
			</div>
		</section>
	</div>
</div>
<!-- Modal Bootstrap -->
<div class="modal fade" id="modalregistro" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
					<h4 class="modal-title" id="myModalLabel"><span class="titulo"></span> Usuario</h4>
				</div>
				<div class="modal-body">
				    <!-- -->	
					<form class="form" method="post" role="form" id="form_usuario" autocomplete="off">
						<input type="hidden" name="UsuarioID" id="UsuarioID" value="0">
						<div class="panel-body">
							<div class="registro_usu">
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Nombre de usuario<span class="required">*</span></label>
											<div class="input-group input-group-icon">
												<span class="input_usuario"></span>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Contraseña</label>
											<div class="input-group input-group-icon">
												<input type="password" name="contrasena" id="contrasena" class="form-control" oninput="verificapass()">
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-sm-12">
										<div class="form-group">
											<label class="control-label">Verificar contraseña</label>
											<div class="input-group input-group-icon">
												<span class="input-group-addon">
													<span class="icon"><i class="fa fa-lock"></i></span>
												</span>
												<input type="password" id="contrasenav" class="form-control" oninput="verificapass()">
												<div style="color: red;" class="passveri"></div>
											</div>
										</div>
									</div>
								</div>
                            </div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Personal <button type="button" class="mb-xs mt-xs mr-xs btn btn-xs btn-default" data-toggle="popover" data-container="body" data-placement="top" title="Nota:" data-content="El usuario que se le cree al personal ingresara a la sucursal que tiene asignada."><i class="fa fa-comment"></i></button></label>
										<div class="input-group btn-group">
											<span class="input-group-addon">
												<span class="icon"><i class="fa fa-user"></i></span>
											</span>
											<select class="form-control" name="personalId" id="personalId">
												    <option selected disabled="">Selecciona un personal</option>
												<?php foreach ($personal as $row) { ?>
						                            <option value="<?php echo $row->personalId ?>"><?php echo $row->nombre ?> <?php echo $row->apellidopaterno ?> <?php echo $row->apellidomaterno ?> ( <?php echo $row->sucursal ?> )</option>
						                        <?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label class="control-label">Perfil</label>
										<div class="input-group btn-group">
											<span class="input-group-addon">
												<span class="icon"><i class="fa fa-user"></i></span>
											</span>
											<select class="form-control" name="perfilId" id="perfilId">
												    <option selected disabled="">Selecciona un perfil</option>
												<?php foreach ($perfiles->result() as $row) { ?>
						                            <option value="<?php echo $row->perfilId ?>"><?php echo $row->perfil ?></option>
						                        <?php } ?>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
					</form>	
				    <!-- -->
				</div>
				<div class="modal-footer">
					
					<button type="button" class="btn btn-success btn_registro"><span class="btn_text">Guardar</span></button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
				</div>
		</div>
	</div>
</div>	
<!-- Modal Bootstrap -->
<div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<div class="modal-wrapper">
					<div class="modal-text text-center">
						<i style="font-size: 50px;color: red;" class="fa fa-times-circle"></i><hr>
						<h4>¿Seguro que deseas eliminar a <span class="text_nombre"></span>?</h4>
						<input type="hidden" id="usuID">
						<button class="btn btn-success modal-confirm" data-dismiss="modal" onclick="delete_registro()">Confirmar</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
					</div>
				</div>
			  
			</div>
		</div>
	</div>
</div>	
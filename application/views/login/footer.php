<!-- end: page -->

		<!-- -->
		<script src="<?php echo base_url(); ?>plugin/jquery/jquery.js"></script>
		<script src="<?php echo base_url(); ?>plugin/jquery-browser-mobile/jquery.browser.mobile.js"></script>
		<script src="<?php echo base_url(); ?>plugin/bootstrap/js/bootstrap.js"></script>
		<script src="<?php echo base_url(); ?>plugin/nanoscroller/nanoscroller.js"></script>
		<script src="<?php echo base_url(); ?>plugin/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="<?php echo base_url(); ?>plugin/magnific-popup/magnific-popup.js"></script>
		<script src="<?php echo base_url(); ?>plugin/jquery-placeholder/jquery.placeholder.js"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="<?php echo base_url(); ?>plugin/javascripts/theme.js"></script>
		
		<!-- Theme Custom -->
		<script src="<?php echo base_url(); ?>plugin/javascripts/theme.custom.js"></script>
		
		<!-- Theme Initialization Files -->
		<script src="<?php echo base_url(); ?>plugin/javascripts/theme.init.js"></script>
		<script src="<?php echo base_url(); ?>plugin/jquery-validation/jquery.validate.js"></script>

	</body>
</html>
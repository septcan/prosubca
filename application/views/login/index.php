<div class="bookshelf_wrapper cargando" style="display:none">
  <ul class="books_list">
    <li class="book_item first"></li>
    <li class="book_item second"></li>
    <li class="book_item third"></li>
    <li class="book_item fourth"></li>
    <li class="book_item fifth"></li>
    <li class="book_item sixth"></li>
  </ul>
  <div class="shelf"></div>
</div>
<ul class="cuadrados">
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
                <li></li>
            </ul>
<!-- start: page <section class="body-sign body-locked">-->
<section class="body-sign body-locked" style="display:block">
	<div class="center-sign">
		<div class="panel panel-sign" style="position: relative; z-index: 2;">
			<div class="panel-body" style="border-radius: 30px;">
				<form action="index.html" id="login-form">
					<div class="current-user text-center">
						<img src="<?php echo base_url(); ?>public/img/icon.png" class="img-circle user-image" style="background: white; padding: 8px;"/>
					</div>
                    <h3 align="center" style="color: #66903b;" ><b>Prosubca</b></h3>
					<div class="form-group mb-lg">
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon icon-lg" style="font-size: 25px;">
									<i class="fa fa-user"></i>
								</span>
							</span>
							<input name="usuario" id="usuario" type="text" class="form-control input-lg" placeholder="Usuario" />
							
						</div>
						<br>
						<div class="input-group input-group-icon">
							<span class="input-group-addon">
								<span class="icon icon-lg" style="font-size: 25px;">
									<i class="fa fa-lock"></i>
								</span>
							</span>
							<input name="password" id="password" type="password" class="form-control input-lg" placeholder="Contraseña" />
							
						</div>
					</div>
				</form>
				    <div class="row">
						<div class="col-xs-12" align="center">
							<button type="button" class="btn btn-outline btn-rounded btn-tertiary  btn-with-arrow mb-2 login" id="login-submit">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Iniciar sesión&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</button>
						</div>
					</div>
				<div class="row">
					<div class="col-md-12 sessionerronea" style="display: none;">
						<br>
						<div class="alert alert-danger" style=" border-radius: 20px; " align="center">
							<strong>El nombre de Usuario y/o <br> Contraseña es incorrecto.</strong>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>
<section">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-12">
					        <h3 style="border-bottom-style: inset;"><b>Notificaciones</b></h3>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">

						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
								<thead>
									<tr>
								     	<th>#</th>
										<th>Notificación</th>
										<th>Fecha</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
								<tr>
							    	<th>#</th>
									<th>Notificación</th>
									<th>Fecha</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
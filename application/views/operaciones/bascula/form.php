<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fileinput/fileinput.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b>Importar excel</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="row">
							<div class="col-4">
							    <div class="form-group" >
									<label class="control-label">Báscula</label><br>
									<input id="file_excel" type="file" style="width: 100%;">
								</div> 
							</div>
						</div>	
						<hr>
						<div class="row">
							<div class="col-sm-12"> 
							    <button type="button" class="btn btn-outline-info round mr-1 mb-1 btn_registro" onclick="guarda_excel()">Guardar <span><i class="fa fa-floppy-o"></i></span></button>
								<a href="<?php echo base_url();?>Basculas" class="btn btn-outline-secondary round mr-1 mb-1">Regresar <span><i class="fa fa-arrow-left"></i></span></a>
                            </div>	
                        </div>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>						
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/fileinput/fileinput.min.css">
<section>
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-9">
					        <h3 style="border-bottom-style: inset;"><b>Listado de báscula</b></h3>
                        </div>	
						<div class="col-3">
					        <a href="<?php echo base_url() ?>Bascula/registro" class="btn btn-outline-info round mr-1 mb-1" style="width: 100%;"><span><i class="fa fa-file-excel-o"></i></span> Importar excel</a>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
								<thead>
									<tr>
								     	<th>#</th>
								     	<th>FOLIO</th>
								     	<th>CLVPROV</th>
								     	<th>RAZSOC</th>
								     	<th>FECFOL</th>
								     	<th>FECSAL</th>
								     	<th>HORAENT</th>
								     	<th>HORASAL</th>
								     	<th>CHOFER</th>
								     	<th>PLACAS</th>
								     	<th>TRANSPORTE</th>
								     	<th>PROCEDE</th>
								     	<th>ORIGEN</th>
								     	<th>CLVPROD</th>
								     	<th>DESCRI</th>
								     	<th>CLVOPE</th>
								     	<th>NOMBRE</th>
								     	<th>PESO_ENVDO</th>
								     	<th>PESO_EMPAQ</th>
								     	<th>PBRUTO</th>
								     	<th>TARA</th>
								     	<th>NETO</th>
								     	<th>PESO_REAL</th>
								     	<th>CANCEL</th>
								     	<th>CICLO</th>
                                        <th>MANUAL</th>
                                        <th></th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
									<tr>
								     	<th>#</th>
										<th>FOLIO</th>
										<th>CLVPROV</th>
										<th>RAZSOC</th>
										<th>FECFOL</th>
										<th>FECSAL</th>
										<th>HORAENT</th>
										<th>HORASAL</th>
										<th>CHOFER</th>
										<th>PLACAS</th>
										<th>TRANSPORTE</th>
										<th>PROCEDE</th>
										<th>ORIGEN</th>
										<th>CLVPROD</th>
										<th>DESCRI</th>
										<th>CLVOPE</th>
										<th>NOMBRE</th>
										<th>PESO_ENVDO</th>
										<th>PESO_EMPAQ</th>
										<th>PBRUTO</th>
										<th>TARA</th>
										<th>NETO</th>
										<th>PESO_REAL</th>
										<th>CANCEL</th>
										<th>CICLO</th>
										<th>MANUAL</th>
										<th></th>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

</section>




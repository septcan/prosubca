<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<input type="hidden" id="idreg" value="">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b> Bitácora de colecta</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<form class="form" method="post" role="form" id="form_registro">
							<div class="row">
								<div class="col-sm-6">
									<fieldset class="form-group">
										<label>Operador</label>
										<select class="form-control round" id="idpersonal">
          
										</select>
									</fieldset>
								</div>
								<div class="col-sm-3">
									<fieldset class="form-group">
										<label>Día</label>
										<input type="date" id="fecha" class="form-control round">
									</fieldset>
								</div>
								<div class="col-sm-2">
									<fieldset class="form-group">
										<label style="color:white">__</label>
										<a class="btn btn-outline-info round mr-1 mb-1" style="width: 100%;  color:#66903b" onclick="dia_colecta()"><span><i class="fa fa-search"></i></span> Buscar </a>
									</fieldset>
								</div>
							</div>	
							<div class="folios_txt"></div>	
							<div class="folio_txt"></div>	
							<div class="folio_materia_txt"></div>
							<div class="folio_proveedor_materia_costos_txt"></div>
							<div class="folios_gastos"></div>
							<div class="folios_gastos_tabla"></div>	
						</form>	
						<hr>	
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

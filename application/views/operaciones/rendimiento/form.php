<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b>Rendimientos</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<input type="hidden" id="idrendimiento">
						<div class="row">
							<div class="col-sm-4">
								<fieldset class="form-group"> 
									<label>Operador</label>
									<select class="form-control round" id="idpersonal" style="width: 100%">
      
									</select>
								</fieldset>
							</div>
							<div class="col-sm-2">
								<fieldset class="form-group">
									<label>Año</label>
									<select  class="form-control round" id="fecha1">
									    <?php
								        for($i=date('o'); $i>=2020; $i--){
								            if ($i == date('o'))
								                echo '<option value="'.$i.'" selected>'.$i.'</option>';
								            else
								                echo '<option value="'.$i.'">'.$i.'</option>';
								        }
								        ?>
									</select>
								</fieldset>
							</div>
							<div class="col-sm-2">
								<fieldset class="form-group">
									<label>Mes</label>
									<select class="form-control round" id="fecha2">
									  <option value="">Selecciona opción</option>	
									  <option value="1">ENERO</option>
									  <option value="2">FEBRERO</option>
									  <option value="3">MARZO</option>
									  <option value="4">ABRIL</option>
									  <option value="5">MAYO</option>
									  <option value="6">JUNIO</option>
									  <option value="7">JULIO</option>
									  <option value="8">AGOSTO</option>
									  <option value="9">SEPTIEMBRE</option>
									  <option value="10">OCTUBRE</option>
									  <option value="11">NOVIEMBRE</option>
									  <option value="12">DICIEMBRE</option>
									</select>
								</fieldset>
							</div>

							<div class="col-sm-2">
								<fieldset class="form-group">
									<label>Unidad</label>
									<select class="form-control round" id="unidad" style="width:100%">
									</select>
								</fieldset>
							</div>
							<div class="col-sm-2">
								<fieldset class="form-group">
									<label style="color: #ff000000;">__</label><br>
									<button onclick="burcar_registro()" class="btn btn-outline-info round mr-1 mb-1" style="width:100%">Buscar</button>
								</fieldset>
							</div> 
						</div>	
						<div class="tabla_text">	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>						 
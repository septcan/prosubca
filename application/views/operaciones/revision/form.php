<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/select2.min.css">
<section id="input-style">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3 style="border-bottom-style: inset;"><b>Revisión de compras y gastos</b></h3>
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="row">
							<div class="col-sm-6">
								<fieldset class="form-group" > 
									<label>Operador</label>
									<select class="form-control round" id="idpersonal" style="width: 100%">
      
									</select>
								</fieldset>
							</div>
							<div class="col-sm-3 dia_">
								<fieldset class="form-group">
									<label>Día</label>
									<input type="date" id="fecha" class="form-control round">
								</fieldset>
							</div>
							<div class="col-sm-2">
								<fieldset class="form-group">
									<label style="color:white">__</label>
									<a class="btn btn-outline-info round mr-1 mb-1" style="width: 100%;  color:#66903b" onclick="dia_colecta()"><span><i class="fa fa-search"></i></span> Buscar </a>
								</fieldset>
							</div>
						</div>	
						<div class="comprar_text">	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>						
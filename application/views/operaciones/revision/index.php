<section">
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<div class="row">
						<div class="col-12">
					        <h3 style="border-bottom-style: inset;"><b>Listado de revisión de compras y gastos</b></h3>
                        </div>	
					</div>	
				</div>
				<div class="card-content">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table table-striped table-bordered" id="datatable" style="width: 100%;"> 
								<thead>
									<tr>
								     	<th>#</th>
										<th>Operador</th>
										<th>Fecha</th>
										<th>Costo de flete</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
								</tbody>
								<tfoot>
								<tr>
							    	<th>#</th>
									<th>Operador</th>
									<th>Fecha</th>
									<th>Costo de flete</th>
									<th>Acciones</th>
								</tr>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade text-left" id="modal_rutas_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Rutas</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<fieldset class="form-group" > 
							<label>Ruta</label>
							<select class="form-control round" id="idruta_colecta" style="width: 100%">

							</select>
						</fieldset>
					</div>
				</div>		
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-outline-info" onclick="guardar_ruta()">Guardar</button>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade text-left" id="modal_unidad_editar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Unidades</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<fieldset class="form-group" > 
							<label>Unidad</label>
							<select class="form-control round" id="idunidad_colecta" style="width: 100%">

							</select>
						</fieldset>
					</div>
				</div>	
				<div class="row">
					<div class="col-sm-12">
						<div class="custom-switch custom-switch-success custom-control-inline mb-1 mb-xl-0">
                            <input type="checkbox" class="custom-control-input" id="validar_unidades">
                            <label class="custom-control-label mr-1" for="validar_unidades">
                                <span>Validar todas las unidades</span>
                            </label>
                        </div>
					</div>
				</div>		
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-outline-info" onclick="guardar_unidad()">Guardar</button>
		    </div>
		</div>
	</div>
</div>

<div class="modal fade text-left" id="modal_unidad_editar_general" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		    <div class="modal-header">
		        <h4 class="modal-title" id="myModalLabel1">Unidades</h4>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		            <span aria-hidden="true"><i class="ft-x font-medium-2 text-bold-700"></i></span>
		        </button>
		    </div>
		    <div class="modal-body">
				<div class="row">
					<div class="col-sm-12">
						<fieldset class="form-group" > 
							<label>Unidad</label>
							<select class="form-control round" id="idunidad_colecta_general" style="width: 100%">

							</select>
						</fieldset>
					</div>
				</div>		
		    </div>
		    <div class="modal-footer">
		        <button type="button" class="btn bg-light-secondary" data-dismiss="modal">Cerrar</button>
		        <button type="button" class="btn btn-outline-info" onclick="guardar_unidad_general()">Guardar</button>
		    </div>
		</div>
	</div>
</div>
<?php
require_once('TCPDF3/examples/tcpdf_include.php');
require_once('TCPDF3/tcpdf.php');
$this->load->helper('url');
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'public/img/header2.PNG';  
        $this->Image($img_file, 0, 0, 210, 45, '', '', '', false, 330, '', false, false, 0); 
        $html='<h1 style="color:white">INFORME DE ESTADOS DE CUENTA</h1>';//Informe de rendimiento
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/footer.PNG';  
        $this->Image($img_file, 0, 287, 210, 10, '', '', '', false, 330, '', false, false, 0); 
        $html='<br><br><br><br><br><br><br><br><br><br><br><h3 style="color:white">compras2@prosubca.com.mx</h3>';
        $this->writeHTML($html, true, false, true, false, '');
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Estados de cuenta');
$pdf->SetTitle('Estados de cuenta');
$pdf->SetSubject('Estados de cuenta');
$pdf->SetKeywords('Estados de cuenta');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(8, 20, 8); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 10);
// add a page
$pdf->AddPage('P', 'A4');
$pdf->SetXY(1,1);
$pdf->Write(0, "");
 
$mes_txt="";
if($get_info->mes==1){
    $mes_txt="ENERO";
}else if($get_info->mes==2){
    $mes_txt="FEBRERO";
}else if($get_info->mes==3){
    $mes_txt="MARZO";
}else if($get_info->mes==4){
    $mes_txt="ABRIL";
}else if($get_info->mes==5){
    $mes_txt="MAYO";
}else if($get_info->mes==6){
    $mes_txt="JUNIO";
}else if($get_info->mes==7){
    $mes_txt="JULIO";
}else if($get_info->mes==8){
    $mes_txt="AGOSTO";
}else if($get_info->mes==9){
    $mes_txt="SEPTIEMBRE";
}else if($get_info->mes==10){
    $mes_txt="OCTUBRE";
}else if($get_info->mes==11){
    $mes_txt="NOVIEMBRE";
}else if($get_info->mes==12){
    $mes_txt="DICIEMBRE";
}
   
$html='
  <style type="text/css">
    .httable{
        font-size:10px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;

    }

    .httabled{
        font-size:9px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;
        color:white;
        background-color:#66903b;
    }
    .httablecom{
        font-size:8px;
        border-bottom: 1px solid #66903b;
    }
    .magintablepro{
        margin-top:0px;
        margin-bottom:0px;
        margin: 0px;
    }
    .fontsize{
        font-size:10px;
        color:white;
        background-color:#66903b;
    }
  </style><br><br><br><br><br><table border="0"><tr>
            <th width="90%"><table border="0" cellpadding="2"><tr><th width="60%" class="fontsize"><b>OPERADOR</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$get_info->nombre.'</td>
                        
                    </tr>
                </table>
            </th>
        </tr>
        <tr>
            <th width="90%"><table border="0" cellpadding="2"><tr><th width="15%" class="fontsize"><b>AÑO</b></th>
                        <th width="45%" class="fontsize"><b>MES</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$get_info->anio.'</td><td class="httable">'.$mes_txt.'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';

$html.='<br><br><br><table border="0"><tr><th width="45%"><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize"><b>RENDIMIENTO</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">$'.number_format($get_info->rendimiento,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
            <th width="10%"></th>
            <th width="45%">
                <table border="0" cellpadding="2"><tr><th width="100%" class="fontsize"><b>SALDO FINAL</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">$'.number_format($get_info->saldo_final,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';


$html.='<br><br><br><table border="0" cellpadding="2">
    <tr>
      <th width="10%" class="httabled"><b>FECHA</b></th>
      <th width="45%" class="httabled"><b>RUTA</b></th>
      <th width="15%" class="httabled"><b>DEPOSITO</b></th>
      <th width="15%" class="httabled"><b>COMPRA/GASTO</b></th>
      <th width="15%" class="httabled"><b>SALDO</b></th>
    </tr>
';

    foreach ($get_info_detalle as $x){
        $total_saldo=$x->saldo;
        $color_saldo='';
        if($x->saldo!=0){
            if($total_saldo<0){
                $color_saldo='style="color:red"'; 
            }
        }else{
            $total_saldo=0; 
        }
        $html.='<tr>
            <td class="httablecom">'.date('d/m/Y',strtotime($x->dia)).'</td>
            <td class="httablecom">'.$x->ruta.'</td>
            <td class="httablecom">$'.number_format($x->deposito, 2, '.', ',').'</td>
            <td class="httablecom">$'.number_format($x->total_compras_gastos, 2, '.', ',').'</td>
            <td class="httablecom"><span '.$color_saldo.'>$'.number_format($total_saldo, 2, '.', ',').'</span></td>
        </tr>';
    }

$html.='</table><br><br>';

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('estadocuenta_'.$get_info->nombre.'_'.$get_info->anio.'_'.$get_info->anio.'.pdf', 'I');
?>
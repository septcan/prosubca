<?php
require_once('TCPDF3/examples/tcpdf_include.php');
require_once('TCPDF3/tcpdf.php');
$this->load->helper('url');
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'public/img/header2.PNG';  
        $this->Image($img_file, 0, 0, 210, 45, '', '', '', false, 330, '', false, false, 0); 
        $html='<h1 style="color:white">INFORME DE RENDIMIENTO</h1>';//Informe de rendimiento
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/footer.PNG';  
        $this->Image($img_file, 0, 287, 210, 10, '', '', '', false, 330, '', false, false, 0); 
        $html='<br><br><br><br><br><br><br><br><br><br><br><h3 style="color:white">compras2@prosubca.com.mx</h3>';
        $this->writeHTML($html, true, false, true, false, '');
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Rendimiento');
$pdf->SetTitle('Rendimiento');
$pdf->SetSubject('Rendimiento');
$pdf->SetKeywords('Rendimiento');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(8, 20, 8); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 10);
// add a page
$pdf->AddPage('P', 'A4');
$pdf->SetXY(1,1);
$pdf->Write(0, "");
 
$mes_txt="";
if($get_rendimiento->mes==1){
    $mes_txt="ENERO";
}else if($get_rendimiento->mes==2){
    $mes_txt="FEBRERO";
}else if($get_rendimiento->mes==3){
    $mes_txt="MARZO";
}else if($get_rendimiento->mes==4){
    $mes_txt="ABRIL";
}else if($get_rendimiento->mes==5){
    $mes_txt="MAYO";
}else if($get_rendimiento->mes==6){
    $mes_txt="JUNIO";
}else if($get_rendimiento->mes==7){
    $mes_txt="JULIO";
}else if($get_rendimiento->mes==8){
    $mes_txt="AGOSTO";
}else if($get_rendimiento->mes==9){
    $mes_txt="SEPTIEMBRE";
}else if($get_rendimiento->mes==10){
    $mes_txt="OCTUBRE";
}else if($get_rendimiento->mes==11){
    $mes_txt="NOVIEMBRE";
}else if($get_rendimiento->mes==12){
    $mes_txt="DICIEMBRE";
}

$conbustible='';
if($get_rendimiento->tipo_combustible==1){
    $conbustible='Disel';
}else if($get_rendimiento->tipo_combustible==2){
    $conbustible='Gas';
}else if($get_rendimiento->tipo_combustible==3){
    $conbustible='Gasolina';
}    
$html='
  <style type="text/css">
    .httable{
        font-size:10px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;

    }

    .httabled{
        font-size:9px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;
        color:white;
        background-color:#66903b;
    }
    .httablecom{
        font-size:8px;
        border-bottom: 1px solid #66903b;
    }
    .magintablepro{
        margin-top:0px;
        margin-bottom:0px;
        margin: 0px;
    }
    .fontsize{
        font-size:10px;
        color:white;
        background-color:#66903b;
    }
  </style><br><br><br><br><br><table border="0"><tr>
            <th width="90%"><table border="0" cellpadding="2">
                    <tr><th width="40%" class="fontsize"><b>OPERADOR</b></th>
                        <th width="20%" class="fontsize"><b>AÑO</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$get_rendimiento->nombre.'</td>
                        <td class="httable">'.$get_rendimiento->anio.'</td>
                    </tr>
                </table>
            </th>
        </tr>
        <tr>
            <th width="90%"><table border="0" cellpadding="2">
                    <tr>
                        <th width="40%" class="fontsize"><b>MES</b></th>
                        <th width="20%" class="fontsize"><b>UNIDAD</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$mes_txt.'</td>
                        <td class="httable">'.$get_rendimiento->placas.'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';

$html.='<br><br><br><table border="0"><tr>
            <th width="45%"><table border="0" cellpadding="2">
                    <tr><th width="60%" class="fontsize"><b>COMBUSTIBLE</b></th>
                        <th width="40%" class="fontsize"><b>LITROS</b></th>
                    </tr>
                    <tr><td width="60%" class="httable">'.$conbustible.'</td>
                        <td width="40%" class="httable">$'.number_format($get_rendimiento->precio_litros,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
            <th width="10%"></th>
            <th width="45%">
                <table border="0" cellpadding="2">
                    <tr><th width="100%" class="fontsize"><b>RENDIMIENTO ESPERADO</b></th></tr>
                    <tr>
                        <td class="httable">'.number_format($get_rendimiento->rendimiento_esperado,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';


$html.='<br><br><table border="0" cellpadding="2">
    <tr>
      <th width="13%" class="httabled"><b>RUTA</b></th>
      <th width="9%" class="httabled"><b>FECHA</b></th>
      <th width="10%" class="httabled"><b>FAC.</b></th>
      <th width="10%" class="httabled"><b>VALES</b></th>
      <th width="10%" class="httabled"><b>INICIAL</b></th>
      <th width="8%" class="httabled"><b>FINAL</b></th>
      <th width="10%" class="httabled"><b>RECORRIDO</b></th>
      <th width="9%" class="httabled"><b>LITROS</b></th>
      <th width="11%" class="httabled"><b>RENDIMIENTO</b></th>
      <th width="10%" class="httabled"><b>UNIDAD</b></th>
    </tr>
';


    foreach ($result_detalles as $x){
        $total_ruta=0;
        $result=$this->ModeloRendimientos->total_ruta_bitacora_colecta($x->id);
        foreach ($result as $ru){
            $total_ruta=$ru->ruta; 
        }
        $kmin=$x->km_inicial/$total_ruta;
        $kmfi=$x->km_final/$total_ruta;
        $kmre=$x->km_recorrido/$total_ruta;
        $kmlt=$x->litros/$total_ruta;

        $rendimiento=$kmre/$kmlt;
        $rendimiento_decimal = number_format($rendimiento, 2, '.', ',');
        $html.='<tr>
            <td class="httablecom">'.$x->ruta.'</td>
            <td class="httablecom">'.date('d/m/Y',strtotime($x->dia)).'</td>
            <td class="httablecom">'.$x->numero_factura.'</td>
            <td class="httablecom">'.$x->num_vale.'</td>
            <td class="httablecom">'.number_format($kmin, 2, '.', ',').'</td>
            <td class="httablecom">'.number_format($kmfi, 2, '.', ',').'</td>
            <td class="httablecom">'.number_format($kmre, 2, '.', ',').'</td>
            <td class="httablecom">'.number_format($kmlt, 2, '.', ',').'</td>
            <td class="httablecom">'.$rendimiento_decimal.'</td>
            <td class="httablecom">'.$x->placas.'</td>
        </tr>';
    }

$html.='</table><br><br>';

$html.='<table border="0" cellpadding="2">
    <tr>
      <th width="100%" class="httabled" align="center"><b>RESUMEN GENERAL</b></th>
    </tr>
</table>';
$html.='<table border="0"><tr><th width="45%"><table border="0" cellpadding="2">
                    <tr><td width="60%" class="httable">KM RECORRIDOS</td>
                        <td width="40%" class="httable">'.number_format($get_rendimiento->km_recorridos,2,'.',',').'</td>
                    </tr>
                    <tr><td width="60%" class="httable">LITROS CONSUMIDOS</td>
                        <td width="40%" class="httable">'.number_format($get_rendimiento->litros_consumidos,2,'.',',').'</td>
                    </tr>
                    <tr><td width="60%" class="httable">RENDIMIENTO</td>
                        <td width="40%" class="httable">'.number_format($get_rendimiento->rendimiento,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
            <th width="10%"></th>
            <th width="45%">
                <table border="0" cellpadding="2">
                    <tr><td width="60%" class="httable">LITROS CONSUMIDOS ESP.</td>
                        <td width="40%" class="httable">'.number_format($get_rendimiento->litros_consumidos_esp,2,'.',',').'</td>
                    </tr>
                    <tr><td width="60%" class="httable">LITROS DE DIFERENCIA</td>
                        <td width="40%" class="httable">'.number_format($get_rendimiento->litros_diferencia,2,'.',',').'</td>
                    </tr>
                    <tr><td width="60%" class="httable">TOTAL EFECTIVO</td>
                        <td width="40%" class="httable">$ '.number_format($get_rendimiento->total_efectivo,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('rendimiento_'.$get_rendimiento->nombre.'_'.$get_rendimiento->anio.'_'.$get_rendimiento->anio.'.pdf', 'I');
?>
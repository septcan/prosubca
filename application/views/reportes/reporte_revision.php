<?php
require_once('TCPDF3/examples/tcpdf_include.php');
require_once('TCPDF3/tcpdf.php');
$this->load->helper('url');
$GLOBAL['operador']=$operador;
$GLOBAL['dia']=$dia;
$GLOBAL['unidad']=$unidad;
$GLOBAL['sueldo']=$sueldo;

$GLOBAL['km_inicial']=$km_inicial;
$GLOBAL['km_final']=$km_final;
$GLOBAL['km_recorrido']=$km_recorrido;
$GLOBAL['tarjeta_pase']=$tarjeta_pase;
$GLOBAL['costo_flete']=$costo_flete;
//=======================================================================================
class MYPDF extends TCPDF {
    //Page header
    public function Header() {
        $img_file = base_url().'public/img/header2.PNG';  
        $this->Image($img_file, 0, 0, 210, 45, '', '', '', false, 330, '', false, false, 0); 
        $html='<h1 style="color:white">REVISIÓN DE COMPRAS Y GASTOS</h1>';//Informe de rendimiento
        $this->writeHTML($html, true, false, true, false, '');
    }
    // Page footer
    public function Footer() {
        $img_file = base_url().'public/img/footer.PNG';  
        $this->Image($img_file, 0, 287, 210, 10, '', '', '', false, 330, '', false, false, 0); 
        $html='<br><br><br><br><br><br><br><br><br><br><br><h3 style="color:white">compras2@prosubca.com.mx</h3>';
        $this->writeHTML($html, true, false, true, false, '');
    }
} 
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Revisión');
$pdf->SetTitle('Revisión');
$pdf->SetSubject('Revisión');
$pdf->SetKeywords('Revisión');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(8, 20, 8); 
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); 
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); 
$pdf->SetFooterMargin('60'); 
// set auto page breaks
$pdf->SetAutoPageBreak(true, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

$pdf->SetFont('dejavusans', '', 10);
// add a page
$pdf->AddPage('P', 'A4');
$pdf->SetXY(1,1);
$pdf->Write(0, "");

$html='
  <style type="text/css">
    .httable{
        font-size:10px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;

    }

    .httabled{
        font-size:9px;
        border-bottom: 1px solid #66903b;
        border-top: 1px solid #66903b;
        color:white;
        background-color:#66903b;
    }
    .httablecom{
        font-size:8px;
        border-bottom: 1px solid #66903b;
    }
    .magintablepro{
        margin-top:0px;
        margin-bottom:0px;
        margin: 0px;
    }
    .fontsize{
        font-size:10px;
        color:white;
        background-color:#66903b;
    }
  </style><br><br><br><br>';
$html.='<table border="0"><tr>
            <th width="90%"><table border="0" cellpadding="2"><tr><th width="60%" class="fontsize"><b>OPERADOR</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$GLOBAL['operador'].'</td>
                        
                    </tr>
                </table>
            </th>
        </tr>
        <tr>
            <th width="90%"><table border="0" cellpadding="2"><tr><th width="15%" class="fontsize"><b>DÍA</b></th>
                        <th width="45%" class="fontsize"><b>UNIDAD</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">'.$GLOBAL['dia'].'</td><td class="httable">'.$GLOBAL['unidad'].'</td>
                    </tr>
                </table>
            </th>
        </tr>  
    </table>';
$html.='<br><br><br><br><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize" align="center"><b>COMPRAS</b></th>
        </tr>  
    </table>';
$html.='<br><table border="0" cellpadding="2"><tr>
      <th width="25%" class="httabled"><b>PROVEEDOR</b></th>
      <th width="22%" class="httabled"><b>PRODUCTO</b></th>
      <th width="8%" class="httabled"><b>SE FAC.</b></th>
      <th width="10%" class="httabled"><b>FACTURA</b></th>
      <th width="11%" class="httabled"><b>CANTIDAD</b></th>
      <th width="11%" class="httabled"><b>PRECIO</b></th>
      <th width="13%" class="httabled"><b>IMPORTE</b></th>
    </tr>
';

$suma_compras=0;
foreach ($result_compra as $x) {
    $sefac='';
    if($x->factura==1){
        $sefac='SI';
    }else{
        $sefac='NO';
    }

    $total_c=0;
    $precio_c=0;
    if($x->metodo_pago==2){
        $total_c=0;
        $precio_c=0;
        $suma_compras+=0; 
    }else{
        $total_c=$x->total;
        $precio_c=$x->precio;
        $suma_compras+=$x->total; 
    }


    $html.='
      <tr class="magintablepro">
        <td class="httablecom">'.$x->nombre.'</td>
        <td class="httablecom">'.$x->materia.'</td>
        <td class="httablecom">'.$sefac.'</td>
        <td class="httablecom">'.$x->numero_factura.'</td>
        <td class="httablecom">'.$x->kilos.'</td>
        <td class="httablecom">$ '.number_format($precio_c,2,'.',',').'</td>
        <td class="httablecom">$ '.number_format($total_c,2,'.',',').'</td>
      </tr>';  
}
$html.='</table>';

$html.='<br><br><br><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize" align="center"><b>GASTOS</b></th>
        </tr>  
    </table>';
$html.='<br><table border="0" cellpadding="2"><tr>
      <th width="15%" class="httabled"><b>FECHA</b></th>
      <th width="30%" class="httabled"><b>DESCRIPCIÓN</b></th>
      <th width="15%" class="httabled"><b>FACTURA</b></th>
      <th width="20%" class="httabled"><b>EDITAR GASTO</b></th>
      <th width="20%" class="httabled"><b>TOTAL</b></th>
    </tr>
';
$suma_gastos=0;
foreach ($result_gasto as $g) {
    $html.='<tr class="magintablepro">
        <td class="httablecom">'.date('d/m/Y',strtotime($g->dia)).'</td>
        <td class="httablecom">'.$g->categoria.'</td>
        <td class="httablecom">'.$g->numero_factura.'</td>
        <td class="httablecom">$ '.number_format($g->importe_edicion,2,'.',',').'</td>
        <td class="httablecom">$ '.number_format($g->importe,2,'.',',').'</td>
    </tr>';
    $suma_gastos+=$g->importe;
}
$html.='</table>';
$suma_compras_gastos=$suma_compras+$suma_gastos;
$html.='<br><br><table border="0" cellpadding="2"><tr><th width="50%" class="fontsize" align="center"><b>TOTAL COMPRAS</b></th>
        <th width="50%" class="fontsize" align="center"><b>TOTAL GASTOS</b></th>
    </tr>
    <tr><td class="httablecom" align="center">$ '.number_format($suma_compras,2,'.',',').'</td>
        <td class="httablecom" align="center">$ '.number_format($suma_gastos,2,'.',',').'</td>
    </tr> 
</table>'; 

$html.='<br><br><br><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize" align="center"><b>VALES</b></th>
        </tr>  
    </table>';
$html.='<br><table border="0" cellpadding="2"><tr>
      <th width="30%" class="httabled"><b>TIPO DE VALE</b></th>
      <th width="20%" class="httabled"><b>NÚMERO DE VALE</b></th>
      <th width="20%" class="httabled"><b>LITROS</b></th>
      <th width="15%" class="httabled"><b>PRECIO</b></th>
      <th width="15%" class="httabled"><b>IMPORTE</b></th>
    </tr>
';
$total_vale=0;
foreach ($result_vale->result() as $v) {
    $tipo='';
    if($v->tipo_vale==1){
        $tipo='Gas';
    }else if($v->tipo_vale==2){
        $tipo='Diésel';
    }else if($v->tipo_vale==3){
        $tipo='Gasolina';
    }
    $total_vale+=$v->importe;
    $html.='<tr class="magintablepro">
        <td class="httablecom">'.$tipo.'</td>
        <td class="httablecom">'.$v->num_vale.'</td>
        <td class="httablecom">'.$v->litros.'</td>
        <td class="httablecom">$ '.number_format($v->precio,2,'.',',').'</td>
        <td class="httablecom">$ '.number_format($v->importe,2,'.',',').'</td>
    </tr>';
}
$html.='</table>';

$html.='<table border="0" cellpadding="2"><tr><th width="10%" class="fontsize"><b>TOTAL</b></th>
        <td width="90%" class="httablecom">$ '.number_format($total_vale,2,'.',',').'</td>
    </tr> 
</table>'; 



$html.='<br><br><table border="0" ><tr>
            <th width="45%"><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize" align="center"><b>SUELDO DE OPERADOR</b></th>
                    </tr>  
                </table>';
                $html.='<br><table border="0" cellpadding="2">
                    <tr>
                      <th width="70%" class="httabled"><b>OPERADOR</b></th>
                      <th width="30%" class="httabled"><b>SUELDO</b></th>
                    </tr>
                    <tr>
                      <td class="httablecom">'.$GLOBAL['operador'].'</td>
                      <td class="httablecom">$ '.number_format($GLOBAL['sueldo'],2,'.',',').'</td>
                    </tr>
                </table>
            </th>
            <th width="10%"></th>
            <th width="45%"><table border="0" cellpadding="2"><tr><th width="100%" class="fontsize" align="center"><b>AYUDANTES</b></th>
                    </tr>  
                </table>';
                $html.='<br><table border="0" cellpadding="2">
                        <tr>
                          <th width="70%" class="httabled"><b>NOMBRE</b></th>
                          <th width="30%" class="httabled"><b>SUELDO</b></th>
                        </tr>
                    ';
                    $total_vale=0;
                    foreach ($result_ayudante->result() as $ay) {
                        $get_personal=$this->ModeloCatalogos->getselectrowwheren_row('personal',array('personalId'=>$ay->personalId));
                        $html.='<tr class="magintablepro">
                            <td class="httablecom">'.$get_personal->nombre.'</td>
                            <td class="httablecom">$ '.number_format($ay->sueldo,2,'.',',').'</td>
                        </tr>';
                    }
                    $html.='</table>
            </th>
        </tr>
    </table>';

$html.='<br><br><table border="0" cellpadding="2">
    <tr>
      <th width="15%" class="httabled"><b>KILÓMETRAJE INICIAL</b></th>
      <th width="15%" class="httabled"><b>KILÓMETRAJE FINAL</b></th>
      <th width="30%" class="httabled"><b>KILÓMETRAJE<br>RECORRIDOS</b></th>
      <th width="20%" class="httabled"><b>TARJETA PASEL</b></th>
      <th width="20%" class="httabled"><b>COSTO DE FLETE</b></th>
    </tr>
    <tr class="magintablepro">
        <td class="httablecom">'.$GLOBAL['km_inicial'].'</td>
        <td class="httablecom">'.$GLOBAL['km_final'].'</td>
        <td class="httablecom">'.$GLOBAL['km_recorrido'].'</td>
        <td class="httablecom">'.$GLOBAL['tarjeta_pase'].'</td>
        <td class="httablecom">$ '.number_format($GLOBAL['costo_flete'],2,'.',',').'</td>
    </tr>
</table>';

$html.='<br><br><table border="0" cellpadding="2"><tr><th width="35%"><table border="0" cellpadding="2">
                    <tr><th width="100%" class="fontsize"><b>TOTAL COMPRAS Y GASTOS</b></th>
                    </tr>  
                    <tr class="magintablepro"><td class="httable">$'.number_format($suma_compras_gastos,2,'.',',').'</td>
                    </tr>
                </table>
            </th>
            <th width="15%"></th>
            <th width="50%"><table border="0" cellpadding="2">';
                    $html.='<tr><th width="100%" class="fontsize" align="center"><b>TOTAL MATERIA PRIMA</b></th>
                    </tr>';
                    $total_kilos_materia=0;        
                    foreach ($result_compras_total as $xt){
                        $html.='<tr>
                            <th width="70%" class="httablecom">'.$xt->materia.'</th>
                            <th width="30%" class="httablecom">'.$xt->kilos.'</th>
                        </tr>';
                        $total_kilos_materia+=$xt->kilos;
                    }
                    $html.='<tr>
                        <th width="70%" class="httable"><b>Kilos totales</b></th>
                        <th width="30%" class="httable"><b>'.$total_kilos_materia.'</b></th>
                    </tr>';
                $html.='</table>
            </th>
            <th width="10%"></th>
        </tr>  
    </table>';
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->Output('Revision_'.$GLOBAL['operador'].'_'.$GLOBAL['dia'].'_'.$GLOBAL['unidad'].'.pdf', 'I');
?>
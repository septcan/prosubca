<!doctype html>
<html class="loading" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Prosubca</title>
	<meta name="keywords" content="" />
	<meta name="description" content="">
	<meta name="author" content="rul">
	<link rel="icon" href="<?php echo base_url(); ?>public/img/icon.png" type="image/x-icon" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/feather/style.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/simple-line-icons/style.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/fonts/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/perfect-scrollbar.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/prism.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/switchery.min.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/chartist.min.css">
	<!-- END VENDOR CSS-->
	<!-- BEGIN APEX CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/bootstrap-extended.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/colors.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/components.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/css/themes/layout-dark.css">
	<link rel="stylesheet" href="<?php echo base_url(); ?>app-assets/css/plugins/switchery.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>app-assets/vendors/css/datatables/dataTables.bootstrap4.min.css">
	<!-- END APEX CSS-->
	<!-- BEGIN Page Level CSS-->
	<!-- END Page Level CSS-->
	<!-- BEGIN: Custom CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugin/sweealert/sweetalert.css">
</head>
<input type="hidden" id="base_url" value="<?php echo base_url(); ?>">

<body class="vertical-layout vertical-menu 2-columns navbar-sticky pace-done nav-collapsed" data-menu="vertical-menu" data-col="2-columns">
<!--
<body class="vertical-layout vertical-menu 2-columns  navbar-sticky" data-menu="vertical-menu" data-col="2-columns">-->
		

<?php
	if(!$this->session->userdata('logeado')){
	    redirect('Login');
	    $perfilid =0;
	}else{
	    $perfilid=$this->session->userdata('perfilid');
	    $personalId=$this->session->userdata('idpersonal');
	    $nombre=$this->session->userdata('nombre');
	    $sexo=$this->session->userdata('sexo');
		$perfiln=$this->session->userdata('perfiln');
        $encry = $this->encrypt->encode($personalId);
	}
	$getperfil=$this->ModeloSession->getperfil($perfilid);
	foreach ($getperfil as $item) {
	    $perfil=$item->perfil;
	    //$foto=$item->foto;
	}

    
    $result_alertas_total=$this->ModeloSession->getalertas_total($personalId);
    $total_alert=0;
    foreach ($result_alertas_total as $at){
        $total_alert=$at->total;
    }
    $result_alertas=$this->ModeloSession->getalertas($personalId);
	?>	
<?php
  $logueo = $this->session->userdata();
  $menu=$this->ModeloSession->menus($logueo['perfilid']);
  if ($logueo['sexo']==1) {
      $imglogo=base_url().'img/avatar/obreroh.png';
  }else{
      $imglogo=base_url().'img/avatar/obrerom.png';
  }
?>

    <nav class="navbar navbar-expand-lg navbar-light header-navbar navbar-fixed">
        <div class="container-fluid navbar-wrapper">
            <div class="navbar-header d-flex">
                <div class="navbar-toggle menu-toggle d-xl-none d-block float-left align-items-center justify-content-center" data-toggle="collapse"><i class="ft-menu font-medium-3"></i></div>
                <ul class="navbar-nav">
                    <li class="nav-item mr-2 d-none d-lg-block"><a class="nav-link apptogglefullscreen" id="navbar-fullscreen" href="javascript:;"><i class="ft-maximize font-medium-3"></i></a></li>
                </ul>
            </div>
            <div class="navbar-container">
                <div class="collapse navbar-collapse d-block" id="navbarSupportedContent">
                    <ul class="navbar-nav">
                        <li class="dropdown nav-item"><a class="nav-link dropdown-toggle dropdown-notification p-0 mt-2" id="dropdownBasic1" href="javascript:;" data-toggle="dropdown"><i class="ft-bell font-medium-3"></i><span class="notification badge badge-pill badge-danger total_alert"><?php echo $total_alert ?></span></a>
                            <ul class="notification-dropdown dropdown-menu dropdown-menu-media dropdown-menu-right m-0 overflow-hidden">
                                <li class="dropdown-menu-header">
                                    <div class="dropdown-header d-flex justify-content-between m-0 px-3 py-2 white bg-primary">
                                        <div class="d-flex"><i class="ft-bell font-medium-3 d-flex align-items-center mr-2"></i><span class="noti-title"><span class="total_alert2"><?php echo $total_alert ?></span> Notificaciones </span></div><!-- <span class="text-bold-400 cursor-pointer"> Marcar todo como leido</span> -->
                                    </div>
                                </li>
                                <li class="scrollable-container">
                                    <?php 
                                    foreach ($result_alertas as $al){ ?>
                                        <a class="d-flex justify-content-between alert_<?php echo $al->id ?>">
                                            <div class="media d-flex align-items-center">
    <!--                                             <div class="media-left">
                                                    <div class="mr-3"><img class="avatar" src="<?php // echo base_url(); ?>app-assets/img/portrait/small/avatar-s-20.png" alt="avatar" height="45" width="45"></div>
                                                </div> -->
                                                <div class="media-body">
                                                    <h6 class="m-0"><span><?php echo $al->tipo ?></span><small class="grey lighten-1 font-italic float-right"><?php echo date('d/m/Y',strtotime($al->dia)) ?></small></h6><h6 class="noti-text font-small-3 m-0"><?php echo $al->operador ?></h6><small class="noti-text"><?php echo $al->concepto ?> <button type="button" class="btn btn-sm btn-outline-primary" onclick="quitar_alert(<?php echo $al->id ?>)">Marcar como leido</button></small>
                                                </div>
                                            </div>
                                        </a>
                                    <?php } ?>
                                </li>
                                <li class="dropdown-menu-footer">
                                    <div class="noti-footer text-center cursor-pointer primary border-top text-bold-400 py-1" onclick="todas_notificacione()">Ver todas las notificaciones</div>
                                </li>
                            </ul>
                        </li>
                        <li class="dropdown nav-item mr-1"><a class="nav-link dropdown-toggle user-dropdown d-flex align-items-end" id="dropdownBasic2" href="javascript:;" data-toggle="dropdown">
                                <div class="user d-md-flex d-none mr-2"><span class="text-right"><?php echo $perfiln ?></span><span class="text-right text-muted font-small-3"><?php echo $nombre ?></span></div><img class="avatar" src="<?php echo $imglogo; ?>" alt="avatar" height="35" width="35">
                            </a>
                            <div class="dropdown-menu text-left dropdown-menu-right m-0 pb-0" aria-labelledby="dropdownBasic2">
                                <div class="dropdown-divider"></div><a class="dropdown-item" href="<?php echo base_url(); ?>Login/exitlogin">
                                    <div class="d-flex align-items-center"><i class="ft-power mr-2"></i><span>Cerrar sesión</span></div>
                                </a>
                            </div>
                        </li>
                        <!-- <li class="nav-item d-none d-lg-block mr-2 mt-1"><a class="nav-link notification-sidebar-toggle" href="javascript:;"><i class="ft-align-right font-medium-3"></i></a></li> -->
                    </ul>
                </div>
            </div>
        </div>
    </nav>

	<div class="wrapper">
        <!-- main menu-->
        <!--.main-menu(class="#{menuColor} #{menuOpenType}", class=(menuShadow == true ? 'menu-shadow' : ''))-->
        <div class="app-sidebar menu-fixed" data-background-color="man-of-steel" data-image="<?php echo base_url(); ?>app-assets/img/sidebar-bg/06.jpg" data-scroll-to-active="true">
            <!-- main menu header-->
            <!-- Sidebar Header starts-->
            <div class="sidebar-header">
                <div class="logo clearfix"><a class="logo-text float-left" href="<?php echo base_url(); ?>Inicio">
                        <div class="logo-img"><img style="filter: invert(1.4);" src="<?php echo base_url(); ?>img/p.png" alt="Prosubca" /></div><span class="text">rosubca</span>
                    </a><a class="nav-toggle d-none d-lg-none d-xl-block" id="sidebarToggle" href="javascript:;"><i class="toggle-icon ft-toggle-right" data-toggle="expanded"></i></a><a class="nav-close d-block d-lg-block d-xl-none" id="sidebarClose" href="javascript:;"><i class="ft-x"></i></a></div>
            </div>
            <!-- Sidebar Header Ends-->
            <!-- / main menu header-->
            <!-- main menu content-->
            <div class="sidebar-content main-menu-content">
                <div class="nav-container">
                    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
                        <?php foreach ($menu->result() as $x){ ?>
                        <li class="has-sub nav-item menu_<?php echo $x->MenuId ?>"><a href="javascript:;"><i class="<?php echo $x->Icon; ?>"></i><span class="menu-title" data-i18n="<?php echo $x->Nombre ?>"><?php echo $x->Nombre ?></span><span class="tag badge badge-pill badge-success float-right mr-1 mt-1"><?php echo $x->cantidadmenu ?></span></a>
                            <ul class="menu-content">
                                <?php $menusub = $this->ModeloSession->submenus($logueo['perfilid'],$x->MenuId); 
                                    foreach ($menusub->result() as $xs) {
                                ?>
                                    <li class="is-shown submenu_<?php echo $xs->MenusubId ?>"><a href="<?php echo base_url().$xs->Pagina; ?>"><i class="<?php echo $xs->Icon; ?> submenu-icon"></i><span class="menu-item" data-i18n="<?php echo $xs->Nombre; ?>"><?php echo $xs->Nombre; ?></span></a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </li>
                        <?php } ?>
                        <?php 
                        $menusub_1 = $this->ModeloSession->submenus_1($logueo['perfilid']);
                        foreach ($menusub_1->result() as $datos) {
                        ?>
                        <li class="nav-item act_<?php echo $datos->MenusubId ?>">
                            <a href="<?php echo base_url().$datos->Pagina; ?>">
                                <i class="<?php echo $datos->Icon; ?>" aria-hidden="true"></i>
                                <span class="menu-title"><?php echo $datos->Nombre; ?></span>
                            </a>
                        </li>
                        <?php } ?>  
                    </ul>
                </div>
            </div>
            <div class="sidebar-background"></div>

        </div>
	    <div class="main-panel">
            <!-- BEGIN : Main Content-->
            <div class="main-content">
                <div class="content-overlay"></div>
                <div class="content-wrapper">
var base_url = $('#base_url').val();
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_1').addClass('active');

    $("#usuario").keyup(function(){              
        var ta      =   $("#usuario");
        letras      =   ta.val().replace(/ /g, "");
        ta.val(letras)
    }); 
    
    $('#unidad').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Personal/search_unidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.id+' / '+element.placas,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }); 
});    

function guardar_empleado(){
    if($('#acceso_sistema').is(':checked')){
        add_user();
    }else{
    /////////Validación////////////
        var form_register = $('#form_registro');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'v_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input   
            ignore: "",
            rules: {
                num_empleado:{
                    required: true
                },
                nombre:{
                  required: true
                },
                puesto:{
                  required: true
                },
                estatus:{
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        /////////Verificar////////////
        var $valid = $("#form_registro").valid();
        console.log($valid);
        if($valid) {
            $(".btn_registro").prop('disabled', true);
            var datos = form_register.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Personal/addregistro',
                data:datos,
                statusCode:{
                    404: function(data){
                        swal("¡Error!","No Se encuentra el archivo","error");
                    },
                    500: function(){
                        swal("¡Error!","500","error");
                    }
                },
                success:function(data){
                    var id = data;
                    // file
                    if ($('#foto')[0].files.length > 0) {
                        var data = new FormData();
                        var inputFileImage = document.getElementById('foto');
                        var file = inputFileImage.files[0]; 
                        data.append('img',file);
                        data.append('id',id);
                        $.ajax({
                            type:'POST',
                            url: base_url+'Personal/file_data',
                            contentType:false,
                            data:data,
                            processData:false,
                            cache:false,
                            statusCode:{
                                404: function(data){
                                    swal("¡Error!","No Se encuentra el archivo","error");
                                },
                                500: function(){
                                    swal("¡Error!","500","error");
                                }
                            },
                            success:function(data){
                            }
                        });
                    }
                    swal({
                        title: "¡Éxito!",
                        text: "Registro guardado correctamente.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    setTimeout(function(){
                        location.href= base_url+'Personal';
                    }, 1500);
                }
            });
        }
    }
}

function btn_acceso(){
    if($('#acceso_sistema').is(':checked')){
        $('.text_acceso').css('display','block');
    }else{
        $('.v_red').remove();
        $('.text_acceso').css('display','none');
    }
}

function add_user(){

	$.validator.addMethod("usuarioexiste", function(value, element) {
    var respuestav;
    $.ajax({
        type: 'POST',
        url: base_url + 'Personal/validar',
        data: {
            usuario: value
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                if ($('#UsuarioID').val() > 0) {
                    respuestav = true;
                } else {
                    respuestav = false;
                }

            } else {
                respuestav = true;
            }



        }
    });
    console.log(respuestav);
    return respuestav;
	});

	var form_register2 = $('#form_registro');
	var error_register = $('.alert-danger', form_register2);
	var success_register = $('.alert-success', form_register2);

	var $validator1 = form_register2.validate({
	    errorElement: 'div', //default input error message container
	    errorClass: 'v_red', // default input error message class
	    focusInvalid: false, // do not focus the last invalid input
	    ignore: "",
	    rules: {
            num_empleado:{
                required: true
            },
	    	nombre:{
                required: true
            },
            puesto:{
                required: true
            },
            estatus:{
                required: true
            },
	        Usuario: {
	            usuarioexiste: true,
	            minlength: 4,
	            required: true
	        },
	        contrasena: {
	            minlength: 6,
	            required: true
	        },
	        contrasena2: {
	            equalTo: contrasena,
	            required: true
	        },
	    },
	    messages: {
	        Usuario: {
	            usuarioexiste: 'Seleccione otro nombre de usuario'
	        }

	    },
	    errorPlacement: function(error, element) {
	        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
	            element.parent().append(error);
	        } else if (element.parent().hasClass("vd_input-wrapper")) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },

	    invalidHandler: function(event, validator) { //display error alert on form submit              
	        success_register.fadeOut(500);
	        error_register.fadeIn(500);
	        scrollTo(form_register2, -100);

	    },

	    highlight: function(element) { // hightlight error inputs

	        $(element).addClass('vd_bd-red');
	        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

	    },

	    unhighlight: function(element) { // revert the change dony by hightlight
	        $(element)
	            .closest('.control-group').removeClass('error'); // set error class to the control group
	    },

	    success: function(label, element) {
	        label
	            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
	            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
	        $(element).removeClass('vd_bd-red');
	    }
	});
	var $valid2 = $("#form_registro").valid();
	if ($valid2) {
		$('.btn_registro').attr('disabled',true);
	    var datos = form_register2.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Personal/addregistro',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                var id = data;
                // file
                if ($('#foto')[0].files.length > 0) {
                    var data = new FormData();
                    var inputFileImage = document.getElementById('foto');
                    var file = inputFileImage.files[0]; 
                    data.append('img',file);
                    data.append('id',id);
                    $.ajax({
                        type:'POST',
                        url: base_url+'Personal/file_data',
                        contentType:false,
                        data:data,
                        processData:false,
                        cache:false,
                        statusCode:{
                            404: function(data){
                                swal("¡Error!","No Se encuentra el archivo","error");
                            },
                            500: function(){
                                swal("¡Error!","500","error");
                            }
                        },
                        success:function(data){
                        }
                    });
                }

            	swal({
                    title: "¡Éxito!",
                    text: "Registro guardado correctamente.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    location.href= base_url+'Personal';
                }, 1500);
            }
        });
	}
}

var cont_bloc=0;
function btn_bloquear(){
    if(cont_bloc==0){
        cont_bloc=1;
        $('#contrasena').attr('disabled',false);
        $('#contrasena2').attr('disabled',false);
    }else{
        cont_bloc=0;
        $('#contrasena').attr('disabled',true);
        $('#contrasena2').attr('disabled',true);
    }
}

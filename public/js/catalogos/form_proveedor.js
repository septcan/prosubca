var base_url = $('#base_url').val();
var idregp = $('#id_proveedor').val();
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_3').addClass('active');

    $("#usuario").keyup(function(){              
        var ta      =   $("#usuario");
        letras      =   ta.val().replace(/ /g, "");
        ta.val(letras)
    }); 

    $('#materiaprima').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Proveedores/search_materia',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    if(idregp!=0){
        tabla_materia_prima(idregp);
    }
    
});    

function guardar_proveedor(){
    if($('#acceso_sistema').is(':checked')){
        add_user();
    }else{
    /////////Validación////////////
        var form_register = $('#form_proveedor');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'v_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input   
            ignore: "",
            rules: {
                nombre:{
                  required: true
                },
                nombre_fiscal:{
                  required: true
                },
                estatus:{
                    required: true
                },
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        /////////Verificar////////////
        var $valid = $("#form_proveedor").valid();
        console.log($valid);
        if($valid) {
            $(".btn_registro").prop('disabled', true);
            var datos = form_register.serialize();
            $.ajax({
                type:'POST',
                url: base_url+'Proveedores/addregistro',
                data:datos,
                statusCode:{
                    404: function(data){
                        swal("¡Error!","No Se encuentra el archivo","error");
                    },
                    500: function(){
                        swal("¡Error!","500","error");
                    }
                },
                success:function(data){
                    var id_r = parseFloat(data);
                    guardar_materiaprima(id_r);
                    swal({
                        title: "¡Éxito!",
                        text: "Registro guardado correctamente.",
                        type: "success",
                        timer: 2000,
                        showConfirmButton: false
                    });
                    setTimeout(function(){
                        location.href= base_url+'Proveedores';
                    }, 1500);
                }
            });
        }
    }
}

function btn_acceso(){
    if($('#acceso_sistema').is(':checked')){
        $('.text_acceso').css('display','block');
    }else{
        $('.v_red').remove();
        $('.text_acceso').css('display','none');
    }
}

function add_user(){

	$.validator.addMethod("usuarioexiste", function(value, element) {
    var respuestav;
    $.ajax({
        type: 'POST',
        url: base_url + 'Proveedores/validar',
        data: {
            usuario: value
        },
        async: false,
        statusCode: {
            404: function(data){
                swal("Error!", "No Se encuentra el archivo", "error");
            },
            500: function(){
                swal("Error!", "500", "error");
            }
        },
        success: function(data) {
            if (data == 1) {
                if ($('#id_proveedor').val() > 0) {
                    respuestav = true;
                } else {
                    respuestav = false;
                }

            } else {
                respuestav = true;
            }



        }
    });
    console.log(respuestav);
    return respuestav;
	});

	var form_register2 = $('#form_proveedor');
	var error_register = $('.alert-danger', form_register2);
	var success_register = $('.alert-success', form_register2);

	var $validator1 = form_register2.validate({
	    errorElement: 'div', //default input error message container
	    errorClass: 'v_red', // default input error message class
	    focusInvalid: false, // do not focus the last invalid input
	    ignore: "",
	    rules: {
	    	nombre:{
                required: true
            },
            nombre_fiscal:{
                required: true
            },
            estatus:{
                required: true
            },
	        usuario: {
	            usuarioexiste: true,
	            minlength: 4,
	            required: true
	        },
	        contrasena: {
	            minlength: 6,
	            required: true
	        },
	        contrasena2: {
	            equalTo: contrasena,
	            required: true
	        },
	    },
	    messages: {
	        Usuario: {
	            usuarioexiste: 'Seleccione otro nombre de usuario'
	        }

	    },
	    errorPlacement: function(error, element) {
	        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")) {
	            element.parent().append(error);
	        } else if (element.parent().hasClass("vd_input-wrapper")) {
	            error.insertAfter(element.parent());
	        } else {
	            error.insertAfter(element);
	        }
	    },

	    invalidHandler: function(event, validator) { //display error alert on form submit              
	        success_register.fadeOut(500);
	        error_register.fadeIn(500);
	        scrollTo(form_register2, -100);

	    },

	    highlight: function(element) { // hightlight error inputs

	        $(element).addClass('vd_bd-red');
	        $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

	    },

	    unhighlight: function(element) { // revert the change dony by hightlight
	        $(element)
	            .closest('.control-group').removeClass('error'); // set error class to the control group
	    },

	    success: function(label, element) {
	        label
	            .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
	            .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
	        $(element).removeClass('vd_bd-red');
	    }
	});
	var $valid2 = $("#form_proveedor").valid();
	if ($valid2) {
		$('.btn_registro').attr('disabled',true);
	    var datos = form_register2.serialize();
        $.ajax({
            type: 'POST',
            url: base_url + 'Proveedores/addregistro',
            data: datos,
            statusCode: {
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
                var id_r = parseFloat(data);
                guardar_materiaprima(id_r);
            	swal({
                    title: "¡Éxito!",
                    text: "Registro guardado correctamente.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    location.href= base_url+'Proveedores';
                }, 1500);
            }
        });
	}
}

var cont_bloc=0;
function btn_bloquear(){
    if(cont_bloc==0){
        cont_bloc=1;
        $('#contrasena').attr('disabled',false);
        $('#contrasena2').attr('disabled',false);
    }else{
        cont_bloc=0;
        $('#contrasena').attr('disabled',true);
        $('#contrasena2').attr('disabled',true);
    }
}

function add_materia(){
    var id_reg = $('#materiaprima option:selected').val();
    var nombre = $('#materiaprima option:selected').text();
    var precio_sin = $('#precio_sin_factura').val();
    var precio_con = $('#precio_con_factura').val();
    if(id_reg!=undefined){
        get_tabla_material(0,id_reg,nombre,precio_sin,precio_con);
        $('#materiaprima').val(null).trigger('change');
        $('#precio_sin_factura').val('');
        $('#precio_con_factura').val('');
    }else{
        swal("¡Atención!", "Falta seleccinar una materia prima", "error");
    }
}
var contmp=0;
function get_tabla_material(id,id_reg,nombre,precio_sin_factura,precio_con_factura){
    //var preciotxt=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(precio);
    var html='<tr class="row_'+contmp+'">\
        <td><input type="hidden" id="id_mp" value="'+id+'">\
        <input type="hidden" id="id_reg_mp" value="'+id_reg+'">\
        '+nombre+'</td>\
        <td><input type="number" class="form-control" id="precio_sin_factura_mp" value="'+precio_sin_factura+'">\</td>\
        <td><input type="number" class="form-control" id="precio_con_factura_mp" value="'+precio_con_factura+'">\</td>\
        <td><a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="delete_mp('+contmp+','+id+')"><b><i class="ft-trash-2"></i></b></a></td>\
    </tr>';
    $('#tbody_pm').append(html);
    contmp++;
}

function delete_mp(cont,id){
    if(id==0){
        $('.row_'+cont).remove();
    }else{
        swal({
            title: "¿Seguro que deseas eliminar la materia prima?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#66903b",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url: base_url+'Proveedores/delete_registro_mp',
                        data: {id:id},
                        statusCode:{
                            404: function(data){
                                swal("¡Error!","No Se encuentra el archivo","error");
                            },
                            500: function(){
                                swal("¡Error!","500","error");
                            }
                        },
                        success:function(data){
                            swal({
                                title: "¡Éxito!",
                                text: "Eliminado correctamente",
                                type: "success",
                                timer: 1500,
                                showConfirmButton: false
                            });
                            $('.row_'+cont).remove();
                        }
                    });
                }
            }
        );
    }
}

function guardar_materiaprima(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#datatable_mp tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["idproveedor"] = id;
        item ["id"] = $(this).find("input[id*='id_mp']").val();
        item ["idmateriaprima"] = $(this).find("input[id*='id_reg_mp']").val();
        item ["precio_sin_factura"] = $(this).find("input[id*='precio_sin_factura_mp']").val();
        item ["precio_con_factura"] = $(this).find("input[id*='precio_con_factura_mp']").val();
        DATA.push(item);


    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Proveedores/registro_materiaprima',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });  
    }
}

function tabla_materia_prima(id){
    $.ajax({
        type:'POST',
        url: base_url+"Proveedores/get_tabla_material_prima",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element) {
                    get_tabla_material(element.id,element.idmateriaprima,element.nombre,element.precio_sin_factura,element.precio_con_factura);
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}
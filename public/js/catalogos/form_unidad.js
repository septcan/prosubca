var base_url = $('#base_url').val();
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_5').addClass('active'); 
    /*
    $('#operador').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Unidad/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });   */
});    

function guardar_registro(){
    /////////Validación////////////
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'v_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input   
        ignore: "",
        rules: {
            propiedad:{
              required: true
            },
            num_economico:{
              required: true
            },
            placas:{
                required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    /////////Verificar////////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $(".btn_registro").prop('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/addregistro',
            data:datos,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                img_tra(data);
                img_del(data);
                img_izq(data);
                img_der(data);
                swal({
                    title: "¡Éxito!",
                    text: "Registro guardado correctamente.",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    location.href= base_url+'Unidad';
                }, 1500);
            }
        });
    }
}

function img_tra(id){
    if ($('#img_trasera')[0].files.length > 0) {
        var data = new FormData();
        var inputFileImage = document.getElementById('img_trasera');
        var file = inputFileImage.files[0]; 
        data.append('img',file);
        data.append('id',id);
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/file_data_tra',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
            }
        });
    }
}

function img_del(id){
    if ($('#img_delantera')[0].files.length > 0) {
        var data = new FormData();
        var inputFileImage = document.getElementById('img_delantera');
        var file = inputFileImage.files[0]; 
        data.append('img',file);
        data.append('id',id);
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/file_data_del',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
            }
        });
    }
}

function img_izq(id){
    if ($('#img_izq')[0].files.length > 0) {
        var data = new FormData();
        var inputFileImage = document.getElementById('img_izq');
        var file = inputFileImage.files[0]; 
        data.append('img',file);
        data.append('id',id);
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/file_data_izq',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
            }
        });
    }
}

function img_der(id){
    if ($('#img_der')[0].files.length > 0) {
        var data = new FormData();
        var inputFileImage = document.getElementById('img_der');
        var file = inputFileImage.files[0]; 
        data.append('img',file);
        data.append('id',id);
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/file_data_der',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
            }
        });
    }
}
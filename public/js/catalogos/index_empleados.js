var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_1').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Personal/getData",
            type: "post",
        },
        "columns": [
            {"data": "personalId","visible": false},
            {"data": "num_empleado"}, 
            {"data": null,
                "render": function(data, type, row, meta){
                    var img = '';
                    if(row.foto!=''){   
                        img = '<img class="zoom" style="height: 35px; width:35px;" src="'+base_url+'uploads/empleado/'+row.foto+'" class="img-circle"/>';
                    }else{
                        if(row.sexo==1){
                            img = '<img class="zoom" style="height: 35px; width:35px;" src="'+base_url+'img/avatar/obreroh.png" class="img-circle"/>';
                        }else{
                            img = '<img class="zoom" style="height: 35px; width:35px;" src="'+base_url+'img/avatar/obrerom.png" class="img-circle"/>';
                        }   
                    }
                    return img;
                }
            },
            {"data": "nombre"}, 
            {"data": null,
                "render": function(data, type, row, meta){
                    var html = '';
                    if(row.puesto==1){   
                        html = 'Administrativo';
                    }else if(row.puesto==2){   
                        html = 'Operador';
                    }else if(row.puesto==3){   
                        html = 'Ayudante';
                    }
                    return html;
                }
            },
            {"data": "placas"}, 
            {"data": "nss"},
            {"data": "curp"},
            {"data": "rfc"},
            {"data": "correo"},
            {"data": "celular"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        if(row.estatus==1){
                            html = '<span class="badge badge-pill bg-light-success cursor-pointer">Activo</span>';
                        }else{
                            html = '<span class="badge badge-pill bg-light-danger cursor-pointer">Inactivo</span>';
                        }  
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="editar_href('+row.personalId+')"><i class="fa fa-edit"></i></button>\
                        <button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.personalId+'" data-nombrecompleto="'+row.nombre+'" onclick="modaldelete('+row.personalId+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function editar_href(id){
    location.href= base_url+'Personal/registro/'+id;
}

function recargartable(){
    tabledata.destroy();
    loadtable();
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar a "+$('.obtenernombre_'+id).data('nombrecompleto')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Personal/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        //swal("¡Éxito!","Eliminado correctamente","success");
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}

var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_6').addClass('active');
    loadtable();
});    

function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Materia_prima/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},  
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Editar" class="btn btn-sm btn-outline-info reg_'+row.id+'" data-nombre="'+row.nombre+'"  onclick="editar_href('+row.id+')"><i class="fa fa-edit"></i></button>\
                        <button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.id+'" data-nombremp="'+row.nombre+'" onclick="modaldelete('+row.id+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function recargartable(){
    tabledata.destroy();
    loadtable();
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar la material prima "+$('.obtenernombre_'+id).data('nombremp')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Materia_prima/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}

function guardar_registro(){
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'v_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input   
        ignore: "",
        rules: {
            nombre:{
              required: true
            },
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    /////////Verificar////////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $(".btn_registro").prop('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Materia_prima/addregistro',
            data:datos,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                swal({
                    title: "¡Éxito!",
                    text: "Registro guardado correctamente.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    $('.title_material').html('Nueva ');
                    recargartable();
                    $('#id_reg').val(0);
                    $('#nombre').val('');
                    $('.v_red').remove();
                    $(".btn_registro").prop('disabled', false);
                }, 1500);
            }
        });
    }
}

function editar_href(id){
    var nombre=$('.reg_'+id).data('nombre');
    $('#id_reg').val(id);
    $('#nombre').val(nombre);
    $('.title_material').html('Editar ');
}
var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_3').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Proveedores/getData",
            type: "post",
        },
        "columns": [
            {"data": "id_proveedor"},
            {"data": "nombre"}, 
            {"data": "nombre_fiscal"},
            {"data": "direccion"}, 
            {"data": "tipo_establecimiento"},
            {"data": "telefono"}, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        if(row.estatus==1){
                            html = '<span class="badge badge-pill bg-light-success cursor-pointer">Activo</span>';
                        }else{
                            html = '<span class="badge badge-pill bg-light-danger cursor-pointer">Inactivo</span>';
                        }  
                    return html;
                }
            }, 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="editar_href('+row.id_proveedor+')"><i class="fa fa-edit"></i></button>\
                        <button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.id_proveedor+'" data-nombrecompleto="'+row.nombre+'" onclick="modaldelete('+row.id_proveedor+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function editar_href(id){
    location.href= base_url+'Proveedores/registro/'+id;
}

function recargartable(){
    tabledata.destroy();
    loadtable();
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar a "+$('.obtenernombre_'+id).data('nombrecompleto')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Proveedores/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        //swal("¡Éxito!","Eliminado correctamente","success");
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}

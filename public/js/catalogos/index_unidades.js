var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
var id_reg=0;
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_5').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Unidad/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "propiedad"}, 
            {"data": "num_economico"},
            {"data": "placas"}, 
            {"data": "unidad_descripcion"},
            {"data": "marca_modelo"},
            {"data": "num_serie"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="modal_expediente('+row.id+')"><i class="fa fa-file-text"></i></button>';
                    html += ' <button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="modal_listado_expedientes('+row.id+')"><i class="fa fa-folder-open"></i></button>';
                    return html;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="editar_href('+row.id+')"><i class="fa fa-edit"></i></button>\
                        <button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.id+'" data-placas="'+row.placas+'" onclick="modaldelete('+row.id+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function editar_href(id){
    location.href= base_url+'Unidad/registro/'+id;
}

function recargartable(){
    tabledata.destroy();
    loadtable();
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar a la unidad con placas "+$('.obtenernombre_'+id).data('placas')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Unidad/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}


function modal_expediente(id){
    id_reg=id;
    $('#modal_archivo').modal();
    $("#archivo_file").fileinput('refresh',{
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["jpg","jpeg","png","pdf"],
        browseLabel: 'Seleccionar pdf',
        maxFilePreviewSize: 1000,
        allowedPreviewTypes: ['image', 'text'], // allow only preview of image & text files
        previewFileIconSettings: {
            'docx': '<i class="fa fa-file-word-o text-primary"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
            'pptx': '<i class="fa fa-file-powerpoint-o text-danger"></i>',
            'jpg': '<i class="fa fa-file-photo-o text-warning"></i>',
            'pdf': '<i class="fa fa-file-pdf-o text-danger"></i>',
            'zip': '<i class="fa fa-file-archive-o text-muted"></i>',
        }
    });
}


function guardar_registro(){
    if ($('#archivo_file')[0].files.length > 0) {
    /////////Validación////////////
        
        var form_register = $('#form_registro');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'v_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input   
            ignore: "",
            rules: {
                nombre:{
                  required: true
                }
            },
            
            errorPlacement: function(error, element) {
                if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                    element.parent().append(error);
                } else if (element.parent().hasClass("vd_input-wrapper")){
                    error.insertAfter(element.parent());
                }else {
                    error.insertAfter(element);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (element) { // hightlight error inputs
        
                $(element).addClass('vd_bd-red');
                $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                    .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
                $(element).removeClass('vd_bd-red');
            }
        });
        /////////Verificar////////////
        var $valid = $("#form_registro").valid();
        if($valid) {
            $(".btn_registro").prop('disabled', true);
            var datos = form_register.serialize()+'&idunidad='+id_reg;
            $.ajax({
                type:'POST',
                url: base_url+'Unidad/addregistro_expediente',
                data:datos,
                statusCode:{
                    404: function(data){
                        swal("¡Error!","No Se encuentra el archivo","error");
                    },
                    500: function(){
                        swal("¡Error!","500","error");
                    }
                },
                success:function(data){
                    var idr=parseFloat(data);
                    swal({
                        title: "¡Éxito!",
                        text: "Registro guardado correctamente.",
                        type: "success",
                        timer: 1500,
                        showConfirmButton: false
                    });
                    archivo_add(idr);
                    setTimeout(function(){
                        $('.v_red').remove();
                        $('#nombre_f').val('');
                        $('#modal_archivo').modal('hide');
                    }, 1500);
                }
            });
        }
    }else{
        swal("Atención!","No has seleccionado algún archivo","error");
    }
}

function archivo_add(id){
    if ($('#archivo_file')[0].files.length > 0) {
        var data = new FormData();
        var inputFileImage = document.getElementById('archivo_file');
        var file = inputFileImage.files[0]; 
        data.append('img',file);
        data.append('id',id);
        $.ajax({
            type:'POST',
            url: base_url+'Unidad/file_data_archivo',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
            }
        });
    }
}

function modal_listado_expedientes(id){
    $('#modal_archivo_listado').modal();
    $('.tabla_expedientes').html('');
    $.ajax({
        type:'POST',
        url: base_url+'Unidad/get_tabla_expedientes',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.tabla_expedientes').html(data);
        }
    });
}

function abrir_archivo(id){
    var archivo = $('.id_exp_'+id).data('archivo');
    var URL=base_url+'uploads/unidad/unidad_expediente/'+archivo;
    window.open(URL, 'Expediente', 'toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=1,width=500,height=500,left = 390,top = 50');
}

function modaldelete_expendiente(id){
    swal({
        title: "¿Seguro que deseas eliminar el expediente "+$('.obtenerexpediente_'+id).data('archivo_nombre')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Unidad/delete_registro_expediente',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_exp_'+id).remove();
                    }
                });
            }
        }
    );
}
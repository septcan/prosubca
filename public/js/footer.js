var base_url = $('#base_url').val();
function quitar_alert(id){
	var total_alert=$('.total_alert').text();
	var total=parseFloat(total_alert);
	$.ajax({
        type:'POST',
        url: base_url+'Inicio/update_alert',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        	var resta=total-parseFloat(1);
        	$('.total_alert').html(resta);
        	$('.total_alert2').html(resta);
            $('.alert_'+id).remove();
        }
    });
}

function todas_notificacione(){
    location.href= base_url+'Notificaciones';
}
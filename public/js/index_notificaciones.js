var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
$(document).ready(function () {
    loadtable();
});    

function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Notificaciones/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                    var fecha=ChangeDate(row.dia);
                        html+='<div class="d-flex align-items-center">\
                            <div class="media-body">\
                                <h6 class="m-0"><span>'+row.tipo+'</span></h6><h6 class="noti-text font-small-3 m-0">'+row.nombre+'</h6><small class="noti-text">'+row.concepto+'</small>\
                            </div>\
                        </div>';    
                    return html;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                    var fecha=ChangeDate(row.dia);
                        html+='<small class="grey lighten-1 font-italic" style="font-size: 20px;">'+ChangeDate(row.dia)+'</small>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"/"+parte[1]+"/"+parte[0];
}
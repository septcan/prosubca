var base_url = $('#base_url').val();
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_9').addClass('active'); 
	importar_excel();
});
function importar_excel(){
	$("#file_excel").fileinput('refresh',{
        showCancel: false,
        showCaption: false,
        showUpload: false,// quita el boton de upload
        //rtl: true,
        allowedFileExtensions: ["xls","xlsx"],
        browseLabel: 'Seleccionar excel',
        maxFilePreviewSize: 1000,
        previewFileIconSettings: {
            'xls': '<i class="fa fa-file-excel-o text-success"></i>',
            'xlsx': '<i class="fa fa-file-excel-o text-success"></i>',
        }
    });
}

function guarda_excel() {
    if ($('#file_excel')[0].files.length > 0) {
        $(".btn_registro").prop('disabled', true);
        swal({
            title: "¡Éxito!",
            text: "Guardando datos por favor espere...",
            type: "success",
            showConfirmButton: false
        });
        var data = new FormData();
        var file_excel = document.getElementById('file_excel');
        var file = file_excel.files[0]; 
        data.append('archivo',file);
        $.ajax({
            type:'POST',
            url: base_url+'Bascula/cargaexcel',
            contentType:false,
            data:data,
            processData:false,
            cache:false,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                swal({
                    title: "¡Éxito!",
                    text: "Excel guardado correctamente.",
                    type: "success",
                    showConfirmButton: false
                });
                setTimeout(function(){
                    location.href= base_url+'Bascula';
                }, 2000);
            }
        });
    }
}
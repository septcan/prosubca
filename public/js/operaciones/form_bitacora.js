var base_url = $('#base_url').val();
var idreg_detalle=0;
var folio_num_aux=0;
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_8').addClass('active'); 

    $('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Bitacora_colecta/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

});    


function dia_colecta(){
    var operador = $('#idpersonal option:selected').text();
    var idoperador = $('#idpersonal option:selected').val();
    var dia = $('#fecha').val();
    if(operador!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Bitacora_colecta/get_folios',
            data: {dia:dia,operador:operador,idoperador:idoperador},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                $('.folios_txt').html(data);
                registro_bitacora_colecta();
                $('.folio_txt').html('');
                $('.folio_materia_txt').html('');
                $('.folio_proveedor_materia_costos_txt').html('');  
                $('.folios_gastos_tabla').html('');
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar un operador","error");
        $('#fecha').val('');
    }
}

function btn_folio(id,estatus){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_info_folio',
        data: {id:id,
            dia:$('#fecha').val(),
            estatus:estatus,
            idoperador:$('#idpersonal option:selected').val(),
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folio_txt').html(data);
        }
    });
}

function validar_folio(id){
    $(".btn_folios").prop('disabled', true);
    $('.btn_validar_'+id).css('display','none');
    $('.tabla_validar_'+id).css('display','block');
    var idreg=$('#idreg').val();
    registro_bitacora_colecta_folio(idreg,id);
    setTimeout(function(){
        get_proveedor_materia();
    }, 1000);
    
}

///// guardado
function registro_bitacora_colecta(){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/addregistro_bitacora_colecta',
        data:{
            dia:$('#fecha').val(),
            idoperador:$('#idpersonal option:selected').val(),
            operador:$('#idpersonal option:selected').text()
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            var array=$.parseJSON(data);
            $('#idreg').val(array.id);;
            //get_folios_gastos(array.id); 
            //tabla_gastos_colecta(array.id);
            if(array.total_folios!=0){
                get_folios_gastos(array.id);
                tabla_gastos_colecta(array.id);
            }
            if(array.estatus_folio!=0){
                btn_folio(array.idbascula,0);
                setTimeout(function(){
                    folio_num_aux=array.estatus_folio;
                    validar_folio(array.estatus_folio);
                    setTimeout(function(){
                        get_proveedor_materia();
                        get_folio_proveedor_materia_costos_txt();
                    }, 1000);
                }, 1000);
                
                //if($('#check_chofer').is(':checked')){
                /*if(array.idbascula!=0){
                    
                }*/
                //}
            }
        }
    });
}

function registro_bitacora_colecta_folio(id,folio){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/addregistro_bitacora_colecta_folio',
        data:{
            idbitacora_colecta:id,
            folio:folio
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            idreg_detalle=parseFloat(data);
            //$('#idreg').val(data);
            get_ruta(idreg_detalle,folio);
        }
    });
}

function get_ruta(id,folio) {
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/tabla_materia',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.tabla_validar_materia_'+folio).html(data);
        }
    });
}


function registro_bitacora_colecta_ruta(id,idruta){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/addregistro_bitacora_colecta_ruta',
        data:{
            idbitacora_colecta_detalle:id,
            idruta:idruta
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function get_proveedor_materia(){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_info_proveedor_materia',
        data: {id:idreg_detalle},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folio_materia_txt').html(data);
            setTimeout(function(){
                $('#idproveedor').select2({
                    width: 'resolve',
                    minimumInputLength: 3,
                    minimumResultsForSearch: 10,
                    placeholder: 'Buscar una opción',
                    allowClear: true,
                    ajax: {
                        url: base_url+'Bitacora_colecta/search_proveedor',
                        dataType: "json",
                        data: function(params) {
                            var query = {
                                search: params.term,
                                type: 'public'
                            }
                            return query;
                        },
                        processResults: function(data) {
                            var itemscli = [];
                            data.forEach(function(element) {
                                itemscli.push({
                                    id: element.id_proveedor,
                                    text: element.nombre,
                                });
                            });
                            return {
                                results: itemscli
                            };
                        },
                    }
                }).on('select2:select', function(e) {
                    $('.folio_materia_txt').css('display','block');
                    var data = e.params.data;            
                    get_select_materia(data.id);
                });
            }, 1000);
        }
    });
}

function get_select_materia(id){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_proveedor_materia',
        data:{
            id:id,
            idreg_detalle:idreg_detalle
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.select_materiaprima').html(data);
        }
    });
}

function add_materia_bitacora(){
    var idproveedor=$('#idproveedor option:selected').val();
    var materiaprima=$('#materiaprima_b').val();
    var precio_sin=$('#precio_sin').val();
    var precio_con=$('#precio_con').val();
    var kilos=$('#kilos_b').val();
    var check_fac=0;
    if($('#factura_materia').is(':checked')){
        check_fac=1;
    }else{
        check_fac=0;
    }
    if(idproveedor!=undefined){
        if(materiaprima!=0){
            if(kilos!='' && kilos!=0){
                $.ajax({
                    type:'POST',
                    url: base_url+'Bitacora_colecta/addregistro_bitacora_colecta_detalle_material',
                    data:{
                        idbitacora_colecta_detalle:idreg_detalle,
                        idproveedor:idproveedor,
                        materiaprima:materiaprima,
                        precio_sin:precio_sin,
                        precio_con:precio_con,
                        kilos:kilos,
                        factura:check_fac,
                    },
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        var arraybita=$.parseJSON(data); 
                        if(arraybita.validar==0){
                            get_folio_proveedor_materia_costos_txt();
                            get_proveedor_materia();
                        }else{
                            swal("¡Atención!","Los kilos ingresados son mayor al peso real","error");
                        }
                    }
                });
            }else{
                swal("¡Atención!","FALTA AGREGAR KILOS","error");    
            }
        }else{
            swal("¡Atención!","EL PRODUCTO NO EXISTE EN PROVEEDOR","error");    
        }
    }else{
        swal("¡Atención!","FALTA SELECCIONAR PROVEEDOR","error");
    }

}

function get_folio_proveedor_materia_costos_txt(){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_tabla_proveedor_materia_costos',
        data:{
            idbitacora_colecta_detalle:idreg_detalle,
            pesoreal:$('#peso_realk').val(),
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folio_proveedor_materia_costos_txt').html(data);
        }
    });
}

function modaldelete_proveedor_material(id){
    swal({
        title: "¿Seguro que deseas eliminar este registro?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Bitacora_colecta/delete_registro_proveedor',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_mc_'+id).remove();
                        $('.btn_v_mc').remove();
                        calculo_total();
                    }
                });
            }
        }
    );
}

function validar_estatus_folio(id,folio){
    swal({
        title: "¿Seguro que deseas validar este folio "+folio+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Bitacora_colecta/estatus_colecta',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        dia_colecta();
                        $('.folio_txt').html('');
                        $('.folio_materia_txt').html('');
                        $('.folio_proveedor_materia_costos_txt').html('');
                        swal({
                            title: "¡Éxito!",
                            text: "Guardado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                });
            }
        }
    );
}

function calculo_total(){
    var TABLA   = $("#datatable_material tbody > tr");
    var sumakilos=0; 
    var pesoreal=0; 

    TABLA.each(function(){ 
        var kilos = $(this).find("input[id*='kilos_x']").val();
        pesoreal = $(this).find("input[id*='pesoreal_x']").val();
        sumakilos+=parseFloat(kilos);
    });     
    var resta=parseFloat(pesoreal)-parseFloat(sumakilos);
    $('.total_kilos').html(sumakilos);
    $('.total_resta').html(resta);
}

function get_folios_gastos(id){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_info_gastos',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folios_gastos').html(data);
        }
    });
}

function add_gatos_bitacora(){
    var idtipogasto=$('#idtipogasto_g option:selected').val();
    var importe=$('#importe_g').val();
    if(idtipogasto!=0){
        if(importe!='' && importe!=0){
            $.ajax({
                type:'POST',
                url: base_url+'Bitacora_colecta/addregistro_bitacora_colecta_gastos',
                data:{
                    idbitacora_colecta:$('#idreg').val(),
                    idcategoria:idtipogasto,
                    importe:importe,
                },
                statusCode:{
                    404: function(data){
                        swal("¡Error!","No Se encuentra el archivo","error");
                    },
                    500: function(){
                        swal("¡Error!","500","error");
                    }
                },
                success:function(data){
                    $('#importe_g').val('');
                    tabla_gastos_colecta($('#idreg').val());
                }
            });
        }else{
            swal("¡Atención!","FALTA AGREGAR IMPORTE","error");    
        }
    }else{
        swal("¡Atención!","FALTA SELECCIONAR TIPO DE GASTO","error");
    }

}


function tabla_gastos_colecta(id){
    $.ajax({
        type:'POST',
        url: base_url+'Bitacora_colecta/get_tabla_gastos_colecta',
        data:{
            idbitacora_colecta:id
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folios_gastos_tabla').html(data);
        }
    });
}

function modaldelete_gasto(id){
    swal({
        title: "¿Seguro que deseas eliminar este gasto?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Bitacora_colecta/delete_registro_gasto',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_gs_'+id).remove();
                        calcular_gasto();
                    }
                });
            }
        }
    );
}

function calcular_gasto(){
    var TABLA   = $("#datatable_gasto tbody > tr");
    var suma=0; 
    TABLA.each(function(){ 
        var importe = $(this).find("input[id*='importe_x']").val();
        suma+=parseFloat(importe);
    });     
    var suma_t=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma);
    $('.total_gastos').html(suma_t);
}

function validar_estatus_gastos(id){
    swal({
        title: "¿Seguro que deseas validar gastos?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Bitacora_colecta/estatus_colecta_gastos',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        dia_colecta();
                        $('.folio_txt').html('');
                        $('.folio_materia_txt').html('');
                        $('.folio_proveedor_materia_costos_txt').html('');
                        swal({
                            title: "¡Éxito!",
                            text: "Guardado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                });
            }
        }
    );
}

function btn_check_chofer(){
    if($('#check_chofer').is(':checked')){
    }else{
    }
}
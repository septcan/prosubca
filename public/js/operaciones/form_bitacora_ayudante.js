var base_url = $('#base_url').val();
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_17').addClass('active'); 

    $('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Bitacora_colecta/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

});    

function dia_colecta(){
    var operador = $('#idpersonal option:selected').text();
    var idoperador = $('#idpersonal option:selected').val();
    var dia = $('#fecha').val();
    if(operador!=''){
        $.ajax({
            type:'POST',
            url: base_url+'AyudanteBitacora_colecta/get_folios',
            data: {dia:dia,operador:operador,idoperador:idoperador},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                var array = $.parseJSON(data);
                $('.folios_txt').html(array.texto_folios);
                if(array.folios==0){
                    $('.folio_txt').html('');
                    $('.folios_gastos').html(''); 
                    $('.folios_gastos_tabla').html('');
                }else{
                    get_folios_gastos(0); 
                    if(array.idreg!=0){
                        tabla_gastos_colecta(array.idreg);
                    }
                }
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar un chofer","error");
        $('#fecha').val('');
    }
}

function btn_folio(id){
    $.ajax({
        type:'POST',
        url: base_url+'AyudanteBitacora_colecta/get_info_folio',
        data: {id:id
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folio_txt').html(data);

        }
    });
}

function get_folios_gastos(id){
    $.ajax({
        type:'POST',
        url: base_url+'AyudanteBitacora_colecta/get_info_gastos',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folios_gastos').html(data);
        }
    });
}

function add_gatos_bitacora(){
    var idtipogasto=$('#idtipogasto_g option:selected').val();
    var importe=$('#importe_g').val();
    var idoperador = $('#idpersonal option:selected').val();
    var dia = $('#fecha').val();
    if(idtipogasto!=0){
        if(importe!='' && importe!=0){
            $(".btn_registro_gasto").prop('disabled', true);
            $.ajax({
                type:'POST',
                url: base_url+'AyudanteBitacora_colecta/addregistro_bitacora_colecta_gastos_ayudante',
                data:{
                    idcategoria:idtipogasto,
                    importe:importe,
                    idoperador:idoperador,
                    dia:dia,
                },
                statusCode:{
                    404: function(data){
                        swal("¡Error!","No Se encuentra el archivo","error");
                    },
                    500: function(){
                        swal("¡Error!","500","error");
                    }
                },
                success:function(data){
                    setTimeout(function(){
                        $(".btn_registro_gasto").prop('disabled', false);
                    }, 1000);
                    $('#importe_g').val('');
                    tabla_gastos_colecta(parseFloat(data));
                }
            });
        }else{
            swal("¡Atención!","FALTA AGREGAR IMPORTE","error");    
        }
    }else{
        swal("¡Atención!","FALTA SELECCIONAR TIPO DE GASTO","error");
    }

}

function tabla_gastos_colecta(id){
    $.ajax({
        type:'POST',
        url: base_url+'AyudanteBitacora_colecta/get_tabla_gastos_colecta',
        data:{
            idbitacora_colecta:id
        },
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.folios_gastos_tabla').html(data);
        }
    });
}

function modaldelete_gasto(id){
    swal({
        title: "¿Seguro que deseas eliminar este gasto?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'AyudanteBitacora_colecta/delete_registro_gasto',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_gs_'+id).remove();
                        calcular_gasto();
                    }
                });
            }
        }
    );
}

function calcular_gasto(){
    var TABLA   = $("#datatable_gasto tbody > tr");
    var suma=0; 
    TABLA.each(function(){ 
        var importe = $(this).find("input[id*='importe_x']").val();
        suma+=parseFloat(importe);
    });     
    var suma_t=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma);
    $('.total_gastos').html(suma_t);
}

function validar_estatus_gastos(id){
    swal({
        title: "¿Seguro que deseas validar gastos?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'AyudanteBitacora_colecta/estatus_colecta_gastos',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        $('.folios_txt').html('');
                        $('.folio_txt').html('');
                        $('.folios_gastos').html(''); 
                        $('.folios_gastos_tabla').html('');
                        $('#fecha').val('');
                        swal({
                            title: "¡Éxito!",
                            text: "Guardado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                    }
                });
            }
        }
    );
}
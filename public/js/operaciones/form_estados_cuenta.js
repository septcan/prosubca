var base_url = $('#base_url').val();

$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_15').addClass('active'); 
    $('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                var itemscli = [];
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
 
});

function burcar_registro(){
    var idoperador=$('#idpersonal option:selected').val();
    var fecha1=$('#fecha1').val();
    var fecha2=$('#fecha2').val();
    if(idoperador!=undefined){
        if(fecha1!=''){
            if(fecha2!=''){
                add_estado_cuenta();
                get_estado_cuenta();
            }else{
                swal("¡Atención!","Falta seleccionar mes","error");
            }
        }else{
            swal("¡Atención!","Falta seleccionar fecha inicio","error");
        }
    }else{
        swal("¡Atención!","Falta seleccionar un operador","error");
    }
}

function get_estado_cuenta(){
    var idoperador=$('#idpersonal option:selected').val();
    var fecha1=$('#fecha1').val();
    var fecha2=$('#fecha2').val();
    $.ajax({
        type:'POST',
        url: base_url+'EstadosDeCuenta/get_estado_cuenta_operador',
        data: {idoperador:idoperador,anio:fecha1,mes:fecha2},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.tabla_text').html(data);
        }
    });
}

function edit_deposito(id){
    calculo_saldo();
    $.ajax({
        type:'POST',
        url: base_url+'EstadosDeCuenta/update_deposito',
        data: {id:id,deposito:$('.deposito'+id).val(),saldo:$('.total_saldo'+id).val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
                        
        }
    });
}

function calculo_saldo(){
    var TABLA   = $("#datatable_estado_cuentas tbody > tr");
    var saldo_ant=$('#saldo_anterior_x').val();
    var saldo_ant_n=parseFloat(saldo_ant);
    var conts=0;
    var saldo_final=0;
    TABLA.each(function(){ 
        var deposito = $(this).find("input[id*='deposito']").val();
        var compras_gastos = $(this).find("input[id*='compras_gastos']").val();
        if(conts==0){
            var suma_saldo=parseFloat(saldo_ant_n)+parseFloat(deposito);
            var resta_saldo=parseFloat(suma_saldo)-parseFloat(compras_gastos);
        }else{
            var cont_r=conts-1;
            var saldo_ant_t=$('.total_saldo_x'+cont_r).val();
            var suma_saldo=parseFloat(saldo_ant_t)+parseFloat(deposito);
            var resta_saldo=parseFloat(suma_saldo)-parseFloat(compras_gastos);
        }
        $(this).find("input[id*='total_saldo']").val(resta_saldo);
        var saldotxt=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(resta_saldo);
        $(this).find("span[class*='saldot']").html(saldotxt);

        if(resta_saldo<0){
            $('.saldo_'+conts).css('color','red');
        }else{
            $('.saldo_'+conts).css('color','black');
        }
        saldo_final=resta_saldo;
        conts++;
    });
    var saldo_finaltxt=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(saldo_final);
    $('#saldo_final').val(saldo_final);
    $('.saldo_final').html(saldo_finaltxt);
}


function edit_ruta(id){
    $.ajax({
        type:'POST',
        url: base_url+'EstadosDeCuenta/update_ruta',
        data: {id:id,idruta:$('#ruta_id'+id+' option:selected').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){           
        }
    });
}

function guardar_estado_cuenta(tipo,validar_estatus){
    var titulo="¿Seguro que deseas finalizar estado de cuenta?";
    if(validar_estatus==1){
        titulo="¿Seguro que deseas editar estado de cuenta afecta proceso siguiente?";
    } 
    swal({
        title: titulo,
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                var id=$('#idestado_cuenta').val();
                $.ajax({
                    type:'POST',
                    url: base_url+'EstadosDeCuenta/update_estado_cuenta',
                    data: {id:id,
                        rendimiento:$('#rendimiento_total_efectivo').val(),
                        saldo_final:$('#saldo_final').val(),
                    },
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        if(tipo==1){
                            generar_estados_cuenta(id);
                        }
                        swal({
                            title: "¡Éxito!",
                            text: "Finalización de estado decuenta correctamente",
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        });
                        setTimeout(function(){
                            location.href= base_url+'EstadosDeCuenta';
                        }, 2500);
                    }
                });

            }
        }
    );
}


function add_estado_cuenta(){
    var idoperador=$('#idpersonal option:selected').val();
    var fecha1=$('#fecha1').val();
    var fecha2=$('#fecha2').val();
    $.ajax({
        type:'POST',
        url: base_url+'EstadosDeCuenta/addestado_cuenta',
        data: {idoperador:idoperador,anio:fecha1,mes:fecha2},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('#idestado_cuenta').val(parseFloat(data));
        }
    });
}

function generar_estados_cuenta(id){
    window.open(base_url+'EstadosDeCuenta/reporte_estados_cuenta/'+id,'_blank'); 
}

var base_url = $('#base_url').val();

$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_13').addClass('active'); 
    $('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#unidad').select2({
        width: 'resolve',
        minimumInputLength: 1,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Rendimientos/search_unidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.id+' / '+element.placas,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }); 
});

function burcar_registro(){
    var idoperador=$('#idpersonal option:selected').val();
    var fecha1=$('#fecha1').val();
    var fecha2=$('#fecha2').val();
    var unidad=$('#unidad option:selected').val();
    if(idoperador!=undefined){
        if(fecha1!=''){
            if(fecha2!=''){
                if(unidad!=undefined){
                    add_rendimiento();
                    get_rendimiento();
                }else{
                    swal("¡Atención!","Falta seleccionar unidad","error");
                }
            }else{
                swal("¡Atención!","Falta seleccionar mes","error");
            }
        }else{
            swal("¡Atención!","Falta seleccionar fecha inicio","error");
        }
    }else{
        swal("¡Atención!","Falta seleccionar un operador","error");
    }
}

function add_rendimiento(){
    var idoperador=$('#idpersonal option:selected').val();
    var fecha1=$('#fecha1').val();
    var fecha2=$('#fecha2').val();
    var unidad=$('#unidad option:selected').val();
    $.ajax({
        type:'POST',
        url: base_url+'Rendimientos/addrendimiento',
        data: {idoperador:idoperador,anio:fecha1,mes:fecha2,unidad:unidad},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('#idrendimiento').val(parseFloat(data));
        }
    });
}


function get_rendimiento(){
	var idoperador=$('#idpersonal option:selected').val();
	var fecha1=$('#fecha1').val();
	var fecha2=$('#fecha2').val();
	var unidad=$('#unidad option:selected').val();
	$.ajax({
        type:'POST',
        url: base_url+'Rendimientos/get_rendimiento_general',
        data: {idoperador:idoperador,anio:fecha1,mes:fecha2,unidad:unidad},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.tabla_text').html(data);
        }
    });
}

function get_fecha(){
    $.ajax({
        type:'POST',
        url: base_url+'Rendimientos/fecha_select',
        //data: {},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.selectfecha_txt').html(data);
        }
    });
}

function edit_litros(){
    var id=$('#idrendimiento').val();
    $.ajax({
        type:'POST',
        url: base_url+'Rendimientos/update_litros',
        data: {id:id,litros:$('#precio_litro').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            calculo();
        }
    });
}

function edit_kmrecorridos(){
    var id=$('#idrendimiento').val();
    $.ajax({
        type:'POST',
        url: base_url+'Rendimientos/update_kmrecorrido',
        data: {id:id,km_recorridos:$('#km_recorridos').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            calculo();
        }
    });
}

function calculo(){

    var km_recorridos = $('#km_recorridos').val();
    var litros_consumidos = $('#litros_consumidos').val();
    var rendimiento_n = $('#rendimiento_n').val();
    
    var precio_litro = $('#precio_litro').val();

    var total_rendimiento = parseFloat(km_recorridos)/parseFloat(litros_consumidos);
    var total_rendimientot=Number.parseFloat(total_rendimiento).toFixed(2);//new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_rendimiento);
    $('#total_rendimiento').val(total_rendimientot);
    $('.total_rendimiento_tx').html(total_rendimientot);

    var litros_consumidos_esp = parseFloat(km_recorridos)/parseFloat(rendimiento_n);
    var litros_consumidos_espt=Number.parseFloat(litros_consumidos_esp).toFixed(2);
    $('#litros_consumidos_esp').val(litros_consumidos_espt);
    $('.litros_consumidos_esp_tx').html(litros_consumidos_espt);       
    

    var litros_diferencia=parseFloat(litros_consumidos)-parseFloat(litros_consumidos_esp);
    var litros_diferenciat=Number.parseFloat(litros_diferencia).toFixed(2);
    $('#litros_diferencia').val(litros_diferenciat);
    $('.litros_diferencia_tx').html(litros_diferenciat);

    var total_efectivo=parseFloat(litros_diferenciat)*parseFloat(precio_litro);
    var total_efectivot=Number.parseFloat(total_efectivo).toFixed(2);
    var total_efectivotx=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total_efectivot);
    $('#total_efectivo').val(total_efectivot);
    $('.total_efectivo_tx').html(total_efectivotx);    
}

function guardar_rendimiento(tipo){
    swal({
        title: "¿Seguro que deseas finalizar rendimiento?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                var id=$('#idrendimiento').val();
                $.ajax({
                    type:'POST',
                    url: base_url+'Rendimientos/update_rendimiento',
                    data: {id:id,
                        precio_litro:$('#precio_litro').val(),
                        rendimiento_n:$('#rendimiento_n').val(),
                        km_recorridos:$('#km_recorridos').val(),
                        litros_consumidos:$('#litros_consumidos').val(),
                        total_rendimiento:$('#total_rendimiento').val(),
                        litros_consumidos_esp:$('#litros_consumidos_esp').val(),
                        litros_diferencia:$('#litros_diferencia').val(),
                        total_efectivo:$('#total_efectivo').val(),
                    },
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        if(tipo==1){
                            generar_rendimiento(id);
                        }
                        swal({
                            title: "¡Éxito!",
                            text: "Finalización de rendimiento correctamente",
                            type: "success",
                            timer: 3000,
                            showConfirmButton: false
                        });
                        setTimeout(function(){
                            location.href= base_url+'Rendimientos';
                        }, 2500);
                    }
                });

            }
        }
    );
}

function generar_rendimiento(id){
    window.open(base_url+'Rendimientos/reporte_rendimiento/'+id,'_blank'); 
}

function editar_rendimiento(){
    var id=$('#idrendimiento').val();
    swal({
        title: "¿Seguro que deseas editar rendimiento afectara el proceso siguiente?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Rendimientos/update_registro_rendimiento_estatus',
                    data: {id:id
                    },
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        burcar_registro();
                        swal.close();
                        window.scrollTo(0,0);
                    }
                });   
            }
        }
    );
}


function generar_rendimiento_pdf(){
    var id=$('#idrendimiento').val();
    window.open(base_url+'Rendimientos/reporte_rendimiento/'+id,'_blank'); 
}
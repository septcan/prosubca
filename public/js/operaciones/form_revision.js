var base_url = $('#base_url').val();
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_11').addClass('active'); 
    $('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    
});    


function dia_colecta(){
    var operador = $('#idpersonal option:selected').text();
    var idoperador = $('#idpersonal option:selected').val();
    var dia = $('#fecha').val();
    if(operador!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Revision/get_compras',
            data: {dia:dia,operador:operador,idoperador:idoperador},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                $('.comprar_text').html(data);
                setTimeout(function(){
                    $('#idayudante').select2({
                        width: 'resolve',
                        minimumInputLength: 3,
                        minimumResultsForSearch: 10,
                        placeholder: 'Buscar una opción',
                        allowClear: true,
                        ajax: {
                            url: base_url+'Revision/search_ayudante',
                            dataType: "json",
                            data: function(params) {
                                var query = {
                                    search: params.term,
                                    type: 'public'
                                }
                                return query;
                            },
                            processResults: function(data) {
                                //var productos=data;
                                var itemscli = [];
                                //console.log(data);
                                data.forEach(function(element) {
                                    itemscli.push({
                                        id: element.personalId,
                                        text: element.nombre,
                                    });
                                });
                                return {
                                    results: itemscli
                                };
                            },
                        }
                    });
                }, 2000);
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar un operador","error");
        $('#fecha').val('');
    }
}

function calcular_km(){
    var km_i=$('#km_inicial').val();
    var km_f=$('#km_final').val();
    var resta=parseFloat(km_f)-parseFloat(km_i);
    $('#km_recorrido').html(resta);
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_kilometraje',
        data: {km_inicial:km_i,km_final:km_f,km_recorrido:resta,idbitacora_colecta:$('#idbitacora_colecta').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function calculo_flete(){
    var gastos = $('#suma_gastos').val();
    var total_vale = $('#total_vale').val();
    var tarjeta_pase = $('#tarjeta_pase').val();
    var sueldoope = $('#sueldoope').val();
    var sueldoayudante = $('#sueldoayudante').val();
    
    var total_kilos = $('#total_kilos').val(); 
    var suma = parseFloat(gastos)+parseFloat(total_vale)+parseFloat(tarjeta_pase)+parseFloat(sueldoope)+parseFloat(sueldoayudante);
    var division=suma/parseFloat(total_kilos);
    console.log(division);
    var conDecimal = division.toFixed(2); 
    if(conDecimal=='NaN'){
        $('#costo_flete').html('$0.00');
    }else{
        $('#costo_flete').html(conDecimal);    
    }

    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_tarjeta_pase_costo_flete',
        data: {tarjeta_pase:tarjeta_pase,costo_flete:division,idbitacora_colecta:$('#idbitacora_colecta').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function add_gas(){
    var tipo_gas = $('#tipo_gas option:selected').val();
    var num_vale = $('#tipo_vale').val();
    var litros_gas = $('#litros_gas').val();
    var precio = $('#precio').val();
    if(tipo_gas!=0){
        if(tipo_vale!=0 && tipo_vale!=''){  
            if(litros_gas!=0 && litros_gas!=''){
                if(precio!=0 && precio!=''){
                    $("#btn_add_gas").prop('disabled', true);
                    $.ajax({
                        type:'POST',
                        url: base_url+'Revision/add_tipo_vale',
                        data: {tipo:tipo_gas,vale:num_vale,litros:litros_gas,precio:precio,idbitacora_colecta:$('#idbitacora_colecta').val()},
                        statusCode:{
                            404: function(data){
                                swal("¡Error!","No Se encuentra el archivo","error");
                            },
                            500: function(){
                                swal("¡Error!","500","error");
                            }
                        },
                        success:function(data){
                            $('#tipo_vale').val('');
                            $('#litros_gas').val('');
                            $('#precio').val('');
                            tabla_gas();
                            setTimeout(function(){
                                $("#btn_add_gas").prop('disabled', false);
                            }, 1500);
                        }
                    });
                }else{
                    swal("¡Atención!","Falta agregar precio","error");
                }
            }else{
                swal("¡Atención!","Falta agregar listros","error");
            }
        }else{
            swal("¡Atención!","Falta agregar número de vale","error");
        }
    }else{
        swal("¡Atención!","Falta seleccionar tipo de vale","error");
    }
}

function tabla_gas(){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/get_tabla_gas',
        data: {idbitacora_colecta:$('#idbitacora_colecta').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.text_tabla_gas').html(data);
        }
    });
}

function modaldelete_gasto_vale(id){
    swal({
        title: "¿Seguro que deseas eliminar este vale?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Revision/delete_registro_vale',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_vale_'+id).remove();
                        calculo_vale();
                    }
                });
            }
        }
    );
}

function calculo_vale(){
    var TABLA   = $("#total_vale_gas tbody > tr");
    var total=0;
    TABLA.each(function(){ 
        var importe = $(this).find("input[id*='importe_gas']").val();
        total+=parseFloat(importe);
    });
    $('#total_vale').val(total);
    var total_vale=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    $('.total_vale_text').html(total_vale);  
}

function edit_factura(id,pago){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_registro_factura',
        data: {id:id,validar_factura:$('#idfactura_c_'+id+' option:selected').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            var precio = parseFloat(data);
            calculo_compra_factura(id,precio,pago);
        }
    });
}

function calculo_compra_factura(id,precio,pago){
    var kilos=$('#kilos_cantidad_'+id).text();
    if(pago==2){
        var mult=0;
        var precio=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(0);
        var importe=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(0);
    }else{
        var mult=parseFloat(kilos)*parseFloat(precio);
        var precio=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(precio);
        var importe=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(mult);
    } 
    $('.idcompra_'+id).html(precio);
    $('#total_importe_'+id).html(importe);
    $('.total_compras_c'+id).val(mult);
    total_compras_global(); 
}
  
function total_compras_global(){
    var TABLA   = $("#datatable_compras tbody > tr");
    var total=0;
    TABLA.each(function(){ 
        var total_c = $(this).find("input[id*='total_compras_c']").val();
        total+=parseFloat(total_c);
    });
    $('#suma_compras').val(total);
    var total_cmp=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    $('.total_compras').html(total_cmp);
    calculo_gatos();
}

function agregar_factura(id){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_agregar_factura',
        data: {id:id,factura:$('#factura_c'+id).val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function check_validar_compra(id){
    var validar_check=0;
    if($('#validar_compra'+id).is(':checked')){
        validar_check=1;
    }else{
        validar_check=0;
    }
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_validar_check',
        data: {id:id,validar:validar_check},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function agregar_factura_gastos(id){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_agregar_factura_gasto',
        data: {id:id,factura:$('#factura_g'+id).val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function check_validar_gasto(id){
    var validar_check=0;
    if($('#validar_gatos'+id).is(':checked')){
        validar_check=1;
    }else{
        validar_check=0;
    }
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_validar_gasto_check',
        data: {id:id,validar:validar_check},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
        }
    });
}

function editar_gasto(id){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/update_gasto',
        data: {id:id,gasto:$('#editargasto_'+id).val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            var total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format($('#editargasto_'+id).val());
            $('.total_gasto_'+id).html(total);
            $('.total_gasto_int_'+id).val($('#editargasto_'+id).val());
            calculo_gatos();
        }
    });
}

function calculo_gatos(){
    var TABLA   = $("#datatable_gasto tbody > tr");
    var total=0;
    TABLA.each(function(){ 
        var total_g = $(this).find("input[id*='total_gasto']").val();
        total+=parseFloat(total_g);
    });
    $('#suma_gastos').val(total);
    var total_vale=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(total);
    $('.text_gatos_global').html(total_vale);  
    total_compras_gastos();
}

function total_compras_gastos(){
    var compras = $('#suma_compras').val();
    var gastos = $('#suma_gastos').val();
    var suma = parseFloat(compras)+parseFloat(gastos);
    $('#compras_gastos').val(suma);
    var total=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(suma);
    $('#totalcomprasgastos').html(total);
}

function add_ayudante(){
    var ayudante = $('#idayudante option:selected').text();
    var idayudante = $('#idayudante option:selected').val();
    if(ayudante!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Revision/add_ayudaten_colecta',
            data: {idayudante:idayudante,idbitacora_colecta:$('#idbitacora_colecta').val()},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                tabla_ayudante();
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar un ayudante","error");
    }
}

function tabla_ayudante(){
    $.ajax({
        type:'POST',
        url: base_url+'Revision/get_tabla_ayudante',
        data: {idbitacora_colecta:$('#idbitacora_colecta').val()},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.text_tabla_ayudante').html(data);
        }
    });
}

function modaldelete_ayudante(id){
    swal({
        title: "¿Seguro que deseas eliminar ayudante?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Revision/delete_registro_ayudante',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        $('.row_ayudante_'+id).remove();
                        calculo_ayudante();
                    }
                });
            }
        }
    );
}

function calculo_ayudante(){
    var TABLA   = $("#tabla_ayudante tbody > tr");
    var total=0;
    TABLA.each(function(){ 
        var importe = $(this).find("input[id*='sueldoayudante_t']").val();
        total+=parseFloat(importe);
    });
    $('#sueldoayudante').val(total);  
}

var id_colecta_detalle_aux=0;
var id_colecta_aux=0;
function editar_ruta(id){
    id_colecta_detalle_aux=id;

    $('#idruta_colecta').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_ruta',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.ruta,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#modal_rutas_editar').modal();
}

function guardar_ruta(){
    var ruta_colecta = $('#idruta_colecta option:selected').text();
    var idruta_colecta = $('#idruta_colecta option:selected').val();
    if(ruta_colecta!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Revision/update_registro_ruta',
            data: {idruta:idruta_colecta,id_colecta_detalle:id_colecta_detalle_aux},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                $('#modal_rutas_editar').modal('hide');
                $('.ruta_'+id_colecta_detalle_aux).html(ruta_colecta);
                $('#idruta_colecta').val(null).trigger('change');
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar una ruta","error");
    }
}

function editar_unidad(id,idbi){
    id_colecta_detalle_aux=id;
    id_colecta_aux=idbi;
    $('#idunidad_colecta').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_unidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.placas,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#modal_unidad_editar').modal();
}


function guardar_unidad(){
    var unidad_colecta = $('#idunidad_colecta option:selected').text();
    var idunidad_colecta = $('#idunidad_colecta option:selected').val();
    var val_uni=0;
    if($('#validar_unidades').is(':checked')){
        val_uni=1;
    }else{
        val_uni=0; 
    }

    if(unidad_colecta!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Revision/update_registro_unidad_colecta_detalle',
            data: {idunidad:idunidad_colecta,id_colecta:id_colecta_detalle_aux,val_uni:val_uni,id_colecta:id_colecta_aux},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                $("#validar_unidades").prop("checked",false);
                $('#modal_unidad_editar').modal('hide');
                $('#idunidad_colecta').val(null).trigger('change');
                if(val_uni==1){
                    $('.unidad_t').html(unidad_colecta);
                }else{
                    $('.uni_'+id_colecta_detalle_aux).html(unidad_colecta);
                }
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar una unidad","error");
    }
}

function guardar_colecta(id,tipo){
    if(tipo==2){
        generar_revision(id);
    }else{
        swal({
            title: "¿Seguro que deseas finalizar la revisión?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#66903b",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url: base_url+'Revision/update_registro_bitacora_colecta',
                        data: {id:id,
                            total_compras:$('#suma_compras').val(),
                            total_gastos:$('#suma_gastos').val(),
                            total_compras_gastos:$('#compras_gastos').val(),
                            total_kilos:$('#total_kilos').val(),
                        },
                        statusCode:{
                            404: function(data){
                                swal("¡Error!","No Se encuentra el archivo","error");
                            },
                            500: function(){
                                swal("¡Error!","500","error");
                            }
                        },
                        success:function(data){
                            if(tipo==1){
                                generar_revision(id);
                            }
                            swal({
                                title: "¡Éxito!",
                                text: "Finalización de revisión correctamente",
                                type: "success",
                                timer: 3000,
                                showConfirmButton: false
                            });
                            setTimeout(function(){
                                location.href= base_url+'Revision';
                            }, 2500);
                        }
                    });
                    
                }
            }
        );
    }
}

var id_colecta_aux=0;
function editar_unidad_general(id){
    id_colecta_aux=id;
    $('#idunidad_colecta_general').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Revision/search_unidad',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id,
                        text: element.placas,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });
    $('#modal_unidad_editar_general').modal();
}

function guardar_unidad_general(){
    var unidad_colecta = $('#idunidad_colecta_general option:selected').text();
    var idunidad_colecta = $('#idunidad_colecta_general option:selected').val();
    if(unidad_colecta!=''){
        $.ajax({
            type:'POST',
            url: base_url+'Revision/update_registro_unidad_colecta',
            data: {idunidad:idunidad_colecta,id_colecta:id_colecta_aux},
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                $('#modal_unidad_editar_general').modal('hide');
                $('#unidad_colecta').val(unidad_colecta);
                $('#idunidad_colecta_general').val(null).trigger('change');
            }
        });
    }else{
        swal("¡Atención!","Falta seleccionar una unidad","error");
    }
}

function editar_colecta(id){
    swal({
        title: "¿Seguro que deseas editar revisión afectara el proceso siguiente?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Revision/update_registro_colecta_estatus',
                    data: {id:id
                    },
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        dia_colecta();
                        swal.close();
                        window.scrollTo(0,0);
                    }
                });   
            }
        }
    );
}


function generar_revision(id){
    window.open(base_url+'Revision/reporte_revision/'+id,'_blank'); 
}

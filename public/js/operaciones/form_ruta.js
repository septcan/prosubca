var base_url = $('#base_url').val();
var idregp = $('#idreg').val();
var razonaux = '';
var direccionaux = ''; 
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_7').addClass('active');

    /*$('#idpersonal').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Rutas/search_operador',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.personalId,
                        text: element.nombre,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    });

    $('#proveedor').select2({
        width: 'resolve',
        minimumInputLength: 3,
        minimumResultsForSearch: 10,
        placeholder: 'Buscar una opción',
        allowClear: true,
        ajax: {
            url: base_url+'Rutas/search_proveedor',
            dataType: "json",
            data: function(params) {
                var query = {
                    search: params.term,
                    type: 'public'
                }
                return query;
            },
            processResults: function(data) {
                //var productos=data;
                var itemscli = [];
                //console.log(data);
                data.forEach(function(element) {
                    itemscli.push({
                        id: element.id_proveedor,
                        text: element.nombre+' / '+element.direccion,
                        razon: element.nombre,
                        direccion: element.direccion,
                    });
                });
                return {
                    results: itemscli
                };
            },
        }
    }).on('select2:select', function(e) {
        var data = e.params.data;
        razonaux=data.razon;
        direccionaux=data.direccion;
    });
    
    if(idregp!=0){
        tabla_rutas(idregp);
    }*/
});    

function guardar_registro(){
    /////////Validación////////////
    var form_register = $('#form_registro');
    var error_register = $('.alert-danger', form_register);
    var success_register = $('.alert-success', form_register);

    var $validator1=form_register.validate({
        errorElement: 'div', //default input error message container
        errorClass: 'v_red', // default input error message class
        focusInvalid: false, // do not focus the last invalid input   
        ignore: "",
        rules: {
            ruta:{
              required: true
            }
        },
        
        errorPlacement: function(error, element) {
            if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
                element.parent().append(error);
            } else if (element.parent().hasClass("vd_input-wrapper")){
                error.insertAfter(element.parent());
            }else {
                error.insertAfter(element);
            }
        }, 
        
        invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register.fadeOut(500);
                error_register.fadeIn(500);
                scrollTo(form_register,-100);

        },

        highlight: function (element) { // hightlight error inputs
    
            $(element).addClass('vd_bd-red');
            $(element).siblings('.help-inline').removeClass('help-inline fa fa-check vd_green mgl-10');

        },

        unhighlight: function (element) { // revert the change dony by hightlight
            $(element)
                .closest('.control-group').removeClass('error'); // set error class to the control group
        },

        success: function (label, element) {
            label
                .addClass('valid').addClass('help-inline fa fa-check vd_green mgl-10') // mark the current input as valid and display OK icon
                .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
            $(element).removeClass('vd_bd-red');
        }
    });
    /////////Verificar////////////
    var $valid = $("#form_registro").valid();
    if($valid) {
        $(".btn_registro").prop('disabled', true);
        var datos = form_register.serialize();
        $.ajax({
            type:'POST',
            url: base_url+'Rutas/addregistro',
            data:datos,
            statusCode:{
                404: function(data){
                    swal("¡Error!","No Se encuentra el archivo","error");
                },
                500: function(){
                    swal("¡Error!","500","error");
                }
            },
            success:function(data){
                var idr=parseFloat(data);
                //guardar_proveedores(idr);
                swal({
                    title: "¡Éxito!",
                    text: "Registro guardado correctamente.",
                    type: "success",
                    timer: 1500,
                    showConfirmButton: false
                });
                setTimeout(function(){
                    location.href= base_url+'Rutas';
                }, 1500);
            }
        });
    }
}


/*function add_proveedor(){
    var id_reg = $('#proveedor option:selected').val();
    var nombre = $('#proveedor option:selected').text();
    if(id_reg!=undefined){
        get_tabla_proveedor(0,id_reg,razonaux,direccionaux);
        $('#proveedor').val(null).trigger('change');
    }else{
        swal("¡Atención!", "Falta seleccinar un proveedor", "error");
    }
    

}
var contp=0;
function get_tabla_proveedor(id,id_reg,nombre,direccion){
    var html='<tr class="row_'+contp+'">\
        <td><input type="hidden" id="id_mp" value="'+id+'">\
        <input type="hidden" id="id_reg_mp" value="'+id_reg+'">\
        '+nombre+'</td>\
        <td>'+direccion+'</td>\
        <td><a type="button" class="btn btn-danger btn-icon round mr-1 mb-1" title="Agregar" onclick="delete_mp('+contp+','+id+')"><b><i class="ft-trash-2"></i></b></a></td>\
    </tr>';
    $('#tbody_pm').append(html);
    contp++;
}

function delete_mp(cont,id){
    if(id==0){
        $('.row_'+cont).remove();
    }else{
        swal({
            title: "¿Seguro que deseas eliminar este proveedor?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#66903b",
            confirmButtonText: "Aceptar",
            cancelButtonText: "Cancelar",
            closeOnConfirm: false,
            closeOnCancel: true },
            function(isConfirm){
                if (isConfirm) {
                    $.ajax({
                        type:'POST',
                        url: base_url+'Rutas/delete_proveedor',
                        data: {id:id},
                        statusCode:{
                            404: function(data){
                                swal("¡Error!","No Se encuentra el archivo","error");
                            },
                            500: function(){
                                swal("¡Error!","500","error");
                            }
                        },
                        success:function(data){
                            swal({
                                title: "¡Éxito!",
                                text: "Eliminado correctamente",
                                type: "success",
                                timer: 1500,
                                showConfirmButton: false
                            });
                            $('.row_'+cont).remove();
                        }
                    });
                }
            }
        );
    }
}

function guardar_proveedores(id){
    var cont=0;
    var DATA  = [];
    var TABLA   = $("#datatable_mp tbody > tr");
    TABLA.each(function(){ 
        cont=1;
        item = {};
        item ["idruta"] = id;
        item ["id"] = $(this).find("input[id*='id_mp']").val();
        item ["idproveedor"] = $(this).find("input[id*='id_reg_mp']").val();
        DATA.push(item);

    });     
    if(cont==1){
        INFO  = new FormData();
        aInfo   = JSON.stringify(DATA);
        INFO.append('data', aInfo);
        $.ajax({
            data: INFO, 
            type: 'POST',
            url : base_url + 'Rutas_operadores/registro_proveedores',
            processData: false, 
            contentType: false,
            async: false,
            statusCode:{
                404: function(data){
                    swal("Error!", "No Se encuentra el archivo", "error");
                },
                500: function(){
                    swal("Error!", "500", "error");
                }
            },
            success: function(data){
            }
        });  
    }
}

function tabla_rutas(id){
    $.ajax({
        type:'POST',
        url: base_url+"Rutas_operadores/get_tabla_rutas_form",
        data: {id:id},
        success: function (response){
            var array = $.parseJSON(response);
            if(array.length>0) {
                array.forEach(function(element) {
                    get_tabla_proveedor(element.id,element.idproveedor,element.nombre,element.direccion);
                });
            }   
        },
        error: function(response){
            swal("Error!", "Algo salió mal, intente de nuevo o contacte al administrador del sistema", "error");
        }
    });
}*/
var base_url = $('#base_url').val();
var tabledata;
var id_delete=0;
var id_reg=0;
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_9').addClass('active'); 
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Bascula/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "folio"},
            {"data": "clvprov"},
            {"data": "razsoc"},
            {"data": "fecfol"},
            {"data": "fecsal"},
            {"data": "horaent"},
            {"data": "horasal"},
            {"data": "chofer"},
            {"data": "placas"},
            {"data": "transporte"},
            {"data": "procede"},
            {"data": "origen"},
            {"data": "clvprod"},
            {"data": "descri"},
            {"data": "clvope"},
            {"data": "nombre"},
            {"data": "peso_envdo"},
            {"data": "peso_empaq"},
            {"data": "pbruto"},
            {"data": "tara"},
            {"data": "neto"},
            {"data": "peso_real"},
            {"data": "cancel"},
            {"data": "ciclo"},
            {"data": "manual"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.id+'" data-folio="'+row.folio+'" onclick="modaldelete('+row.id+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar el registro con folio "+$('.obtenernombre_'+id).data('folio')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Bascula/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}























var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_12').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "ListaEstadosDeCuenta/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": "anio"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var mes_txt = "";
                    if(row.mes==1){
                        mes_txt="ENERO";
                    }else if(row.mes==2){
                        mes_txt="FEBRERO";
                    }else if(row.mes==3){
                        mes_txt="MARZO";
                    }else if(row.mes==4){
                        mes_txt="ABRIL";
                    }else if(row.mes==5){
                        mes_txt="MAYO";
                    }else if(row.mes==6){
                        mes_txt="JUNIO";
                    }else if(row.mes==7){
                        mes_txt="JULIO";
                    }else if(row.mes==8){
                        mes_txt="AGOSTO";
                    }else if(row.mes==9){
                        mes_txt="SEPTIEMBRE";
                    }else if(row.mes==10){
                        mes_txt="OCTUBRE";
                    }else if(row.mes==11){
                        mes_txt="NOVIEMBRE";
                    }else if(row.mes==12){
                        mes_txt="DICIEMBRE";
                    }
                    return mes_txt;
                }
            }, 
            {"data": "rendimiento"},
            {"data": "saldo_final"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="PDF" class="btn btn-sm btn-outline-info"  onclick="generar_estados_cuenta('+row.id+')"><i class="fa fa-file-pdf-o" style="font-size: 20px;"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function generar_estados_cuenta(id){
    window.open(base_url+'ListaEstadosDeCuenta/reporte_estados_cuenta/'+id,'_blank'); 
}
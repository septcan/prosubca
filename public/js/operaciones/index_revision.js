var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function () {
    $('.menu_2').addClass('sidebar-group-active');
    $('.submenu_12').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "ListaRevision/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "nombre"},
            {"data": null,
                "render": function(data, type, row, meta ) {
                    return ChangeDate(row.dia);
                }
            },
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var costo=new Intl.NumberFormat('es-MX',{style: 'currency', currency: 'MXN'}).format(row.costo_flete);
                    return costo;
                }
            },
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="PDF" class="btn btn-sm btn-outline-info"  onclick="generar_revision('+row.id+')"><i class="fa fa-file-pdf-o" style="font-size: 20px;"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function ChangeDate(data){
  var parte=data.split('-');
  return parte[2]+"/"+parte[1]+"/"+parte[0];
}

function generar_revision(id){
    window.open(base_url+'Revision/reporte_revision/'+id,'_blank'); 
}
var base_url = $('#base_url').val();
var tabledata;
$(document).ready(function () {
    $('.menu_1').addClass('sidebar-group-active');
    $('.submenu_7').addClass('active');
    loadtable();
});    
function loadtable() {
    tabledata = $('#datatable').DataTable({
        fixedHeader: true,
        responsive: 0,
        "bProcessing": true,
        "serverSide": true,
        "ajax": {
            url: base_url + "Rutas/getData",
            type: "post",
        },
        "columns": [
            {"data": "id"},
            {"data": "ruta"},
            /*
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="modal_rutas_listado('+row.id+')"><i class="fa fa-eye"></i></button>';
                    return html;
                }
            },
            */ 
            {"data": null,
                "render": function(data, type, row, meta ) {
                    var html = '';
                        html+='<button type="button" title="Editar" class="btn btn-sm btn-outline-info"  onclick="editar_href('+row.id+')"><i class="fa fa-edit"></i></button>\
                        <button type="button" title="Eliminar" class="btn btn-sm btn-outline-danger obtenernombre_'+row.id+'" data-operador="'+row.nombre+'" onclick="modaldelete('+row.id+')"><i class="fa fa-trash-o"></i></button>';    
                    return html;
                }
            }, 
          ],
        order: [
            [0, "desc"]
          ],
    });
}

function editar_href(id){
    location.href= base_url+'Rutas/registro/'+id;
}

function modaldelete(id){
    swal({
        title: "¿Seguro que deseas eliminar la ruta con el operador "+$('.obtenernombre_'+id).data('operador')+"?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#66903b",
        confirmButtonText: "Aceptar",
        cancelButtonText: "Cancelar",
        closeOnConfirm: false,
        closeOnCancel: true },
        function(isConfirm){
            if (isConfirm) {
                $.ajax({
                    type:'POST',
                    url: base_url+'Rutas/delete_registro',
                    data: {id:id},
                    statusCode:{
                        404: function(data){
                            swal("¡Error!","No Se encuentra el archivo","error");
                        },
                        500: function(){
                            swal("¡Error!","500","error");
                        }
                    },
                    success:function(data){
                        swal({
                            title: "¡Éxito!",
                            text: "Eliminado correctamente",
                            type: "success",
                            timer: 1500,
                            showConfirmButton: false
                        });
                        //swal("¡Éxito!","Eliminado correctamente","success");
                        tabledata.ajax.reload();
                    }
                });
            }
        }
    );
}


function modal_rutas_listado(id){
    $('.tabla_rutas').html('');
    $('#modal_rutas_listado').modal();
    $.ajax({
        type:'POST',
        url: base_url+'Rutas/get_tabla_rutas',
        data: {id:id},
        statusCode:{
            404: function(data){
                swal("¡Error!","No Se encuentra el archivo","error");
            },
            500: function(){
                swal("¡Error!","500","error");
            }
        },
        success:function(data){
            $('.tabla_rutas').html(data); 
        }
    });
}
$(document).ready(function() {
	
		"use strict";
        var form_register = $('#login-form');
        var error_register = $('.alert-danger', form_register);
        var success_register = $('.alert-success', form_register);

        var $validator1=form_register.validate({
            errorElement: 'label', //default input error message container
            errorClass: 'error', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                usuario: {
                    required: true
                },              
                password: {
                    required: true,
                    minlength: 5
                },
                
            },
            
            errorPlacement: function(error, element) {
                var placement = element.closest('.input-group');
                if (!placement.get(0)) {
                    placement = element;
                }
                if (error.text() !== '') {
                    placement.after(error);
                }
            }, 
            
            invalidHandler: function (event, validator) { //display error alert on form submit              
                    success_register.fadeOut(500);
                    error_register.fadeIn(500);
                    scrollTo(form_register,-100);

            },

            highlight: function (label) { // hightlight error inputs
        
                $(label).closest('.form-group').removeClass('has-success').addClass('has-error');

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                $(label).closest('.form-group').removeClass('has-error');
                label.remove();
            }
        });
        $('.login').click(function(event) {
            var $valid = form_register.valid();
            if ($valid) {
                var user = $('#usuario').val();
                var pass =  $('#password').val();                
                $.ajax({
                    type:'POST',
                    url:'Login/session',
                    data:{
                            usu:user,
                            passw:pass
                        },
                    async:false,
                    success:function(data){
                        console.log(data);
                        
                        if(data==1){
                            $('#login-submit').prepend('<i class="fa fa-spinner fa-spin mgr-10"></i>');
                            $(".sessionerronea").hide(500);
                            setTimeout(function(){window.location.href = "Sistema"},2500);
                            $('.panel-sign').hide(1000);
                            $('.cargando').css('display','block');
                            $('body').css('background','#66903b');
                            $('.cuadrados').css('display','none');
                            
                        }
                        else{
                            $(".sessionerronea").show(500);
                            setTimeout(function(){
                                $( "#login-submit" ).removeClass( "danger" );
                            },5000);
                        }

                    }
                });
            }
        });	
	$('#password').keypress(function(e) {
        if(e.which == 13) {
           $('.login').click();
        }
    });
});


